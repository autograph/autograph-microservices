package com.meelash.fundingservice.api.service.funding;

import com.meelash.fundingservice.api.entity.FundingBackgroundImage;
import com.meelash.fundingservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IFundingBackgroundImageService {
    FundingBackgroundImage storeFile(MultipartFile file, Long id);

    ApiResponse listAllBackgrounds();

    ApiResponse getBackgroundById(Long id);
}
