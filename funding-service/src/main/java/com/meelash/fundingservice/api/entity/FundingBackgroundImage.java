package com.meelash.fundingservice.api.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "background_image")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FundingBackgroundImage {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "backgroundImage")
    private Funding funding;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "document_type")
    private String documentType;

    @Column(name = "upload_directory")
    private String uploadDirectory;

    public FundingBackgroundImage(String name, String documentType, String uploadDirectory) {
        this.fileName = name;
        this.documentType = documentType;
        this.uploadDirectory = uploadDirectory;
    }

    public String getPath(Long userId) {
        String separator = System.getProperty("file.separator");
        return uploadDirectory + separator + userId + separator + "funding_background_image" + separator
                + fileName;
    }
}
