package com.meelash.fundingservice.api.dto.response;

import lombok.Data;

@Data
public class FundingIconResponseDTO {
    private Long id;
    private Long fundingId;
    private String fileName;
    private String documentType;
    private String uploadDirectory;
}
