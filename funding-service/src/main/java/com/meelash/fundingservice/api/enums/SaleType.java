package com.meelash.fundingservice.api.enums;

public enum SaleType {
    PRIMARY, SECONDARY
}
