package com.meelash.fundingservice.api.repository.funding;

import com.meelash.fundingservice.api.entity.Funding;
import com.meelash.fundingservice.api.entity.Sale;
import com.meelash.fundingservice.api.enums.SaleType;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface ISaleRepository extends JpaRepository<Sale, Long> {
    Optional<Sale> findById(@NonNull Long id);
   ArrayList<Sale> findAllByFunding(@NonNull Funding funding);
   ArrayList<Sale> findAllByFundingAndSaleType(@NonNull Funding funding, @NonNull SaleType saleType);
}
