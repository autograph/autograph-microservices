package com.meelash.fundingservice.api.exception;

public class AttachmentStorageException extends RuntimeException {

    public AttachmentStorageException(String message) {
        super(message);
    }

    public AttachmentStorageException(String message, Throwable clause) {
        super(message, clause);
    }
}
