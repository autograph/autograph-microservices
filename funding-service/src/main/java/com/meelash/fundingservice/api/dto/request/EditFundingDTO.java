package com.meelash.fundingservice.api.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class EditFundingDTO {
    private String name;
    private String description;
    private String website;
    private String phoneNumber;
    private Long fundingId;

    private MultipartFile fundingIcon;
    private MultipartFile fundingProfileImage;
    private MultipartFile fundingBackgroundImage;
}
