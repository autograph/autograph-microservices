package com.meelash.fundingservice.api.repository.funding;

import com.meelash.fundingservice.api.entity.FundingProfileImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IFundingProfileImageRepository extends JpaRepository<FundingProfileImage, Long> {
    @Override
    Optional<FundingProfileImage> findById(Long id);

    @Override
    List<FundingProfileImage> findAll();
}
