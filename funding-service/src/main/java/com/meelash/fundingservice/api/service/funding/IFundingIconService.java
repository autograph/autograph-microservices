package com.meelash.fundingservice.api.service.funding;

import com.meelash.fundingservice.api.entity.FundingIcon;
import com.meelash.fundingservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IFundingIconService {
    FundingIcon storeFile(MultipartFile file, Long id);

    ApiResponse listAllIcons();

    ApiResponse getIconById(Long id);
}
