package com.meelash.fundingservice.api.service.funding.impl;

import com.meelash.fundingservice.api.dto.request.EditFundingDTO;
import com.meelash.fundingservice.api.dto.request.FundingDTO;
import com.meelash.fundingservice.api.dto.request.FundingResponseDTO;
import com.meelash.fundingservice.api.dto.response.AutographResponseDTO;
import com.meelash.fundingservice.api.entity.*;
import com.meelash.fundingservice.api.enums.ResponseCodes;
import com.meelash.fundingservice.api.repository.funding.IFundingRepository;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.IFundingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FundingService implements IFundingService {
    private final ModelMapper modelMapper;
    private final IFundingRepository fundingRepository;
    private final FundingIconsService fundingIconsService;
    private final FundingBackgroundImageService fundingBackgroundImageService;
    private final FundingProfileImageService fundingProfileImageService;
    private final CircuitBreakerFactory cbFactory;
    private final WebClient.Builder webClientBuilder;
    private ApiResponse apiResponse;

    @Autowired
    public FundingService(ModelMapper modelMapper, IFundingRepository fundingRepository, ApiResponse apiResponse, FundingIconsService fundingIconsService, FundingBackgroundImageService fundingBackgroundImageService, FundingProfileImageService fundingProfileImageService, CircuitBreakerFactory cbFactory, WebClient.Builder webClientBuilder) {
        this.modelMapper = modelMapper;
        this.fundingRepository = fundingRepository;
        this.apiResponse = apiResponse;
        this.fundingIconsService = fundingIconsService;
        this.fundingBackgroundImageService = fundingBackgroundImageService;
        this.fundingProfileImageService = fundingProfileImageService;
        this.cbFactory = cbFactory;
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public ApiResponse createFunding(FundingDTO fundingDTO, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();
        if (Objects.equals(fundingDTO.getFundingIcon(), null) ||
                Objects.equals(fundingDTO.getFundingBackgroundImage(), null) ||
                Objects.equals(fundingDTO.getFundingProfileImage(), null)) {
            apiResponse.setCode(ResponseCodes.FILE_CANNOT_BE_NULL.getValue());
            apiResponse.setResponse(false);

            return apiResponse;
        }

        FundingIcon fundingIcon = fundingIconsService.storeFile(fundingDTO.getFundingIcon(), userId);
        FundingProfileImage fundingProfileImage = fundingProfileImageService.storeFile(fundingDTO.getFundingProfileImage(), userId);
        FundingBackgroundImage fundingBackgroundImage = fundingBackgroundImageService.storeFile(fundingDTO.getFundingBackgroundImage(), userId);


        Funding funding = new Funding(userId, fundingDTO.getWalletAddress(), fundingDTO.getName(), fundingDTO.getDescription(),
                fundingDTO.getWebsite(), fundingDTO.getPhoneNumber(), fundingIcon, fundingProfileImage, fundingBackgroundImage);
        Funding fundingSaved = fundingRepository.save(funding);

//        ApiResponse followingUsersResponse =
//                cbFactory.create("get-list-of-user-followers")
//                        .run(() -> getListOfUserFollowers(userId, authorization),
//                                throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
//                                        Collections.emptyList()));
////
//        if (followingUsersResponse != null && followingUsersResponse.getResponse() != null) {
//            List<Long> followingUsers = modelMapper.map(followingUsersResponse.getResponse(),
//                    List.class);
//
//            cbFactory.create("post-to-followers-feed")
//                    .run(
//                            () -> postToFollowersFeed(userId, authorization, autograph, followingUsers),
//                            throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
//                                    Collections.emptyList()));
//        }
//
//        ApiResponse autographCreationResponse = cbFactory.create("add-autograph-to-user")
//                .run(() -> addAutographToUser(userId, authorization, autograph),
//                        throwable -> new ApiResponse(ResponseCodes.USER_NOT_FOUND.getValue(), false));

        if (Objects.equals(fundingSaved, null)) {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        } else {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(fundingSaved, FundingResponseDTO.class));
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getListOfFundings(String authorization) {
        List<Funding> fundings = fundingRepository.findAll();
        List<FundingResponseDTO> fundingsDTO = fundings.stream()
                .map(funding -> modelMapper.map(funding, FundingResponseDTO.class))
                .collect(Collectors.toList());

        apiResponse.setResponse(fundingsDTO);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse getFundingDetailsById(Long id, String authorization) {
        Optional<Funding> fundingOptional = fundingRepository.findById(id);
        if (fundingOptional.isPresent()) {
            FundingResponseDTO fundingDTO = modelMapper.map(fundingOptional.get(), FundingResponseDTO.class);

            apiResponse.setResponse(fundingDTO);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAutographIdsByFundingId(Long id, String authorization) {
        Optional<Funding> fundingOptional = fundingRepository.findById(id);
        if (fundingOptional.isPresent()) {
            List<Long> autographIds = fundingOptional.get().getAutographList();

            apiResponse.setResponse(autographIds);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getTopFunderById(Long id, String authorization) {
        Optional<Funding> fundingOptional = fundingRepository.findById(id);
        Long topFounderId = -1L;
        Long topOwnerId = -1L;
        float topFunding = -1f;
        if (fundingOptional.isPresent()) {
            List<Sale> salesList = fundingOptional.get().getSales();
            for (Sale sale : salesList
            ) {
                if (sale.getPrice() * sale.getRoyalty() > topFunding) {
                    topFounderId = sale.getBuyerId();
                    topOwnerId = sale.getOwnerId();
                    topFunding = sale.getPrice() * sale.getRoyalty();
                }
            }

            if (topFounderId == -1L || topOwnerId == -1L || topFunding == -1L) {
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            } else {
                apiResponse.setResponse(topFunding);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse addAutographToFunding(Long autographId, Long fundingId, String authorization) {
        Optional<Funding> fundingOptional = fundingRepository.findById(fundingId);
        if (fundingOptional.isPresent()) {
            ApiResponse autographResponse = getAutographById(autographId, authorization);

            if (Objects.equals(autographResponse.getResponse(), null)) {
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            } else {
                AutographResponseDTO autographResponseDTO =
                        modelMapper.map(autographResponse.getResponse(),
                                AutographResponseDTO.class);

                Funding funding = fundingOptional.get();
                List<Long> autographList = funding.getAutographList();
                autographList.add(autographResponseDTO.getId());
                funding.setAutographList(autographList);

                FundingResponseDTO fundingDTO = modelMapper.map(fundingRepository.save(funding), FundingResponseDTO.class);
                apiResponse.setResponse(fundingDTO);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse editFundingDetails(EditFundingDTO editFundingDTO, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(editFundingDTO.getFundingId());

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            if (!Objects.equals(editFundingDTO.getFundingIcon(), null)) {
                FundingIcon fundingIcon = fundingIconsService.storeFile(editFundingDTO.getFundingIcon(), userId);
                funding.setIcon(fundingIcon);
            }
            if (!Objects.equals(editFundingDTO.getFundingProfileImage(), null)) {
                FundingProfileImage fundingProfileImage = fundingProfileImageService.storeFile(editFundingDTO.getFundingProfileImage(), userId);
                funding.setProfileImage(fundingProfileImage);
            }
            if (!Objects.equals(editFundingDTO.getFundingBackgroundImage(), null)) {
                FundingBackgroundImage fundingBackgroundImage = fundingBackgroundImageService.storeFile(editFundingDTO.getFundingBackgroundImage(), userId);
                funding.setBackgroundImage(fundingBackgroundImage);
            }

            if (!Objects.equals(editFundingDTO.getDescription(), null)) {
                funding.setDescription(editFundingDTO.getDescription());
            }
            if (!Objects.equals(editFundingDTO.getName(), null)) {
                funding.setName(editFundingDTO.getName());
            }
            if (!Objects.equals(editFundingDTO.getWebsite(), null)) {
                funding.setWebsite(editFundingDTO.getWebsite());
            }
            if (!Objects.equals(editFundingDTO.getPhoneNumber(), null)) {
                funding.setPhoneNumber(editFundingDTO.getPhoneNumber());
            }

            Funding fundingSaved = fundingRepository.save(funding);

            if (Objects.equals(fundingSaved, null)) {
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(false);
            } else {
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(modelMapper.map(fundingSaved, FundingResponseDTO.class));
            }
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    private ApiResponse getAutographById(Long autographId, String authorization) {
        ApiResponse response = webClientBuilder.build()
                .get()
                .uri("http://autograph-service/api/autograph/" + autographId)
                .header("Authorization", authorization)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();

        return response;
    }
}
