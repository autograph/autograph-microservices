package com.meelash.fundingservice.api.repository.funding;

import com.meelash.fundingservice.api.entity.FundingBackgroundImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IFundingBackgroundImageRepository extends JpaRepository<FundingBackgroundImage, Long> {
    Optional<FundingBackgroundImage> findById(Long id);
}
