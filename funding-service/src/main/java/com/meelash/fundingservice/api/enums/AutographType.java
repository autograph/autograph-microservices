package com.meelash.fundingservice.api.enums;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
