package com.meelash.fundingservice.api.repository.funding;

import com.meelash.fundingservice.api.entity.Funding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFundingRepository extends JpaRepository<Funding, Long> {
    @Override
    List<Funding> findAll();
}
