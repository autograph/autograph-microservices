package com.meelash.fundingservice.api.dto.response;

import lombok.Data;

@Data
public class FundingBackgroundImageResponseDTO {
    private Long id;
    private Long fundingId;
    private String fileName;
    private String documentType;
    private String uploadDirectory;
}
