package com.meelash.fundingservice.api.service.funding.impl;

import com.meelash.fundingservice.api.dto.request.AutographTransactionDTO;
import com.meelash.fundingservice.api.dto.request.FundingResponseDTO;
import com.meelash.fundingservice.api.entity.Funding;
import com.meelash.fundingservice.api.entity.Sale;
import com.meelash.fundingservice.api.enums.ResponseCodes;
import com.meelash.fundingservice.api.enums.SaleType;
import com.meelash.fundingservice.api.repository.funding.IFundingRepository;
import com.meelash.fundingservice.api.repository.funding.ISaleRepository;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.ISaleService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@Service
public class SaleService implements ISaleService {
    private final ModelMapper modelMapper;
    private final ISaleRepository saleRepository;
    private final IFundingRepository fundingRepository;
    private ApiResponse apiResponse;

    public SaleService(ModelMapper modelMapper, ISaleRepository saleRepository, IFundingRepository fundingRepository, ApiResponse apiResponse) {
        this.modelMapper = modelMapper;
        this.saleRepository = saleRepository;
        this.fundingRepository = fundingRepository;
        this.apiResponse = apiResponse;
    }

    @Override
    public ApiResponse createAutographTransactionRoyalty(Long fundId, AutographTransactionDTO autographTransationDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(fundId);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            Sale sale = new Sale(funding, autographTransationDTO);
            List<Sale> sales = funding.getSales();
            sales.add(sale);
            funding.setSales(sales);

            fundingRepository.save(funding);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(funding, FundingResponseDTO.class));
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse removeAutographFromFund(Long autographId, Long fundId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(fundId);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            Optional<Sale> saleOptional = saleRepository.findById(autographId);
            if (saleOptional.isPresent()) {
                List<Sale> sales = funding.getSales();
                sales.remove(saleOptional.get());
                funding.setSales(sales);

                fundingRepository.save(funding);

                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(modelMapper.map(funding, FundingResponseDTO.class));
            } else {
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(false);
            }
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getTotalFundingById(Long id, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(id);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            List<Sale> sales = funding.getSales();

            float totalFunding = 0f;

            for (Sale sale : sales
            ) {
                totalFunding += (sale.getPrice() * sale.getRoyalty());
            }


            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(totalFunding);
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getSecondaryFundingById(Long id, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(id);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            List<Sale> sales = funding.getSales();

            float secondaryFunding = 0f;

            for (Sale sale : sales
            ) {
                if (sale.getSaleType() == SaleType.SECONDARY)
                    secondaryFunding += (sale.getPrice() * sale.getRoyalty());
            }


            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(secondaryFunding);
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getFoundersByFundingId(Long id, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(id);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            List<Sale> sales = funding.getSales();

            ArrayList<Long> funderIds = new ArrayList<>();

            for (Sale sale : sales
            ) {
                funderIds.add(sale.getBuyerId());
            }


            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(funderIds);
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getNumberOfFundersById(Long id, String authorization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LinkedHashMap<String, Object> currentPrincipal = (LinkedHashMap<String, Object>)
                ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails();

        LinkedHashMap<String, Object> userDetails = (LinkedHashMap<String, Object>) currentPrincipal.get("userDetails");
        Long userId = ((Integer) userDetails.get("id")).longValue();

        Optional<Funding> fundingOptional = fundingRepository.findById(id);

        if (fundingOptional.isPresent() && fundingOptional.get().getOwnerId() == userId) {
            Funding funding = fundingOptional.get();

            List<Sale> sales = funding.getSales();

            Integer funders = sales.size();

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(funders);
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }
}
