package com.meelash.fundingservice.api.controller.funding;

import com.meelash.fundingservice.api.dto.request.EditFundingDTO;
import com.meelash.fundingservice.api.dto.request.FundingDTO;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.impl.FundingBackgroundImageService;
import com.meelash.fundingservice.api.service.funding.impl.FundingIconsService;
import com.meelash.fundingservice.api.service.funding.impl.FundingProfileImageService;
import com.meelash.fundingservice.api.service.funding.impl.FundingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class FundingController {

    private final FundingService fundingService;
    private final FundingProfileImageService fundingProfileImageService;
    private final FundingBackgroundImageService fundingBackgroundImageService;
    private final FundingIconsService fundingIconsService;
    private final ResourceServerTokenServices resourceServerTokenServices;

    @Autowired
    public FundingController(FundingService fundingService, FundingProfileImageService fundingProfileImageService, FundingBackgroundImageService fundingBackgroundImageService, FundingIconsService fundingIconsService, ResourceServerTokenServices resourceServerTokenServices) {
        this.fundingService = fundingService;
        this.fundingProfileImageService = fundingProfileImageService;
        this.fundingBackgroundImageService = fundingBackgroundImageService;
        this.fundingIconsService = fundingIconsService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }

    @PostMapping("/funding")
    public ApiResponse createFunding(@ModelAttribute FundingDTO fundingDTO, @RequestHeader("Authorization") String authorization) {
        return fundingService.createFunding(fundingDTO, authorization);
    }

    @PutMapping("/funding")
    public ApiResponse createFunding(@ModelAttribute EditFundingDTO fundingDTO, @RequestHeader("Authorization") String authorization) {
        return fundingService.editFundingDetails(fundingDTO, authorization);
    }

    @GetMapping("/funding")
    public ApiResponse getListOfFundings(@RequestHeader("Authorization") String authorization) {
        return fundingService.getListOfFundings(authorization);
    }

    @GetMapping("/funding/{id}")
    public ApiResponse getFundingDetailsById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return fundingService.getFundingDetailsById(id, authorization);
    }

    @GetMapping("/funding/{id}/autographs")
    public ApiResponse getAutographIdsByFundingId(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return fundingService.getAutographIdsByFundingId(id, authorization);
    }

    @GetMapping("/funding/{id}/top-founder")
    public ApiResponse getTopFunderById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return fundingService.getTopFunderById(id, authorization);
    }

    @PostMapping("/funding/{id}/autograph/{autographId}")
    public ApiResponse addAutographToFunding(@PathVariable("autographId") Long autographId, @PathVariable("id") Long fundingId, @RequestHeader("Authorization") String authorization) {
        return fundingService.addAutographToFunding(autographId, fundingId, authorization);
    }
}
