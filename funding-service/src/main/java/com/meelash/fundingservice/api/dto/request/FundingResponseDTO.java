package com.meelash.fundingservice.api.dto.request;

import lombok.Data;

@Data
public class FundingResponseDTO {
    private Long id;
    private String name;
    private String description;
    private String website;
    private String walletAddress;
    private String phoneNumber;
    private Long ownerId;

    private String fundingIconUploadDirectory;
    private String fundingIconFileName;
    private String fundingProfileImageUploadDirectory;
    private String fundingProfileImageFileName;
    private String fundingBackgroundImageUploadDirectory;
    private String fundingBackgroundImageFileName;
}
