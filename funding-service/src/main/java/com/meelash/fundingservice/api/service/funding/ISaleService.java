package com.meelash.fundingservice.api.service.funding;

import com.meelash.fundingservice.api.dto.request.AutographTransactionDTO;
import com.meelash.fundingservice.api.response.ApiResponse;

public interface ISaleService {
    ApiResponse createAutographTransactionRoyalty(Long fundId, AutographTransactionDTO autographTransationDTO);

    ApiResponse removeAutographFromFund(Long autographId, Long fundId);

    ApiResponse getTotalFundingById(Long id, String authorization);

    ApiResponse getSecondaryFundingById(Long id, String authorization);

    ApiResponse getFoundersByFundingId(Long id, String authorization);

    ApiResponse getNumberOfFundersById(Long id, String authorization);
}
