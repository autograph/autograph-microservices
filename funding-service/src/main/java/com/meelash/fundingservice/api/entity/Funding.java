package com.meelash.fundingservice.api.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Funding {
    @OneToMany(fetch = FetchType.EAGER)
    List<Sale> sales;
    @Id
    @GeneratedValue
    private Long id;
    private Long ownerId;
    private String walletAddress;
    private String name;
    private String description;
    private String website;
    private String phoneNumber;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "funding_icon",
            joinColumns =
                    {@JoinColumn(name = "funding_id", referencedColumnName = "id")},
            inverseJoinColumns =
                    {@JoinColumn(name = "icon_id", referencedColumnName = "id")})
    private FundingIcon icon;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "funding_profile_image",
            joinColumns =
                    {@JoinColumn(name = "funding_id", referencedColumnName = "id")},
            inverseJoinColumns =
                    {@JoinColumn(name = "profile_image_id", referencedColumnName = "id")})
    private FundingProfileImage profileImage;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "funding_profile_background",
            joinColumns =
                    {@JoinColumn(name = "funding_id", referencedColumnName = "id")},
            inverseJoinColumns =
                    {@JoinColumn(name = "background_image_id", referencedColumnName = "id")})
    private FundingBackgroundImage backgroundImage;
    @ElementCollection
    private List<Long> autographList = new ArrayList<>();

    @CreatedDate
    private Date createdAt;


    public Funding(Long ownerId, String walletAddress, String name, String description, String website, String phoneNumber, FundingIcon icon, FundingProfileImage profileImage, FundingBackgroundImage backgroundImage) {
        this.ownerId = ownerId;
        this.walletAddress = walletAddress;
        this.name = name;
        this.description = description;
        this.website = website;
        this.phoneNumber = phoneNumber;
        this.icon = icon;
        this.profileImage = profileImage;
        this.backgroundImage = backgroundImage;
        this.autographList = Collections.emptyList();
    }
}
