package com.meelash.fundingservice.api.service.funding;

import com.meelash.fundingservice.api.dto.request.EditFundingDTO;
import com.meelash.fundingservice.api.dto.request.FundingDTO;
import com.meelash.fundingservice.api.response.ApiResponse;

public interface IFundingService {
    ApiResponse createFunding(FundingDTO fundingDTO, String authorization);

    ApiResponse getListOfFundings(String authorization);

    ApiResponse getFundingDetailsById(Long id, String authorization);

    ApiResponse getAutographIdsByFundingId(Long id, String authorization);

    ApiResponse getTopFunderById(Long id, String authorization);

    ApiResponse addAutographToFunding(Long autographId, Long fundingId, String authorization);

    ApiResponse editFundingDetails(EditFundingDTO editFundingDTO, String authorization);
}
