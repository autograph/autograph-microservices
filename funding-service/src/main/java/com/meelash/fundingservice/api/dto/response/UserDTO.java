package com.meelash.fundingservice.api.dto.response;

import lombok.Data;

import java.util.ArrayList;


@Data
public class UserDTO {
    public int id;
    public String email;
    public String username;
    public String phoneNumber;
    public String address;
    public String name;
    public String surname;
    public Object qrCodeValue;
    public ArrayList<Object> socialMediaAccounts;
    public Object profilePictureFileName;
    public Object profilePictureUploadDirectory;
    public Object profilePictureId;
    public int unreadNotificationCount;
    public int followed;
    public int following;
    public String bio;
    public String userVerificationStatus;
    public boolean isBlocked;
    public Object autographCount;
}

@Data
class Authority {
    public String authority;
}

@Data
class DecodedDetails {
    public Object password;
    public String username;
    public ArrayList<Authority> authorities;
    public boolean accountNonExpired;
    public boolean accountNonLocked;
    public boolean credentialsNonExpired;
    public boolean enabled;
    public UserDTO userDetails;
}

@Data
class Root {
    public String remoteAddress;
    public Object sessionId;
    public String tokenValue;
    public String tokenType;
    public DecodedDetails decodedDetails;
}