package com.meelash.fundingservice.api.dto.request;

import lombok.Data;

@Data
public class AutographTransactionDTO {
    private Long ownerId;
    private Long buyerId;
    private Long autographId;
    private float price;
    private float royalty;
    private boolean isPrimaryTransaction;


}
