package com.meelash.fundingservice.api.entity;

import com.meelash.fundingservice.api.dto.request.AutographTransactionDTO;
import com.meelash.fundingservice.api.enums.SaleType;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Sale {
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Funding funding;
    @Id
    @GeneratedValue
    private Long id;
    private Long autographId;
    private Long ownerId, buyerId;
    private SaleType saleType;
    private float price, royalty;

    public Sale(Funding funding, Long autographId, Long ownerId, Long buyerId, SaleType saleType, float price, float royalty) {
        this.funding = funding;
        this.autographId = autographId;
        this.ownerId = ownerId;
        this.buyerId = buyerId;
        this.saleType = saleType;
        this.price = price;
        this.royalty = royalty;
    }

    public Sale(Funding funding, AutographTransactionDTO autographTransactionDTO) {
        this.funding = funding;
        this.autographId = autographTransactionDTO.getAutographId();
        this.ownerId = autographTransactionDTO.getOwnerId();
        this.buyerId = autographTransactionDTO.getBuyerId();
        this.saleType = autographTransactionDTO.isPrimaryTransaction() ? SaleType.PRIMARY : SaleType.SECONDARY;
        this.price = autographTransactionDTO.getPrice();
        this.royalty = autographTransactionDTO.getRoyalty();
    }
}
