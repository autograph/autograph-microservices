package com.meelash.fundingservice.api.controller.funding;

import com.meelash.fundingservice.api.dto.request.AutographTransactionDTO;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/funding")
public class SalesController {
    private final FundingService fundingService;
    private final SaleService saleService;
    private final FundingProfileImageService fundingProfileImageService;
    private final FundingBackgroundImageService fundingBackgroundImageService;
    private final FundingIconsService fundingIconsService;
    private final ResourceServerTokenServices resourceServerTokenServices;

    @Autowired
    public SalesController(FundingService fundingService, SaleService saleService, FundingProfileImageService fundingProfileImageService, FundingBackgroundImageService fundingBackgroundImageService, FundingIconsService fundingIconsService, ResourceServerTokenServices resourceServerTokenServices) {
        this.fundingService = fundingService;
        this.saleService = saleService;
        this.fundingProfileImageService = fundingProfileImageService;
        this.fundingBackgroundImageService = fundingBackgroundImageService;
        this.fundingIconsService = fundingIconsService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }

    @PostMapping("/{fundId}/sale")
    public ApiResponse autographTransactionRoyalty(@PathVariable("fundId") Long fundId, @RequestBody AutographTransactionDTO autographTransationDTO) {
        return saleService.createAutographTransactionRoyalty(fundId, autographTransationDTO);
    }

    @DeleteMapping("/{fundingId}/sale/autograph/{autographId}")
    public ApiResponse removeAutographFromFund(@PathVariable("fundingId") Long fundingId,
                                               @PathVariable("autographId") Long autographId) {
        return saleService.removeAutographFromFund(fundingId, autographId);
    }

    @GetMapping("/{id}/total-funding")
    public ApiResponse getTotalFundingById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return saleService.getTotalFundingById(id, authorization);
    }

    @GetMapping("/{id}/secondary-funding")
    public ApiResponse getSecondaryFundingById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return saleService.getSecondaryFundingById(id, authorization);
    }

    @GetMapping("/{id}/funders")
    public ApiResponse getFoundersByFundingId(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return saleService.getFoundersByFundingId(id, authorization);
    }

    @GetMapping("/{id}/total-funders")
    public ApiResponse getNumberOfFundersById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return saleService.getNumberOfFundersById(id, authorization);
    }
}
