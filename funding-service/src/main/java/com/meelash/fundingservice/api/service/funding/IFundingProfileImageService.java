package com.meelash.fundingservice.api.service.funding;

import com.meelash.fundingservice.api.entity.FundingProfileImage;
import com.meelash.fundingservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IFundingProfileImageService {
    FundingProfileImage storeFile(MultipartFile file, Long id);

    ApiResponse listAllProfileImages();

    ApiResponse getProfileImageById(Long id);
}
