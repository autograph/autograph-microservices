package com.meelash.fundingservice.api.service.funding.impl;

import com.meelash.fundingservice.api.config.FileStorageProperties;
import com.meelash.fundingservice.api.dto.response.FundingBackgroundImageResponseDTO;
import com.meelash.fundingservice.api.entity.FundingBackgroundImage;
import com.meelash.fundingservice.api.enums.ResponseCodes;
import com.meelash.fundingservice.api.exception.AttachmentStorageException;
import com.meelash.fundingservice.api.repository.funding.IFundingBackgroundImageRepository;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.IFundingBackgroundImageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FundingBackgroundImageService implements IFundingBackgroundImageService {
    private final ModelMapper modelMapper;
    private final Path fileStoragePath;
    private final IFundingBackgroundImageRepository fundingBackgroundImageRepository;

    @Autowired
    public FundingBackgroundImageService(ModelMapper modelMapper, FileStorageProperties fileStorageProperties, IFundingBackgroundImageRepository fundingBackgroundImageRepository) {
        this.modelMapper = modelMapper;
        this.fundingBackgroundImageRepository = fundingBackgroundImageRepository;

        this.fileStoragePath = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStoragePath);
        } catch (Exception ex) {
            throw new AttachmentStorageException(
                    "Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public FundingBackgroundImage storeFile(MultipartFile file, Long id) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new AttachmentStorageException(
                        "Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExtension = getExtensionByStringHandling(fileName).get();

            Path targetLocationWithUserId = this.fileStoragePath.resolve(Long
                    .toString(id))
                    .resolve("funding_background_image");

            if (!Files.exists(targetLocationWithUserId)) {
                Files.createDirectories(targetLocationWithUserId);
            }

            int fileCount = Objects
                    .requireNonNull(new File(targetLocationWithUserId.toString()).list()).length + 1;
            String newFileName = fileCount + "." + fileExtension;
            Path targetLocation = targetLocationWithUserId.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

            return new FundingBackgroundImage(newFileName,
                    mimetypesFileTypeMap.getContentType(file.getName()),
                    this.fileStoragePath.toString());
        } catch (IOException ex) {
            throw new AttachmentStorageException(
                    "Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public ApiResponse listAllBackgrounds() {
        ApiResponse apiResponse = new ApiResponse();

        List<FundingBackgroundImage> attachments = fundingBackgroundImageRepository.findAll();
        if (!attachments.isEmpty()) {
            List<FundingBackgroundImageResponseDTO> attachmentResponseDTOS = attachments
                    .stream()
                    .map(attachment -> modelMapper.map(attachment, FundingBackgroundImageResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTOS);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getBackgroundById(Long id) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<FundingBackgroundImage> attachmentOptional = fundingBackgroundImageRepository.findById(id);
        if (attachmentOptional.isPresent()) {
            FundingBackgroundImage attachment = attachmentOptional.get();
            FundingBackgroundImageResponseDTO attachmentResponseDTO = modelMapper.map(attachment,
                    FundingBackgroundImageResponseDTO.class);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTO);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }


    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
