package com.meelash.fundingservice.api.repository.funding;

import com.meelash.fundingservice.api.entity.FundingIcon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IIconRepository extends JpaRepository<FundingIcon, Long> {
    Optional<FundingIcon> findById(Long id);
}
