package com.meelash.fundingservice.api.service.funding.impl;

import com.meelash.fundingservice.api.config.FileStorageProperties;
import com.meelash.fundingservice.api.dto.response.FundingIconResponseDTO;
import com.meelash.fundingservice.api.entity.FundingIcon;
import com.meelash.fundingservice.api.enums.ResponseCodes;
import com.meelash.fundingservice.api.exception.AttachmentStorageException;
import com.meelash.fundingservice.api.repository.funding.IIconRepository;
import com.meelash.fundingservice.api.response.ApiResponse;
import com.meelash.fundingservice.api.service.funding.IFundingIconService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FundingIconsService implements IFundingIconService {
    private final IIconRepository iconRepository;
    private final ModelMapper modelMapper;
    private final Path fileStoragePath;

    @Autowired
    public FundingIconsService(IIconRepository iconRepository, ModelMapper modelMapper,  FileStorageProperties fileStorageProperties) {
        this.iconRepository = iconRepository;
        this.modelMapper = modelMapper;
        this.fileStoragePath = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStoragePath);
        } catch (Exception ex) {
            throw new AttachmentStorageException(
                    "Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public FundingIcon storeFile(MultipartFile file, Long id) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new AttachmentStorageException(
                        "Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExtension = getExtensionByStringHandling(fileName).get();

            Path targetLocationWithUserId = this.fileStoragePath.resolve(Long
                    .toString(id))
                    .resolve("funding_icons");

            if (!Files.exists(targetLocationWithUserId)) {
                Files.createDirectories(targetLocationWithUserId);
            }

            int fileCount = Objects
                    .requireNonNull(new File(targetLocationWithUserId.toString()).list()).length + 1;
            String newFileName = fileCount + "." + fileExtension;
            Path targetLocation = targetLocationWithUserId.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

            return new FundingIcon(newFileName,
                    mimetypesFileTypeMap.getContentType(file.getName()),
                    this.fileStoragePath.toString());
        } catch (IOException ex) {
            throw new AttachmentStorageException(
                    "Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public ApiResponse listAllIcons() {
        ApiResponse apiResponse = new ApiResponse();

        List<FundingIcon> attachments = iconRepository.findAll();
        if (!attachments.isEmpty()) {
            List<FundingIconResponseDTO> attachmentResponseDTOS = attachments
                    .stream()
                    .map(attachment -> modelMapper.map(attachment, FundingIconResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTOS);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getIconById(Long id) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<FundingIcon> attachmentOptional = iconRepository.findById(id);
        if (attachmentOptional.isPresent()) {
            FundingIcon attachment = attachmentOptional.get();
            FundingIconResponseDTO attachmentResponseDTO = modelMapper.map(attachment,
                    FundingIconResponseDTO.class);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTO);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
