package com.meelash.fundingservice.api.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FundingDTO {
    private String name;
    private String description;
    private String website;
    private String walletAddress;
    private String phoneNumber;

    private MultipartFile fundingIcon;
    private MultipartFile fundingProfileImage;
    private MultipartFile fundingBackgroundImage;
}
