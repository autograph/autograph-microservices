package com.autograph.autographservice.api.entity.autograph;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "report_attachments")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportAttachment {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "attachment")
    private Autograph autograph;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "upload_directory")
    private String uploadDirectory;

    public ReportAttachment(String name, String uploadDirectory) {
        this.fileName = name;
        this.uploadDirectory = uploadDirectory;
    }

    public String getPath(Long autographId) {
        String separator = System.getProperty("file.separator");
        return uploadDirectory + separator + "reports" + autographId + separator
                + fileName;
    }
}
