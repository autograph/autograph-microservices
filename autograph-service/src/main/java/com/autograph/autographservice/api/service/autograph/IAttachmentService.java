package com.autograph.autographservice.api.service.autograph;

import com.autograph.autographservice.api.entity.autograph.Attachment;
import com.autograph.autographservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IAttachmentService {

    Attachment storeFile(MultipartFile file, Long id);

    ApiResponse listAllAttachments();

    ApiResponse getAttachmentById(Long id);

}
