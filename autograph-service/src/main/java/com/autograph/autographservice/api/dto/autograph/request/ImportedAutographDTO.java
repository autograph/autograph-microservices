package com.autograph.autographservice.api.dto.autograph.request;

import com.autograph.autographservice.api.entity.autograph.Metadata;
import com.autograph.autographservice.api.enums.AutographType;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
public class ImportedAutographDTO {

    @NotNull
    private String title;
    private String description;
    private String externalLink;
    private String lockableExtras;
    private MultipartFile file;
    @NotNull
    private AutographType autographType;
    private BigInteger tokenId;
    private String animationUrl;
    private String animationOriginalUrl;
    private String address;
    private String assetContractType;
    private String buyerFeeBasisPoints;
    private BigInteger openseaBuyerFeeBasisPoints;
    private BigInteger openseaSellerFeeBasisPoints;
    private LocalDateTime createdAt;
    private String name;
    private String nftVersion;
    private String openseaVersion;
    private String onlyProxiedTransfers;
    private BigInteger ownerId;
    private String payoutAddress;
    private String schemaName;
    private String totalSupply;
    private String ownerAddress, creatorAddress;
    private String transferFee;
    private Metadata metadata;

}
