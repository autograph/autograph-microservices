package com.autograph.autographservice.api.dto.autograph.request;

import com.autograph.autographservice.api.enums.MintType;
import lombok.Data;

@Data
public class AutographSaleIdentityDTO {

    private MintType mintType;

    private String author;
    private double royaltiesPercentage;
    private double price;

    private String erc721ContractAddress;
    private String intermediaryContractAddress;

    private String signatureLazyMint;
    private String signatureLazyDeal;

    private String tokenId;
    private String signatureDeal;

    // TODO: Add identities for bidding.
}
