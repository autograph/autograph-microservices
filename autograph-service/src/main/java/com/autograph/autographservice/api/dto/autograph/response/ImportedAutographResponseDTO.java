package com.autograph.autographservice.api.dto.autograph.response;

import com.autograph.autographservice.api.enums.AutographType;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@Builder
public class ImportedAutographResponseDTO {

    @NotNull
    private String title;
    private String description;
    private String externalLink;
    private String lockableExtras;
    private AutographType autographType;
    private String tokenId;
    private String animationUrl;
    private String animationOriginalUrl;
    private String address;
    private String assetContractType;
    private String buyerFeeBasisPoints;
    private BigInteger openseaBuyerFeeBasisPoints;
    private BigInteger openseaSellerFeeBasisPoints;
    private LocalDateTime createdAt;
    private String name;
    private String nftVersion;
    private String openseaVersion;
    private String onlyProxiedTransfers;
    private BigInteger ownerId;
    private String payoutAddress;
    private String schemaName;
    private String totalSupply;
    private String ownerAddress, creatorAddress;
    private String transferFee;
    private Boolean isLiked, isBlocked, isVerified, isDeleted;

    private String attachmentUploadDirectory;
    private String attachmentFileName;
    private Collection<Long> previousOwners;
    private Long numberOfLikes;
}
