package com.autograph.autographservice.api.dto.autograph.response;

import com.autograph.autographservice.api.enums.AutographType;
import com.autograph.autographservice.api.enums.PurchaseType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AuctionResponseDTO {

    private Long auctionId;
    private Long id;
    private LocalDateTime createdAt;
    private String title;
    private String description;
    private String externalLink;
    private Long creator;
    private Long owner;
    private Long attachmentId;
    private String attachmentUploadDirectory;
    private String attachmentFileName;
    private AutographType autographType;
    private Long numberOfLikes;
    private BigDecimal auctionPrice;
    private LocalDateTime auctionStartDate;
    private LocalDateTime auctionEndDate;
    private LocalDateTime auctionCreatedDate;
    private PurchaseType purchaseType;
}
