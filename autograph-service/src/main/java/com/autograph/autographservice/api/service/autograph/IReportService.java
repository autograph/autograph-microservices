package com.autograph.autographservice.api.service.autograph;
import com.autograph.autographservice.api.dto.autograph.request.ReportAutographRequestDTO;
import com.autograph.autographservice.api.dto.autograph.request.ReportAutographWithoutAttachmentRequestDTO;
import com.autograph.autographservice.api.enums.ReportReason;
import com.autograph.autographservice.api.response.ApiResponse;

public interface IReportService {
    ApiResponse getReportById(Long reportId);
    ApiResponse getReportsByAutographId(Long reportId, int page);
    ApiResponse getReportsByUserId(Long reportId, int page);
    ApiResponse reportAutographById(ReportAutographRequestDTO reportAutographRequestDTO, Long userId);
    ApiResponse getAllReports(int page);
    ApiResponse getReportByReason(ReportReason reason, int page);
    ApiResponse reportAutographWithoutAttachmentById(ReportAutographWithoutAttachmentRequestDTO reportAutographRequestDTO, Long userId);
}
