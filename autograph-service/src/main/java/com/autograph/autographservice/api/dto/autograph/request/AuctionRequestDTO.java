package com.autograph.autographservice.api.dto.autograph.request;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AuctionRequestDTO {

    private Long id;
    private Long autographId;
    private BigDecimal price;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private LocalDateTime createdDate;

}
