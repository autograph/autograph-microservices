package com.autograph.autographservice.api.dto.category.request;

import lombok.Data;

@Data
public class CategoryRequestDTO {

    String name;
}
