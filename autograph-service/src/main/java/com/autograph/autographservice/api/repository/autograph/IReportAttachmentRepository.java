package com.autograph.autographservice.api.repository.autograph;

import com.autograph.autographservice.api.entity.autograph.Report;
import com.autograph.autographservice.api.entity.autograph.ReportAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IReportAttachmentRepository extends JpaRepository<ReportAttachment, Report> {
    Optional<ReportAttachment> findById(Long id);
}
