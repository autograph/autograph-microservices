package com.autograph.autographservice.api.dto.autograph.response;

import com.autograph.autographservice.api.dto.autograph.request.AutographSaleIdentityDTO;
import com.autograph.autographservice.api.enums.AutographType;
import com.autograph.autographservice.api.enums.PurchaseType;
import com.autograph.autographservice.api.enums.ReportReason;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

@Data
@Getter
@Setter
public class ReportAutographResponseDTO {
    private Long autographId;
    private Long userId;
    private ReportReason reason;
    private String comment;
    private MultipartFile file;
    private String reportAttachmentUploadDirectory;
    private String reportAttachmentFileName;

    private String autographTitle;
    private String autographDescription;
    private String autographExternalLink;
    private Long autographCreator;
    private Long autographOwner;
    private Long autographAttachmentId;
    private String autographAttachmentUploadDirectory;
    private String autographAttachmentFileName;
    private AutographType autographAutographType;
    private Long autographNumberOfLikes;
    private PurchaseType autographPurchaseType;
    private AutographSaleIdentityDTO autographIdentity;
    private BigDecimal autographCurrentPrice;
    private Boolean autographIsLiked, autographIsBlocked, autographIsVerified, autographIsDeleted;
    private Long autographAuctionId;
    private MetadataDTO autographMetadata;
}
