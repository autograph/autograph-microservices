package com.autograph.autographservice.api.dto.category.response;

import lombok.Data;

@Data
public class CategoryResponseDTO {

    Long id;
    String name;
}
