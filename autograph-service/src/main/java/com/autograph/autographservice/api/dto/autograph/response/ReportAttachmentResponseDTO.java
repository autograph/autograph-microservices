package com.autograph.autographservice.api.dto.autograph.response;

public class ReportAttachmentResponseDTO {
    private Long id;
    private Long autographId;
    private Long userId;
    private String fileName;
    private String documentType;
    private String uploadDirectory;
}
