package com.autograph.autographservice.api.controller.autograph;

import com.autograph.autographservice.api.dto.autograph.request.ReportAutographRequestDTO;
import com.autograph.autographservice.api.dto.autograph.request.ReportAutographWithoutAttachmentRequestDTO;
import com.autograph.autographservice.api.enums.ReportReason;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.impl.ReportAttachmentService;
import com.autograph.autographservice.api.service.autograph.impl.ReportService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin
@RestController
@RequestMapping("/api/autograph")
public class ReportController {
    private final ResourceServerTokenServices resourceServerTokenServices;
    private final ReportService reportService;
    private final ReportAttachmentService reportAttachmentService;

    public ReportController(ResourceServerTokenServices resourceServerTokenServices, ReportService reportService, ReportAttachmentService reportAttachmentService) {
        this.resourceServerTokenServices = resourceServerTokenServices;
        this.reportService = reportService;
        this.reportAttachmentService = reportAttachmentService;
    }

    @PostMapping("/{autographId}/report")
    public ApiResponse reportAutograph(@ModelAttribute ReportAutographRequestDTO reportDTO,
                                    @RequestHeader("Authorization") String authorization,
                                    @PathVariable("autographId") Long autographId) {
        Long userId = getUserId(authorization);
        reportDTO.setAutographId(autographId);
        return reportService.reportAutographById(reportDTO, userId);
    }

    @PostMapping("/{autographId}/report/without-attachment")
    public ApiResponse reportAutograph(@ModelAttribute ReportAutographWithoutAttachmentRequestDTO reportDTO,
                                       @RequestHeader("Authorization") String authorization,
                                       @PathVariable("autographId") Long autographId) {
        Long userId = getUserId(authorization);
        reportDTO.setAutographId(autographId);
        return reportService.reportAutographWithoutAttachmentById(reportDTO, userId);
    }

    @GetMapping("/{autographId}/report/{page}")
    public ApiResponse getAutographReports(@PathVariable("autographId") Long autographId, @PathVariable("page") int page) {
        return reportService.getReportsByAutographId(autographId, page);
    }

    @GetMapping("report/user/{userId}/{page}")
    public ApiResponse getReportByUserId(@PathVariable("userId") Long userId, @PathVariable("page") int page) {
        return reportService.getReportsByUserId(userId, page);
    }

    @GetMapping("report/{reportId}")
    public ApiResponse getReportById(@PathVariable("reportId") Long reportId) {
        return reportService.getReportById(reportId);
    }

    @GetMapping("reports/{page}")
    public ApiResponse getReports(@PathVariable("page") int page) {
        return reportService.getAllReports(page);
    }

    @GetMapping("report/reason/{reason}/{page}")
    public ApiResponse getAutographReportsByReason(@PathVariable("reason") ReportReason reason, @PathVariable("page") int page) {
        return reportService.getReportByReason(reason, page);
    }

    @GetMapping("report/attachment/{attachmentId}")
    public ApiResponse getReportAttachments(@PathVariable("attachmentId") Long attachmentId) {
        return reportAttachmentService.getAttachmentById(attachmentId);
    }

    @GetMapping("report/attachment/")
    public ApiResponse getReportAttachments() {
        return reportAttachmentService.listAllAttachments();
    }

    HashMap<String, Object> getUserDetails(String token) {
        if (!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token) {
        HashMap<String, Object> userDetails = getUserDetails(token);

        if (userDetails != null) {
            return ((Integer) userDetails.get("id")).longValue();
        } else {
            return null;
        }
    }
}
