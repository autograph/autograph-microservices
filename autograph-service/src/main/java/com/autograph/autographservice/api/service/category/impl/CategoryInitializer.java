package com.autograph.autographservice.api.service.category.impl;

import com.autograph.autographservice.api.entity.category.Category;
import com.autograph.autographservice.api.repository.category.ICategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CategoryInitializer implements CommandLineRunner {

    private final ICategoryRepository categoryRepository;

    @Autowired
    public CategoryInitializer(
        ICategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (categoryRepository.findAll().isEmpty()) {
            List<Category> categoryList = Arrays.asList(
                new Category("Automotive (Cars, Trucks, Racing)"),
                new Category("Digital Creators"),
                new Category("Entertainment & Pop Culture"),
                new Category("Financial & Business News")
            );

            categoryRepository.saveAll(categoryList);
        }
    }
}
