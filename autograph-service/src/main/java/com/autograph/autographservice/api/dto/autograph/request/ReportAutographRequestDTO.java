package com.autograph.autographservice.api.dto.autograph.request;

import com.autograph.autographservice.api.enums.ReportReason;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Data
@Getter
@Setter
public class ReportAutographRequestDTO {
    Long autographId;
    ReportReason reason;
    String comment;
    MultipartFile file;
}
