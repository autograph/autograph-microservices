package com.autograph.autographservice.api.repository.category;

import com.autograph.autographservice.api.entity.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findAll();

    Optional<Category> findByName(String name);

}
