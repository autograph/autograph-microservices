package com.autograph.autographservice.api.enums;

public enum SearchType {
    USER,
    AUTOGRAPH
}
