package com.autograph.autographservice.api.repository.autograph;

import com.autograph.autographservice.api.entity.autograph.Report;
import com.autograph.autographservice.api.enums.ReportReason;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IReportRepository extends JpaRepository<Report, Long> {
    Optional<Report> findById(Long id);

    List<Report> findByUserId(Long userId, Pageable pageable);

    List<Report> findByReason(ReportReason reason, Pageable pageable);

    List<Report> findByAutographId(Long userId, Pageable pageable);
}
