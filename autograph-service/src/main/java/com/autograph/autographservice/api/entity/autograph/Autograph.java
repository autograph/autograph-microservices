package com.autograph.autographservice.api.entity.autograph;

import com.autograph.autographservice.api.entity.category.Category;
import com.autograph.autographservice.api.enums.AutographState;
import com.autograph.autographservice.api.enums.AutographType;
import com.autograph.autographservice.api.enums.PurchaseType;
import lombok.*;
import lombok.ToString.Exclude;
import org.hibernate.Hibernate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "autographs")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Autograph {

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "autograph_asset_contract",
        joinColumns =
            {@JoinColumn(name = "autograph_id", referencedColumnName = "id")},
        inverseJoinColumns =
            {@JoinColumn(name = "asset_id", referencedColumnName = "id")})
    AssetContract assetContract;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    Set<Long> likes;
    @Id
    @GeneratedValue
    private Long id;
    @CreatedDate
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String title;
    private String description;
    private String externalLink;
    private String lockableExtras;
    private AutographState state = AutographState.SUBMITTED;
    private AutographType autographType;
    private PurchaseType purchaseType;
    private Boolean isVerified = false, isBlocked = false, isDeleted = false;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "autograph_sale_identity",
        joinColumns =
            {@JoinColumn(name = "autograph_id", referencedColumnName = "id")},
        inverseJoinColumns =
            {@JoinColumn(name = "sale_identity_id", referencedColumnName = "id")})
    private AutographSaleIdentity saleIdentity;

    private Long numberOfLikes = 0L;
    private boolean imported = false;
    private Long owner;
    private Long creator;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    private Set<Long> previousOwners = new HashSet<>();
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
        name = "categories_autographs",
        joinColumns = @JoinColumn(
            name = "autograph_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "category_id", referencedColumnName = "id"))
    @Exclude
    private Collection<Category> categories = new HashSet<>();
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "autograph_attachment",
        joinColumns =
            {@JoinColumn(name = "autograph_id", referencedColumnName = "id")},
        inverseJoinColumns =
            {@JoinColumn(name = "attachment_id", referencedColumnName = "id")})
    private Attachment attachment;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "autograph_metadata",
        joinColumns =
            {@JoinColumn(name = "autograph_id", referencedColumnName = "id")},
        inverseJoinColumns =
            {@JoinColumn(name = "metadata_id", referencedColumnName = "id")})
    private Metadata metadata;

    private Long auctionId;

    public Autograph(
        LocalDateTime createdAt,
        String title,
        String description,
        String externalLink,
        String lockableExtras,
        AutographType autographType,
        Long owner, Long creator,
        Attachment attachment,
        Metadata metadata) {
        this.createdAt = createdAt;
        this.title = title;
        this.description = description;
        this.externalLink = externalLink;
        this.lockableExtras = lockableExtras;
        this.autographType = autographType;
        this.owner = owner;
        this.creator = creator;
        this.attachment = attachment;
        this.numberOfLikes = 0L;
        this.purchaseType = PurchaseType.NOT_FOR_SALE;
        this.likes = new HashSet<>();
        this.auctionId = null;
        this.metadata = metadata;
        this.saleIdentity = null;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(
            o)) {
            return false;
        }
        Autograph autograph = (Autograph) o;
        return Objects.equals(id, autograph.id);
    }
}
