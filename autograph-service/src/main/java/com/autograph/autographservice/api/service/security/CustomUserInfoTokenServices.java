package com.autograph.autographservice.api.service.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

import java.util.*;

public class CustomUserInfoTokenServices implements ResourceServerTokenServices {

    private static final String[] PRINCIPAL_KEYS = new String[]{"user", "username",
        "userid", "user_id", "login", "id", "name"};
    protected final Log logger = LogFactory.getLog(getClass());
    private final String userInfoEndpointUrl;
    private final String clientId;
    private final String tokenType = DefaultOAuth2AccessToken.BEARER_TYPE;
    private final AuthoritiesExtractor authoritiesExtractor = new FixedAuthoritiesExtractor();
    private OAuth2RestOperations restTemplate;


    public CustomUserInfoTokenServices(String userInfoEndpointUrl, String clientId) {
        logger.warn("Constructor call authentication: " + clientId + " " + userInfoEndpointUrl);

        this.userInfoEndpointUrl = userInfoEndpointUrl;
        this.clientId = clientId;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String accessToken)
        throws InvalidTokenException {
        logger.warn("loadAuthentication call: " + accessToken);

        Map<String, Object> map = getMap(this.userInfoEndpointUrl, accessToken);
        logger.info("Load authentication: " + map);
        if (map.containsKey("error")) {
            this.logger.warn("userinfo returned error: " + map.get("error"));
            throw new InvalidTokenException(accessToken);
        }
        return extractAuthentication(map);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }

    private OAuth2Authentication extractAuthentication(Map<String, Object> map) {
        logger.warn("extractAuthentication call: " + map);

        Object principal = getPrincipal(map);
        OAuth2Request request = getRequest(map);
        List<GrantedAuthority> authorities = this.authoritiesExtractor
            .extractAuthorities(map);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
            principal, "N/A", authorities);
        token.setDetails(map);
        logger.info("Extract authentication: " + map);

        token.setDetails(((Map<String, Object>) map.get("userAuthentication")).get("principal"));

        OAuth2Authentication authentication = new OAuth2Authentication(request, token);
        authentication.setDetails(
            ((Map<String, Object>) map.get("userAuthentication")).get("principal"));

        return authentication;
    }

    private Object getPrincipal(Map<String, Object> map) {
        for (String key : PRINCIPAL_KEYS) {
            if (map.containsKey(key)) {
                return map.get(key);
            }
        }
        return "unknown";
    }

    @SuppressWarnings({"unchecked"})
    private OAuth2Request getRequest(Map<String, Object> map) {
        logger.info("Request map: " + map);
        Map<String, Object> request = (Map<String, Object>) map.get("oauth2Request");

        String clientId = (String) request.get("clientId");
        Set<String> scope = new LinkedHashSet<>(request.containsKey("scope") ?
            (Collection<String>) request.get("scope") : Collections.emptySet());

        return new OAuth2Request(null, clientId, null, true, new HashSet<>(scope),
            null, null, null, null);
    }

    @SuppressWarnings({"unchecked"})
    private Map<String, Object> getMap(String path, String accessToken) {
        this.logger.warn("Getting user info from: " + path);
        try {
            OAuth2RestOperations restTemplate = this.restTemplate;
            if (restTemplate == null) {
                BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
                resource.setClientId(this.clientId);
                restTemplate = new OAuth2RestTemplate(resource);
            }
            OAuth2AccessToken existingToken = restTemplate.getOAuth2ClientContext()
                .getAccessToken();
            if (existingToken == null || !accessToken.equals(existingToken.getValue())) {
                DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(
                    accessToken);
                token.setTokenType(this.tokenType);
                restTemplate.getOAuth2ClientContext().setAccessToken(token);
            }
            return restTemplate.getForEntity(path, Map.class).getBody();
        } catch (Exception ex) {
            this.logger.info("Could not fetch user details: " + ex.getClass() + ", "
                + ex.getMessage());
            return Collections.singletonMap("error",
                "Could not fetch user details");
        }
    }


}
