package com.autograph.autographservice.api.service.category.impl;

import com.autograph.autographservice.api.dto.autograph.response.AutographResponseDTO;
import com.autograph.autographservice.api.dto.category.request.CategoryRequestDTO;
import com.autograph.autographservice.api.dto.category.response.CategoryResponseDTO;
import com.autograph.autographservice.api.entity.autograph.Autograph;
import com.autograph.autographservice.api.entity.category.Category;
import com.autograph.autographservice.api.enums.ResponseCodes;
import com.autograph.autographservice.api.repository.autograph.IAutographRepository;
import com.autograph.autographservice.api.repository.category.ICategoryRepository;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.category.ICategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService implements ICategoryService {

    private final ModelMapper modelMapper;
    private final ICategoryRepository categoryRepository;
    private final IAutographRepository autographRepository;
    private final ApiResponse apiResponse;
    private final int numberOfItemsOnPage = 10;


    @Autowired
    public CategoryService(ModelMapper modelMapper,
        ICategoryRepository categoryRepository, IAutographRepository autographRepository,
        ApiResponse apiResponse) {
        this.modelMapper = modelMapper;
        this.categoryRepository = categoryRepository;
        this.autographRepository = autographRepository;
        this.apiResponse = apiResponse;
    }

    @Override
    public ApiResponse createCategory(CategoryRequestDTO categoryRequestDTO) {
        Optional<Category> categoryOptional = categoryRepository
            .findByName(categoryRequestDTO.getName());
        if (categoryOptional.isPresent()) {
            apiResponse.setCode(ResponseCodes.CATEGORY_ALREADY_EXISTS.getValue());
            apiResponse.setResponse(false);
        } else {
            Category category = new Category(categoryRequestDTO.getName());
            categoryRepository.save(category);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(true);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse deleteCategory(Long id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();

            categoryRepository.delete(category);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(category, CategoryResponseDTO.class));
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.CATEGORY_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getListOfCategories() {
        List<Category> categoryList = categoryRepository.findAll();
        List<CategoryResponseDTO> categoryResponseDTOS =
            categoryList
                .stream()
                .map(category -> modelMapper.map(category, CategoryResponseDTO.class))
                .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(categoryResponseDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse getListOfCategoriesByListOfIds(Collection<Long> idList) {
        Collection<CategoryResponseDTO> categoryResponseDTOS = idList
            .stream()
            .map(id -> modelMapper.map(categoryRepository.findById(id).get(),
                CategoryResponseDTO.class))
            .collect(Collectors.toList());

        apiResponse.setResponse(categoryResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse addCategoryToAutograph(Long categoryId, Long itemId) {
        ApiResponse apiResponse = new ApiResponse();
        Optional<Autograph> autographOptional = autographRepository.findById(itemId);
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

        if (autographOptional.isPresent()) {
            if (categoryOptional.isPresent()) {
                Autograph autograph = autographOptional.get();
                Category category = categoryOptional.get();

                Collection<Category> categories = autograph.getCategories();
                categories.add(category);
                autograph.setCategories(categories);
                autographRepository.save(autograph);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(modelMapper.map(autograph, AutographResponseDTO.class));
            } else {
                apiResponse.setResponse(null);
                apiResponse.setCode(ResponseCodes.CATEGORY_NOT_FOUND.getValue());
            }
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse addCategoriesToAutograph(List<Long> categoryIds, Long autographId) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<Autograph> autographOptional = autographRepository.findById(autographId);
        if (autographOptional.isPresent()) {
            Autograph autograph = autographOptional.get();
            Collection<Category> categories = autograph.getCategories();

            categoryIds.stream()
                .forEach(id -> {
                    Optional<Category> categoryOptional = categoryRepository.findById(id);

                    if (categoryOptional.isPresent()) {
                        Category category = categoryOptional.get();

                        categories.add(category);
                    }
                });

            autograph.setCategories(categories);
            autographRepository.save(autograph);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(autograph, AutographResponseDTO.class));
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse setCategoriesToAutograph(List<Long> categoryIds, Long autographId) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<Autograph> autographOptional = autographRepository.findById(autographId);
        if (autographOptional.isPresent()) {
            Autograph autograph = autographOptional.get();
            Collection<Category> categories = new ArrayList<>();

            categoryIds.stream()
                    .forEach(id -> {
                        Optional<Category> categoryOptional = categoryRepository.findById(id);

                        if (categoryOptional.isPresent()) {
                            Category category = categoryOptional.get();

                            categories.add(category);
                        }
                    });

            autograph.setCategories(categories);
            autographRepository.save(autograph);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(autograph, AutographResponseDTO.class));
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }
}
