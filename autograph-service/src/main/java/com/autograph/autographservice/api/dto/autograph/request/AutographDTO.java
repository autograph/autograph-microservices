package com.autograph.autographservice.api.dto.autograph.request;

import com.autograph.autographservice.api.enums.AutographType;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
public class AutographDTO {

    @NotNull
    private String title;
    private String description;
    private String externalLink;
    private String lockableExtras;
    private MultipartFile file;
    private boolean imported;
    @NotNull
    private AutographType autographType;
}
