package com.autograph.autographservice.api.entity.autograph;

import com.autograph.autographservice.api.enums.ReportReason;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Report {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    Autograph autograph;

    @NotNull
    Long userId;

    @NotNull
    ReportReason reason;
    String comment;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "report_attachment",
            joinColumns =
                    {@JoinColumn(name = "autograph_id", referencedColumnName = "id")},
            inverseJoinColumns =
                    {@JoinColumn(name = "attachment_id", referencedColumnName = "id")})
    private ReportAttachment attachment;

    public Report(Autograph autograph, Long userId, ReportReason reason, String comment, ReportAttachment attachment) {
        this.autograph = autograph;
        this.userId = userId;
        this.reason = reason;
        this.comment = comment;
        this.attachment = attachment;
    }
}
