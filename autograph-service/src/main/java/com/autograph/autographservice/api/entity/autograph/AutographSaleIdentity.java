package com.autograph.autographservice.api.entity.autograph;

import com.autograph.autographservice.api.enums.MintType;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "sale_identity")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AutographSaleIdentity {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "saleIdentity")
    private Autograph autograph;

    @Column(name = "mint_type")
    private MintType mintType;

    @Column(name = "author")
    private String author;
    @Column(name = "royalties_percentage")
    private double royaltiesPercentage;
    @Column(name = "price")
    private double price;

    // common fields
    @Column(name = "erc721_contract_address")
    private String erc721ContractAddress;
    @Column(name = "intermediary_contract_address")
    private String intermediaryContractAddress;

    // lazy-mint fixed price fields
    @Column(name = "signature_lazy_mint")
    private String signatureLazyMint;
    @Column(name = "signature_lazy_deal")
    private String signatureLazyDeal;

    // pre-minted fixed price fields
    @Column(name = "token_id")
    private String tokenId;
    @Column(name = "signature_deal")
    private String signatureDeal;
}
