package com.autograph.autographservice.api.service.autograph.impl;

import com.autograph.autographservice.api.dto.autograph.request.AuctionRequestDTO;
import com.autograph.autographservice.api.dto.autograph.request.AutographDTO;
import com.autograph.autographservice.api.dto.autograph.request.ImportedAutographDTO;
import com.autograph.autographservice.api.dto.autograph.request.SaleDTO;
import com.autograph.autographservice.api.dto.autograph.response.*;
import com.autograph.autographservice.api.dto.category.response.CategoryResponseDTO;
import com.autograph.autographservice.api.dto.like.request.LikeActivityResponseDTO;
import com.autograph.autographservice.api.dto.user.request.UserHeaderRequestDTO;
import com.autograph.autographservice.api.entity.autograph.*;
import com.autograph.autographservice.api.entity.category.Category;
import com.autograph.autographservice.api.enums.AutographState;
import com.autograph.autographservice.api.enums.PurchaseType;
import com.autograph.autographservice.api.enums.ResponseCodes;
import com.autograph.autographservice.api.repository.autograph.IAssetContractRepository;
import com.autograph.autographservice.api.repository.autograph.IAutographRepository;
import com.autograph.autographservice.api.repository.autograph.IReportRepository;
import com.autograph.autographservice.api.repository.category.ICategoryRepository;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.IAutographService;
import com.autograph.autographservice.api.utils.IpfsUtils;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AutographService implements IAutographService {

    private final ModelMapper modelMapper;
    private final AttachmentService attachmentService;
    private final IAutographRepository autographRepository;
    private final IAssetContractRepository assetContractRepository;
    private final ApiResponse apiResponse;
    private final ICategoryRepository categoryRepository;
    private final int numberOfItemsOnPage = 10;
    private final WebClient.Builder webClientBuilder;
    private final CircuitBreakerFactory cbFactory;
    private final IReportRepository reportRepository;


    @Autowired
    public AutographService(ModelMapper modelMapper, AttachmentService attachmentService,
        IAutographRepository autographRepository,
        IAssetContractRepository assetContractRepository, ApiResponse apiResponse,
        ICategoryRepository categoryRepository,
        WebClient.Builder webClientBuilder, CircuitBreakerFactory cbFactory,
        IReportRepository reportRepository) {
        this.modelMapper = modelMapper;
        this.attachmentService = attachmentService;
        this.autographRepository = autographRepository;
        this.assetContractRepository = assetContractRepository;
        this.apiResponse = apiResponse;
        this.categoryRepository = categoryRepository;
        this.webClientBuilder = webClientBuilder;
        this.cbFactory = cbFactory;
        this.reportRepository = reportRepository;
    }

    @SneakyThrows
    @Override
    public ApiResponse getAutographsCreatedByUser(Long userId, String authorization, int page) {
        //Get all autographs created by use by address
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        List<Autograph> autographs = autographRepository.findAllByCreatorAndIsDeletedFalse(userId, pageable);
        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> {
                AutographResponseDTO autographResponseDTO = autographResponseDTOMapper(autograph, authorization);
                autographResponseDTO.setIsLiked(autograph.getLikes().contains(userId));
                return  autographResponseDTO;
            })
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @SneakyThrows
    @Override
    public ApiResponse getAutographsOwnedByUser(Long ownerId, String authorization, int page) {
        //Get all autographs owned by user by id
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        List<Autograph> autographs = autographRepository.findAllByOwnerAndIsDeletedFalse(ownerId, pageable);
        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> autographResponseDTOMapper(autograph, authorization))
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographById(Long id, String authorization,Long userId) {
        Optional<Autograph> autograph = autographRepository.findByIdAndIsDeletedFalse(id);
        //Get autograph by id

        if (autograph.isPresent() && !autograph.get().getIsDeleted()) {

            AutographResponseDTO autographDetailsResponseDTO = autographResponseDTOMapper(autograph.get(),
                    authorization);

            autographDetailsResponseDTO.setIsLiked(autograph.get().getLikes().contains(userId));

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographDetailsResponseDTO);

        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getUserAutographs(int page,String authorization, Long ownerId) {
        //Get all autographs owned by you
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        List<Autograph> autographs = autographRepository.findAllByOwnerAndIsDeletedFalse(ownerId, pageable);
        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> autographResponseDTOMapper(autograph, authorization))
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse getUserCreatedAutographs(int page,String authorization, Long creatorId) {
        //Get all autographs created by user
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        List<Autograph> autographs = autographRepository.findAllByCreatorAndIsDeletedFalse(creatorId, pageable);
        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> autographResponseDTOMapper(autograph, authorization))
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse addAutograph(Long userId, AutographDTO autographRequest,
        String authorization) {
        //Celebrity user adds autograph

        if (Objects.equals(autographRequest.getFile(), null)) {
            apiResponse.setCode(ResponseCodes.FILE_CANNOT_BE_NULL.getValue());
            apiResponse.setResponse(false);

            return apiResponse;
        }
        LocalDateTime createdAt = LocalDateTime.now();
        Attachment attachment = attachmentService.storeFile(autographRequest.getFile(), userId);
        Autograph autograph = new Autograph(createdAt, autographRequest.getTitle(),
            autographRequest.getDescription(), autographRequest.getExternalLink(),
            autographRequest.getLockableExtras(),
            autographRequest.getAutographType(), userId, userId, attachment, null);

        Autograph autographSaved = autographRepository.save(autograph);
        ApiResponse followingUsersResponse =
            cbFactory.create("get-list-of-user-followers")
                .run(() -> getListOfUserFollowers(userId, authorization),
                    throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                        Collections.emptyList()));

        if (followingUsersResponse != null && followingUsersResponse.getResponse() != null) {
            List<Long> followingUsers = modelMapper.map(followingUsersResponse.getResponse(),
                List.class);

            cbFactory.create("post-to-followers-feed")
                .run(
                    () -> postToFollowersFeed(userId, authorization, autograph, followingUsers),
                    throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                        Collections.emptyList()));
        }

        ApiResponse autographCreationResponse = cbFactory.create("add-autograph-to-user")
            .run(() -> addAutographToUser(userId, authorization, autograph),
                throwable -> new ApiResponse(ResponseCodes.USER_NOT_FOUND.getValue(), false));

        if (!((Boolean) autographCreationResponse.getResponse())) {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        } else {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographResponseDTOMapper(autographSaved, authorization));
        }

        return apiResponse;
    }

    @Override
    public ApiResponse updateAutograph(Long creatorId,String authorization, Long id, AutographDTO autographRequest) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(id);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();

            if (AutographState.SUBMITTED.equals(autograph.getState())) {
                LocalDateTime updatedAt = LocalDateTime.now();

                if (!Objects.equals(autographRequest.getFile(), null)) {
                    Attachment attachment = attachmentService.storeFile(autographRequest.getFile(),
                            creatorId);

                    autograph.setAttachment(attachment);
                }

                autograph.setAutographType(autographRequest.getAutographType());
                autograph.setDescription(autographRequest.getDescription());
                autograph.setTitle(autographRequest.getTitle());
                autograph.setExternalLink(autographRequest.getExternalLink());
                autograph.setLockableExtras(autographRequest.getLockableExtras());
                autograph.setUpdatedAt(updatedAt);

                Autograph autographSaved = autographRepository.save(autograph);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(
                    autographResponseDTOMapper(autographSaved, authorization));

            } else {
                apiResponse.setCode(ResponseCodes.ALREADY_MINTED.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse deleteAutographById(Long userId,String authorization, Long id) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(id);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            if (AutographState.SUBMITTED.equals(autograph.getState())) {
                autograph.setIsDeleted(true);
                autographRepository.save(autograph);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(autographResponseDTOMapper(autograph, authorization));

            } else {
                apiResponse.setCode(ResponseCodes.ALREADY_MINTED.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAutographLikes(Long autographId, String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            Set<Long> autographLikes = autograph.getLikes();

            ApiResponse response = webClientBuilder.build()
                .post()
                .uri("http://user-service/api/users")
                .header("Authorization", authorization)
                .bodyValue(autographLikes.toArray())
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();

            return response;

        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographsByIdList(List<Long> ids,String authorization, Long userId) {
        List<Autograph> autographs = new ArrayList();

        for (Long id: ids) {
            Optional<Autograph> autograph = autographRepository.findByIdAndIsDeletedFalse(id);
            if(autograph.isPresent() && !autograph.get().getIsDeleted())
                autographs.add(autograph.get());
        }

        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> {
                AutographResponseDTO autographResponseDTO = autographResponseDTOMapper(autograph, authorization);
                autographResponseDTO.setIsLiked(autograph.getLikes().contains(userId));

                return autographResponseDTO;
            })
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse freezeAutograph(Long userId,String authorization, Long autographId) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isEmpty() || autographOptional.get().getIsDeleted()) {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            return apiResponse;
        }

        Autograph autograph = autographOptional.get();
        if (!autograph.getOwner().equals(userId)) {
            apiResponse.setCode(
                ResponseCodes.USER_IS_NOT_OWNER_OF_THE_AUTOGRAPH.getValue());
            return apiResponse;
        }

        if (AutographState.SUBMITTED.equals(autograph.getState())) {
            // autograph is not frozen or minted or lazy-minted yet
            autograph = IpfsUtils.uploadToIpfs(userId, autograph);
            if (autograph == null) {
                apiResponse.setCode(ResponseCodes.FREEZING_FAILED.getValue());
                return apiResponse;
            }
            autograph.setState(AutographState.FROZEN);
            autographRepository.save(autograph);
        }

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographResponseDTOMapper(autograph, authorization));
        return apiResponse;
    }

    @Override
    public ApiResponse putAutographOnSale(Long userId, Long autographId, SaleDTO saleDTO,
        String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isEmpty() || autographOptional.get().getIsDeleted()) {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            return apiResponse;
        }

        Autograph autograph = autographOptional.get();
        if (!autograph.getOwner().equals(userId)) {
            apiResponse.setCode(
                ResponseCodes.USER_IS_NOT_OWNER_OF_THE_AUTOGRAPH.getValue());
            return apiResponse;
        }

        // if it's not at least frozen, fail
        if (AutographState.SUBMITTED.equals(autograph.getState())) {
            apiResponse.setCode(ResponseCodes.NOT_FROZEN.getValue());
            return apiResponse;
        }

        // TODO: verify signatures in identity

        autograph.setPurchaseType(saleDTO.getPurchaseType());
        AutographSaleIdentity identity = new AutographSaleIdentity();
        identity.setMintType(saleDTO.getSaleIdentity().getMintType());
        switch (saleDTO.getSaleIdentity().getMintType()) {
            case LAZY_MINT: {
                identity.setErc721ContractAddress(
                    saleDTO.getSaleIdentity().getErc721ContractAddress());
                identity.setIntermediaryContractAddress(
                    saleDTO.getSaleIdentity().getIntermediaryContractAddress());
                identity.setSignatureLazyMint(saleDTO.getSaleIdentity().getSignatureLazyMint());
                identity.setSignatureLazyDeal(saleDTO.getSaleIdentity().getSignatureLazyDeal());
                identity.setAuthor(saleDTO.getSaleIdentity().getAuthor());
                identity.setRoyaltiesPercentage(saleDTO.getSaleIdentity().getRoyaltiesPercentage());
                identity.setPrice(saleDTO.getSaleIdentity().getPrice());
            }
            break;
            case MINTED: {
                identity.setErc721ContractAddress(
                    saleDTO.getSaleIdentity().getErc721ContractAddress());
                identity.setIntermediaryContractAddress(
                    saleDTO.getSaleIdentity().getIntermediaryContractAddress());
                identity.setTokenId(saleDTO.getSaleIdentity().getTokenId());
                identity.setSignatureDeal(saleDTO.getSaleIdentity().getSignatureDeal());
                identity.setAuthor(saleDTO.getSaleIdentity().getAuthor());
                identity.setRoyaltiesPercentage(saleDTO.getSaleIdentity().getRoyaltiesPercentage());
                identity.setPrice(saleDTO.getSaleIdentity().getPrice());
            }
            break;
            default:
                throw new IllegalStateException(
                    "Unexpected value: " + saleDTO.getSaleIdentity().getMintType());
        }
        autograph.setSaleIdentity(identity);

        if (saleDTO.getPurchaseType() == PurchaseType.AUCTION) {
            AuctionRequestDTO auction = webClientBuilder.build()
                .post()
                .uri("http://auction-service/api/auction/autograph/" + autograph.getId()
                    + "/owner/" + autograph.getOwner())
                .header("Authorization", authorization)
                .bodyValue(saleDTO)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(AuctionRequestDTO.class)
                .block();

            if (!Objects.equals(auction, null)) {
                AuctionResponseDTO auctionResponseDTO = auctionMapper(autograph, auction);
                autograph.setAuctionId(auction.getId());
                autographRepository.save(autograph);
                apiResponse.setResponse(auctionResponseDTO);
            }

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else if (saleDTO.getPurchaseType() == PurchaseType.PURCHASE_NOW) {
            autographRepository.save(autograph);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographResponseDTOMapper(autograph, authorization));
        }

        return apiResponse;
    }

    @Override
    public ApiResponse addCategoryToAutograph(Long categoryId,String authorization, Long itemId) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(itemId);
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            if (categoryOptional.isPresent()) {
                var autograph = autographOptional.get();
                Category category = categoryOptional.get();
                Collection<Category> categories = autograph.getCategories();

                categories.add(category);
                autograph.setCategories(categories);
                autographRepository.save(autograph);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(autographResponseDTOMapper(autograph, authorization));
            } else {
                apiResponse.setCode(ResponseCodes.CATEGORY_NOT_FOUND.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographsByCategory(Long categoryId,String authorization, Long userId, int page) {
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);
        if (categoryOptional.isPresent()) {
            Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
            Category category = categoryOptional.get();
            List<Autograph> autographs =
                autographRepository.findAllByCategoriesContainsAndIsDeletedFalse(category, pageable);

            List<AutographResponseDTO> autographResponseDTOS =
                autographs
                    .stream()
                        .filter(autograph -> !autograph.getIsDeleted())
                        .map(autograph -> {
                        AutographResponseDTO autographResponseDTO = autographResponseDTOMapper(autograph, authorization);
                        autographResponseDTO.setIsLiked(autograph.getLikes().contains(userId));
                        return autographResponseDTO;
                    })
                    .collect(Collectors.toList());

            apiResponse.setResponse(autographResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.CATEGORY_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getCategoriesOfAutographById(Long id,String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(id);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            Collection<CategoryResponseDTO> categories =
                autograph.getCategories().stream().map(
                        category -> modelMapper.map(category, CategoryResponseDTO.class)
                    )
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(categories);
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Transactional
    @Override
    public ApiResponse likeAutograph(Long autographId, Long userId, String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            ApiResponse userHeaderResponse = webClientBuilder.build()
                .get()
                .uri("http://user-service/api/user/header/" + userId)
                .header("Authorization", String.valueOf(authorization))
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();

            UserHeaderRequestDTO userHeaderRequestDTO =
                modelMapper.map(userHeaderResponse.getResponse(), UserHeaderRequestDTO.class);

            Autograph autograph = autographOptional.get();
            Set<Long> likes = autograph.getLikes();

            if (!Objects.equals(userHeaderRequestDTO, null)) {
                LikeActivityResponseDTO likeActivityResponseDTO =
                    new LikeActivityResponseDTO(autograph.getOwner(), autographId,
                        autograph.getAttachment().getFileName(), autograph.getAutographType(),
                        autograph.getTitle(),
                        userHeaderRequestDTO.getId(), userHeaderRequestDTO.getUsername(),
                        userHeaderRequestDTO.getProfilePictureFileName());

                ApiResponse apiResponse = webClientBuilder.build()
                    .post()
                    .uri("http://user-service/api/user/liked/" + autographId)
                    .header("Authorization", String.valueOf(authorization))
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

                    if (!likes.contains(userId)) {
                        webClientBuilder.build()
                                .post()
                                .uri("http://activity-service/api/activity/broadcast/like")
                                .bodyValue(likeActivityResponseDTO)
                                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                                .header("Authorization", authorization)
                                .acceptCharset(StandardCharsets.UTF_8)
                                .ifNoneMatch("*")
                                .ifModifiedSince(ZonedDateTime.now())
                                .retrieve()
                                .bodyToMono(ApiResponse.class)
                                .block();
                    }else{
                        webClientBuilder.build()
                                .post()
                                .uri("http://activity-service/api/activity/broadcast/like/remove")
                                .bodyValue(likeActivityResponseDTO)
                                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                                .header("Authorization", authorization)
                                .acceptCharset(StandardCharsets.UTF_8)
                                .ifNoneMatch("*")
                                .ifModifiedSince(ZonedDateTime.now())
                                .retrieve()
                                .bodyToMono(ApiResponse.class)
                                .block();
                    }
            } else {
                apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            }
            if (likes.contains(userId)) {
                likes.remove(userId);
                autograph.setNumberOfLikes(autograph.getNumberOfLikes() - 1);
                autograph.setLikes(likes);
                autographRepository.save(autograph);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(false);
            } else {
                likes.add(userId);
                autograph.setLikes(likes);
                autograph.setNumberOfLikes(autograph.getNumberOfLikes() + 1);
                autographRepository.save(autograph);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(true);
            }
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse isAutographLiked(Long autographId, Long userId, String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            Set<Long> autographLikes = autograph.getLikes();

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographLikes.contains(userId));
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getSearchFeed(int page,String authorization, Long userId) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage, Sort.by(Sort.Direction.DESC, "createdAt"));

        Page<Autograph> autographs = autographRepository.findAllByIsDeletedFalse(pageable);
        List<AutographResponseDTO> autographDTOS = autographs.stream()
                .filter(autograph -> !autograph.getIsDeleted())
            .map(autograph -> {
                AutographResponseDTO autographResponseDTO = autographResponseDTOMapper(autograph, authorization);
                autographResponseDTO.setIsLiked(autograph.getLikes().contains(userId));

                return autographResponseDTO;
            })
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(autographDTOS);

        return apiResponse;
    }

    @Override
    public ApiResponse search(String query,String authorization) {
        List<AutographResponseDTO> autographs =
            autographRepository.findAllByTitleContainingAndIsDeletedFalse(query)
                .stream()
                    .filter(autograph -> !autograph.getIsDeleted())
                    .map(autograph -> autographResponseDTOMapper(autograph, authorization))
                .collect(Collectors.toList());

        apiResponse.setResponse(autographs);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographOwnerId(Long autographId,String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Long ownerId = autographOptional.get().getOwner();

            apiResponse.setResponse(ownerId);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public Resource getAutographPicture() {
        return null;
    }

    @Override
    public ApiResponse getVerifiedAutographs(Boolean status, String authorization) {
        List<Autograph> autographs =
            autographRepository.findAllByIsVerifiedAndIsDeletedFalse(status);

        List<AutographResponseDTO> autographResponseDTOS =
            autographs
                .stream()
                    .filter(autograph -> !autograph.getIsDeleted())
                    .map(autograph -> autographResponseDTOMapper(autograph, authorization))
                .collect(Collectors.toList());

        apiResponse.setResponse(autographResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse getBlockedAutographs(Boolean status, String authorization) {
        List<Autograph> autographs =
            autographRepository.findAllByIsBlockedAndIsDeletedFalse(status);

        List<AutographResponseDTO> autographResponseDTOS =
            autographs
                .stream()
                    .filter(autograph -> !autograph.getIsDeleted())
                .map(autograph -> autographResponseDTOMapper(autograph, authorization))
                .collect(Collectors.toList());

        apiResponse.setResponse(autographResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse getReportedAutographs(Long userId, String authorization) {
        List<Report> reports = reportRepository.findAll();
        List<Long> reportsAutographIds = reports.stream()
            .map(report -> report.getAutograph().getId())
            .collect(Collectors.toList());

        return getAutographsByIdList(reportsAutographIds, authorization, userId);
    }

    @Override
    public ApiResponse getAllAutographs(Long userId, String authorization) {
        List<Autograph> autographs =
            autographRepository.findAll();

        List<AutographResponseDTO> autographResponseDTOS =
            autographs
                .stream()
                    .filter(autograph -> !autograph.getIsDeleted())
                .map(autograph -> {
                    AutographResponseDTO autographResponseDTO = autographResponseDTOMapper(autograph, authorization);
                    autographResponseDTO.setIsLiked(autograph.getLikes().contains(userId));
                    return autographResponseDTO;
                })
                .collect(Collectors.toList());

        apiResponse.setResponse(autographResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse blockAutographById(Long autographId,String authorization, Boolean status) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            autograph.setIsBlocked(status);

            autographRepository.save(autograph);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(true);
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographPreviousOwners(Long autographId, String authorization) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(autographId);

        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            Autograph autograph = autographOptional.get();
            Set<Long> userIds = autograph.getPreviousOwners();

            ApiResponse userHeaders = cbFactory.create("get-user-headers-by-ids")
                    .run(() -> getUserHeadersByIds(userIds, authorization),
                            throwable -> new ApiResponse(ResponseCodes.USER_NOT_FOUND.getValue(), new ArrayList<Long>()));

            apiResponse.setCode(userHeaders.getCode());
            apiResponse.setResponse(userHeaders.getResponse());
        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographsInfo() {
        Integer numberOfautographs = autographRepository.countByIdNotNull();
        LocalDateTime current = LocalDateTime.now();
        Integer currentMonth = current.getMonthValue();
        Integer currentYear = current.getYear();

        HashMap<Integer, Integer> months = new HashMap<>();
        HashMap<Integer, Integer> years = new HashMap<>();

        for(int i = currentYear; i > currentYear - 5; i--){
            years.put(i, autographRepository.countByCreatedDate(i).size());
        }

        for(int i = currentMonth; i > currentMonth - 5; i--){
            if(i < 1)
                months.put(12+i, autographRepository.countByCreatedDateMonthAndCreatedDateYear(12+i, currentYear - 1).size());
            else
                months.put(i, autographRepository.countByCreatedDateMonthAndCreatedDateYear(i, currentYear).size());
        }

        AutographsInfoResponseDTO autographsInfoResponseDTO = new AutographsInfoResponseDTO(numberOfautographs, months,years);
        apiResponse.setResponse(autographsInfoResponseDTO);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse getAutographByIdPublic(Long id) {
        Optional<Autograph> autograph = autographRepository.findByIdAndIsDeletedFalse(id);

        if (autograph.isPresent() && !autograph.get().getIsDeleted()) {

            AutographResponseDTO autographDetailsResponseDTO = modelMapper.map(autograph.get(), AutographResponseDTO.class);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographDetailsResponseDTO);

        } else {
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getNumberOfDeletedAutographsByUserId(Long userId) {
        Long numberOfDeletedAutographs = autographRepository.countByIsDeletedAndOwner(true, userId);

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(numberOfDeletedAutographs);

        return apiResponse;
    }

    private ApiResponse addAutographToUser(Long userId, String authorization, Autograph autograph) {
        return webClientBuilder.build()
            .post()
            .uri("http://user-service/api/user/autograph/" + autograph.getId())
            .header("id", String.valueOf(userId))
            .header("Authorization", String.valueOf(authorization))
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

    private ApiResponse postToFollowersFeed(Long userId, String authorization, Autograph autograph,
        List<Long> followingUsers) {
        return webClientBuilder.build()
            .post()
            .uri("http://feed-service/api/feed/users/" + autograph.getId())
            .header("id", String.valueOf(userId))
            .header("Authorization", String.valueOf(authorization))
            .bodyValue(followingUsers)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

    private ApiResponse getListOfUserFollowers(Long userId, String authorization) {
        return webClientBuilder.build()
            .get()
            .uri("http://user-service/api/user/follow/followers")
            .header("id", String.valueOf(userId))
            .header("Authorization", String.valueOf(authorization))
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

    public ApiResponse importAutograph(Long userId, ImportedAutographDTO importedAutographDTO,
        String authorization) {
        LocalDateTime createdAt = LocalDateTime.now();
        Attachment attachment = attachmentService.storeFile(importedAutographDTO.getFile(), userId);
        Autograph autograph = new Autograph(createdAt, importedAutographDTO.getTitle(),
            importedAutographDTO.getDescription(), importedAutographDTO.getExternalLink(),
            importedAutographDTO.getLockableExtras(),
            importedAutographDTO.getAutographType(), userId, userId, attachment,
            importedAutographDTO.getMetadata());

        AssetContract assetContract = AssetContract
            .builder()
            .assetContractType(importedAutographDTO.getAssetContractType())
            .address(importedAutographDTO.getAddress())
            .animationOriginalUrl(importedAutographDTO.getAnimationOriginalUrl())
            .animationUrl(importedAutographDTO.getAnimationUrl())
            .buyerFeeBasisPoints(importedAutographDTO.getBuyerFeeBasisPoints())
            .createdAt(importedAutographDTO.getCreatedAt())
            .creatorAddress(importedAutographDTO.getCreatorAddress())
            .description(importedAutographDTO.getDescription())
            .externalLink(importedAutographDTO.getExternalLink())
            .name(importedAutographDTO.getName())
            .onlyProxiedTransfers(importedAutographDTO.getOnlyProxiedTransfers())
            .openseaBuyerFeeBasisPoints(importedAutographDTO.getOpenseaBuyerFeeBasisPoints())
            .openseaSellerFeeBasisPoints(importedAutographDTO.getOpenseaSellerFeeBasisPoints())
            .openseaVersion(importedAutographDTO.getOpenseaVersion())
            .ownerId(importedAutographDTO.getOwnerId())
            .payoutAddress(importedAutographDTO.getPayoutAddress())
            .schemaName(importedAutographDTO.getSchemaName())
            .totalSupply(importedAutographDTO.getTotalSupply())
            .transferFee(importedAutographDTO.getTransferFee())
            .tokenId(importedAutographDTO.getTokenId().toString())
            .nftVersion(importedAutographDTO.getNftVersion())
            .ownerAddress(importedAutographDTO.getOwnerAddress())
            .build();

        autograph.setAssetContract(assetContract);
        autograph.setImported(true);
        Autograph autographSaved = autographRepository.save(autograph);

        ApiResponse autographCreationResponse = webClientBuilder.build()
            .post()
            .uri("http://user-service/api/user/autograph/" + autograph.getId())
            .header("id", String.valueOf(userId))
            .header("Authorization", authorization)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();

        ApiResponse followingUsersResponse =
                cbFactory.create("get-list-of-user-followers")
                        .run(() -> getListOfUserFollowers(userId, authorization),
                                throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                                        Collections.emptyList()));

        if (followingUsersResponse != null && followingUsersResponse.getResponse() != null) {
            List<Long> followingUsers = modelMapper.map(followingUsersResponse.getResponse(),
                    List.class);

            cbFactory.create("post-to-followers-feed")
                    .run(
                            () -> postToFollowersFeed(userId, authorization, autograph, followingUsers),
                            throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                                    Collections.emptyList()));
        }


        if (!((Boolean) autographCreationResponse.getResponse())) {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        } else {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(autographResponseDTOMapper(autographSaved, authorization));
        }

        return apiResponse;
    }

    public ApiResponse importAutographs(Long userId, List<ImportedAutographDTO> importedAutographDTOs,
                                       String authorization) {
        for(ImportedAutographDTO importedAutographDTO : importedAutographDTOs) {
            LocalDateTime createdAt = LocalDateTime.now();
            Attachment attachment = attachmentService.storeFile(importedAutographDTO.getFile(), userId);
            Autograph autograph = new Autograph(createdAt, importedAutographDTO.getTitle(),
                    importedAutographDTO.getDescription(), importedAutographDTO.getExternalLink(),
                    importedAutographDTO.getLockableExtras(),
                    importedAutographDTO.getAutographType(), userId, userId, attachment,
                    importedAutographDTO.getMetadata());

            AssetContract assetContract = AssetContract
                    .builder()
                    .assetContractType(importedAutographDTO.getAssetContractType())
                    .address(importedAutographDTO.getAddress())
                    .animationOriginalUrl(importedAutographDTO.getAnimationOriginalUrl())
                    .animationUrl(importedAutographDTO.getAnimationUrl())
                    .buyerFeeBasisPoints(importedAutographDTO.getBuyerFeeBasisPoints())
                    .createdAt(importedAutographDTO.getCreatedAt())
                    .creatorAddress(importedAutographDTO.getCreatorAddress())
                    .description(importedAutographDTO.getDescription())
                    .externalLink(importedAutographDTO.getExternalLink())
                    .name(importedAutographDTO.getName())
                    .onlyProxiedTransfers(importedAutographDTO.getOnlyProxiedTransfers())
                    .openseaBuyerFeeBasisPoints(importedAutographDTO.getOpenseaBuyerFeeBasisPoints())
                    .openseaSellerFeeBasisPoints(importedAutographDTO.getOpenseaSellerFeeBasisPoints())
                    .openseaVersion(importedAutographDTO.getOpenseaVersion())
                    .ownerId(importedAutographDTO.getOwnerId())
                    .payoutAddress(importedAutographDTO.getPayoutAddress())
                    .schemaName(importedAutographDTO.getSchemaName())
                    .totalSupply(importedAutographDTO.getTotalSupply())
                    .transferFee(importedAutographDTO.getTransferFee())
                    .tokenId(importedAutographDTO.getTokenId().toString())
                    .nftVersion(importedAutographDTO.getNftVersion())
                    .ownerAddress(importedAutographDTO.getOwnerAddress())
                    .build();

            autograph.setAssetContract(assetContract);
            autograph.setImported(true);
            Autograph autographSaved = autographRepository.save(autograph);

            ApiResponse autographCreationResponse = webClientBuilder.build()
                    .post()
                    .uri("http://user-service/api/user/autograph/" + autograph.getId())
                    .header("id", String.valueOf(userId))
                    .header("Authorization", authorization)
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();


            if (!((Boolean) autographCreationResponse.getResponse())) {
                apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
                apiResponse.setResponse(false);
            } else {
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(autographResponseDTOMapper(autographSaved, authorization));
            }

            return apiResponse;
        }

        return apiResponse;
    }

    public ApiResponse getAssetContractByAutographId(Long id) {
        Optional<Autograph> autographOptional = autographRepository.findByIdAndIsDeletedFalse(id);
        if (autographOptional.isPresent() && !autographOptional.get().getIsDeleted()) {
            if (!Objects.equals(autographOptional.get().getAssetContract(), null)) {
                Long assetId = autographOptional.get().getAssetContract().getId();
                Optional<AssetContract> assetContractOptional = assetContractRepository.findById(
                    assetId);

                if (assetContractOptional.isPresent()) {
                    AssetContract assetContract = assetContractOptional.get();
                    ImportedAutographResponseDTO importedAutographDTO =
                        mapImportedAutograph(autographOptional.get(), assetContract);

                    apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                    apiResponse.setResponse(importedAutographDTO);
                } else {
                    apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                    apiResponse.setResponse(null);
                }
            } else {
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            }
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    ImportedAutographResponseDTO mapImportedAutograph(Autograph autograph,
                                                      AssetContract assetContract) {

        return ImportedAutographResponseDTO.builder()
            .autographType(autograph.getAutographType())
            .assetContractType(assetContract.getAssetContractType())
            .address(assetContract.getAddress())
            .animationOriginalUrl(assetContract.getAnimationOriginalUrl())
            .animationUrl(assetContract.getAnimationUrl())
            .buyerFeeBasisPoints(assetContract.getBuyerFeeBasisPoints())
            .createdAt(assetContract.getCreatedAt())
            .creatorAddress(assetContract.getCreatorAddress())
            .description(assetContract.getDescription())
            .externalLink(assetContract.getExternalLink())
            .name(autograph.getTitle())
            .title(autograph.getTitle())
            .onlyProxiedTransfers(assetContract.getOnlyProxiedTransfers())
            .openseaBuyerFeeBasisPoints(assetContract.getOpenseaBuyerFeeBasisPoints())
            .openseaSellerFeeBasisPoints(assetContract.getOpenseaSellerFeeBasisPoints())
            .openseaVersion(assetContract.getOpenseaVersion())
            .ownerId(assetContract.getOwnerId())
            .payoutAddress(assetContract.getPayoutAddress())
            .schemaName(assetContract.getSchemaName())
            .totalSupply(assetContract.getTotalSupply())
            .transferFee(assetContract.getTransferFee())
            .tokenId(assetContract.getTokenId())
            .nftVersion(assetContract.getNftVersion())
            .attachmentFileName(autograph.getAttachment().getFileName())
            .attachmentUploadDirectory(autograph.getAttachment().getUploadDirectory())
            .previousOwners(autograph.getPreviousOwners())
            .lockableExtras(autograph.getLockableExtras())
            .numberOfLikes(autograph.getNumberOfLikes())
            .build();
    }

    AutographResponseDTO autographResponseDTOMapper(Autograph autograph, String authorization) {

        AutographResponseDTO autographResponseDTO =  modelMapper.map(autograph, AutographResponseDTO.class);

        Set<Long> userIds = new HashSet<>();
        userIds.add(autograph.getOwner());
        userIds.add(autograph.getCreator());

        ApiResponse userHeadersResponse = cbFactory.create("get-user-headers-by-ids")
                .run(() -> getUserHeadersByIds(userIds, authorization),
                        throwable -> new ApiResponse(ResponseCodes.USER_NOT_FOUND.getValue(), new ArrayList<Long>()));

        ArrayList<UserHeaderResponseDTO> userHeaders = ((ArrayList<UserHeaderResponseDTO>)userHeadersResponse.getResponse());
        autographResponseDTO.setHeaders(userHeaders);

        return autographResponseDTO;
    }

    AuctionResponseDTO auctionMapper(Autograph autograph, AuctionRequestDTO auction) {
        AuctionResponseDTO auctionResponseDTO = modelMapper.map(autograph,
            AuctionResponseDTO.class);
        auctionResponseDTO.setAuctionId(auction.getId());
        auctionResponseDTO.setAuctionPrice(auction.getPrice());
        auctionResponseDTO.setAuctionEndDate(auction.getEndDate());
        auctionResponseDTO.setAuctionStartDate(auction.getStartDate());
        auctionResponseDTO.setAuctionCreatedDate(auction.getCreatedDate());
        return auctionResponseDTO;
    }

    private ApiResponse getUserHeadersByIds(Set<Long> userIds, String authorization) {
        return webClientBuilder.build()
                .post()
                .uri("http://user-service/api/user/headers")
                .header("Authorization", String.valueOf(authorization))
                .bodyValue(userIds)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();
    }
}
