package com.autograph.autographservice.api.repository.autograph;

import com.autograph.autographservice.api.entity.autograph.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IAttachmentRepository extends JpaRepository<Attachment, Long> {

    Optional<Attachment> findById(Long id);
}
