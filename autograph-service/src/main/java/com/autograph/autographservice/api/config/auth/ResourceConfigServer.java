package com.autograph.autographservice.api.config.auth;

import com.autograph.autographservice.api.service.security.CustomUserInfoTokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class ResourceConfigServer extends ResourceServerConfigurerAdapter {

    @Autowired
    private ResourceServerProperties sso;

    @Bean
    public ResourceServerTokenServices tokenServices() {
        CustomUserInfoTokenServices resourceServerTokenServices =
            new CustomUserInfoTokenServices(sso.getUserInfoUri(), sso.getClientId());

        return resourceServerTokenServices;
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/autograph/**/public").permitAll()
                .antMatchers("/api/autograph/deleted").permitAll()
            .anyRequest().authenticated();
    }
}
