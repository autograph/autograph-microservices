package com.autograph.autographservice.api.dto.autograph.response;

import lombok.Data;

@Data
public class MetadataDTO {

    private Long id;
    private String dataIpfsUri;
    private String metadataIpfsUri;
}
