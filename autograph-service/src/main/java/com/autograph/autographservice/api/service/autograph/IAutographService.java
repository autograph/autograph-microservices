package com.autograph.autographservice.api.service.autograph;


import com.autograph.autographservice.api.dto.autograph.request.AutographDTO;
import com.autograph.autographservice.api.dto.autograph.request.SaleDTO;
import com.autograph.autographservice.api.response.ApiResponse;
import org.springframework.core.io.Resource;

import java.util.List;

public interface IAutographService {

    ApiResponse getAutographsCreatedByUser(Long userId, String authorization, int page);

    ApiResponse getAutographsOwnedByUser(Long userId, String authorization, int page);

    ApiResponse getAutographById(Long autographId, String authorization, Long userId) throws Exception;

    ApiResponse getUserAutographs(int page, String authorization, Long ownerId) throws Exception;

    ApiResponse getUserCreatedAutographs(int page, String authorization, Long creatorId) throws Exception;

    ApiResponse addAutograph(Long userId, AutographDTO autographRequest, String authorization)
        throws Exception;

    ApiResponse updateAutograph(Long userId, String authorization, Long autographId, AutographDTO autographRequest)
        throws Exception;

    ApiResponse deleteAutographById(Long userId, String authorization, Long autographId) throws Exception;

    ApiResponse getAutographLikes(Long autographId, String authorization) throws Exception;

    ApiResponse getAutographsByIdList(List<Long> ids, String authorization, Long userId);

    ApiResponse freezeAutograph(Long userId, String authorization, Long autographId);

    ApiResponse putAutographOnSale(Long userId, Long autographId, SaleDTO saleDTO,
        String authorization);

    ApiResponse addCategoryToAutograph(Long categoryId, String authorization, Long itemId);

    ApiResponse getAutographsByCategory(Long categoryId, String authorization,Long userId, int page);

    ApiResponse getCategoriesOfAutographById(Long id, String authorization);

    ApiResponse likeAutograph(Long autographId, Long userId, String authorization);

    ApiResponse isAutographLiked(Long autographId, Long userId, String authorization);

    ApiResponse getSearchFeed(int page, String authorization, Long userId);

    ApiResponse search(String query,  String authorization);

    ApiResponse getAutographOwnerId(Long autographId,String authorization);

    Resource getAutographPicture();

    ApiResponse getVerifiedAutographs(Boolean status, String authorization);

    ApiResponse getBlockedAutographs(Boolean status, String authorization);

    ApiResponse getReportedAutographs(Long userId, String authorization);

    ApiResponse getAllAutographs(Long userId, String authorization);

    ApiResponse blockAutographById(Long autographId, String authorization, Boolean status);

    ApiResponse getAutographPreviousOwners(Long autographId, String authorization);

    ApiResponse getAutographsInfo();

    ApiResponse getAutographByIdPublic(Long id);

    ApiResponse getNumberOfDeletedAutographsByUserId(Long userId);
}
