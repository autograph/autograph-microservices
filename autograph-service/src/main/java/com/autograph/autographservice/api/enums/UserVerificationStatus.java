package com.autograph.autographservice.api.enums;

public enum UserVerificationStatus {
    VERIFIED,
    REJECTED,
    PENDING,
    REVISION_NEEDED,
    STALE
}
