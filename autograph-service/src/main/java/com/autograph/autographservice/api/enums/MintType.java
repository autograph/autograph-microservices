package com.autograph.autographservice.api.enums;

public enum MintType {
    /**
     * Lazy-mint sale for a fixed price. Requires a minting signature permit given to the
     * Intermediary contract, and a deal signature for the ERC721Address + MintingSignature.
     */
    LAZY_MINT,
    /**
     * Already minted sale for a fixed price. Requires a deal signature for the ERC721Address +
     * ERC721TokenId.
     */
    MINTED
}
