package com.autograph.autographservice.api.service.autograph.impl;

import com.autograph.autographservice.api.config.FileStorageProperties;
import com.autograph.autographservice.api.dto.autograph.response.ReportAttachmentResponseDTO;
import com.autograph.autographservice.api.entity.autograph.ReportAttachment;
import com.autograph.autographservice.api.enums.ResponseCodes;
import com.autograph.autographservice.api.exception.AttachmentStorageException;
import com.autograph.autographservice.api.repository.autograph.IReportAttachmentRepository;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.IReportAttachmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReportAttachmentService implements IReportAttachmentService {
    private final IReportAttachmentRepository reportAttachmentRepository;
    private final ModelMapper modelMapper;
    private final Path fileStorageLocation;

    @Autowired
    public ReportAttachmentService(FileStorageProperties fileStorageProperties,
                                   IReportAttachmentRepository reportAttachmentRepository, ModelMapper modelMapper) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        this.reportAttachmentRepository = reportAttachmentRepository;

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new AttachmentStorageException(
                    "Could not create the directory where the uploaded files will be stored.", ex);
        }
        this.modelMapper = modelMapper;
    }

    @Override
    public ReportAttachment storeFile(MultipartFile file, Long autographId) {

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new AttachmentStorageException(
                        "Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExtension = getExtensionByStringHandling(fileName).get();

            Path targetLocationWithUserId = this.fileStorageLocation.resolve("reports")
                    .resolve(Long
                            .toString(autographId));

            if (!Files.exists(targetLocationWithUserId)) {
                Files.createDirectories(targetLocationWithUserId);
            }

            int fileCount = Objects
                    .requireNonNull(new File(targetLocationWithUserId.toString()).list()).length + 1;
            String newFileName = fileCount + "." + fileExtension;
            Path targetLocation = targetLocationWithUserId.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

            return new ReportAttachment(newFileName,
                    this.fileStorageLocation.toString());
        } catch (IOException ex) {
            throw new AttachmentStorageException(
                    "Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public ApiResponse listAllAttachments() {
        ApiResponse apiResponse = new ApiResponse();

        List<ReportAttachment> attachments = reportAttachmentRepository.findAll();
        if (!attachments.isEmpty()) {
            List<ReportAttachmentResponseDTO> attachmentResponseDTOS = attachments
                    .stream()
                    .map(attachment -> modelMapper.map(attachment, ReportAttachmentResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTOS);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAttachmentById(Long id) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<ReportAttachment> attachmentOptional = reportAttachmentRepository.findById(id);
        if (attachmentOptional.isPresent()) {
            ReportAttachment attachment = attachmentOptional.get();
            ReportAttachmentResponseDTO attachmentResponseDTO = modelMapper.map(attachment,
                    ReportAttachmentResponseDTO.class);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTO);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
