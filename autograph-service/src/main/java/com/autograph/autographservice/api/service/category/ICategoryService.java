package com.autograph.autographservice.api.service.category;

import com.autograph.autographservice.api.dto.category.request.CategoryRequestDTO;
import com.autograph.autographservice.api.response.ApiResponse;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public interface ICategoryService {

    ApiResponse createCategory(CategoryRequestDTO category);

    ApiResponse deleteCategory(Long id);

    ApiResponse getListOfCategories();

    ApiResponse getListOfCategoriesByListOfIds(Collection<Long> idList);

    ApiResponse addCategoryToAutograph(Long categoryId, Long itemId);

    ApiResponse addCategoriesToAutograph(List<Long> categoryIds, Long autographId);

    ApiResponse setCategoriesToAutograph(List<Long> categoryIds, Long autographId);
}
