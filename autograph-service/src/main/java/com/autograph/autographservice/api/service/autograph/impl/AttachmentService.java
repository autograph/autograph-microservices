package com.autograph.autographservice.api.service.autograph.impl;

import com.autograph.autographservice.api.config.FileStorageProperties;
import com.autograph.autographservice.api.dto.autograph.response.AttachmentResponseDTO;
import com.autograph.autographservice.api.entity.autograph.Attachment;
import com.autograph.autographservice.api.enums.ResponseCodes;
import com.autograph.autographservice.api.exception.AttachmentStorageException;
import com.autograph.autographservice.api.repository.autograph.IAttachmentRepository;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.IAttachmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class AttachmentService implements IAttachmentService {

    final IAttachmentRepository attachmentRepository;
    final ModelMapper modelMapper;
    private final Path fileStorageLocation;

    @Autowired
    public AttachmentService(FileStorageProperties fileStorageProperties,
        IAttachmentRepository attachmentRepository, ModelMapper modelMapper) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
            .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new AttachmentStorageException(
                "Could not create the directory where the uploaded files will be stored.", ex);
        }
        this.attachmentRepository = attachmentRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Attachment storeFile(MultipartFile file, Long userId) {

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new AttachmentStorageException(
                    "Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExtension = getExtensionByStringHandling(fileName).get();

            Path targetLocationWithUserId = this.fileStorageLocation.resolve(Long
                    .toString(userId))
                .resolve("attachments");

            if (!Files.exists(targetLocationWithUserId)) {
                Files.createDirectories(targetLocationWithUserId);
            }

            int fileCount = Objects
                .requireNonNull(new File(targetLocationWithUserId.toString()).list()).length + 1;
            String newFileName = fileCount + "." + fileExtension;
            Path targetLocation = targetLocationWithUserId.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

            return new Attachment(newFileName,
                mimetypesFileTypeMap.getContentType(file.getName()),
                this.fileStorageLocation.toString());
        } catch (IOException ex) {
            throw new AttachmentStorageException(
                "Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public ApiResponse listAllAttachments() {
        ApiResponse apiResponse = new ApiResponse();

        List<Attachment> attachments = attachmentRepository.findAll();
        if (!attachments.isEmpty()) {
            List<AttachmentResponseDTO> attachmentResponseDTOS = attachments
                .stream()
                .map(attachment -> modelMapper.map(attachment, AttachmentResponseDTO.class))
                .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTOS);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAttachmentById(Long id) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<Attachment> attachmentOptional = attachmentRepository.findById(id);
        if (attachmentOptional.isPresent()) {
            Attachment attachment = attachmentOptional.get();
            AttachmentResponseDTO attachmentResponseDTO = modelMapper.map(attachment,
                AttachmentResponseDTO.class);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(attachmentResponseDTO);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
            .filter(f -> f.contains("."))
            .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
