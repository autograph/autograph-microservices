package com.autograph.autographservice.api.enums;

public enum ReportReason {
    fake_account, pornography, hate_speech, threats, other
}
