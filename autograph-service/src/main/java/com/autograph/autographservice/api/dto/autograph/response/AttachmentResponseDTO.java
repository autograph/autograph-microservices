package com.autograph.autographservice.api.dto.autograph.response;

import lombok.Data;

@Data
public class AttachmentResponseDTO {

    private Long id;
    private Long autographId;
    private String fileName;
    private String documentType;
    private String uploadDirectory;
}
