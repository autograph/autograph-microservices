package com.autograph.autographservice.api.dto.autograph.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;

@Data
@AllArgsConstructor
public class AutographsInfoResponseDTO {
    Integer numberOfAutographs;
    HashMap<Integer, Integer> months;
    HashMap<Integer, Integer> years;

}
