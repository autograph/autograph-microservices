package com.autograph.autographservice.api.dto.autograph.request;

import com.autograph.autographservice.api.enums.ReportReason;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ReportAutographWithoutAttachmentRequestDTO {
    Long autographId;
    ReportReason reason;
    String comment;
}
