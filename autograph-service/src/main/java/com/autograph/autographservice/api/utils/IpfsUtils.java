package com.autograph.autographservice.api.utils;

import com.autograph.autographservice.api.entity.autograph.Autograph;
import com.autograph.autographservice.api.entity.autograph.Metadata;
import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class IpfsUtils {

    public static String IPFS_PREFIX = "ipfs://";

    public static Autograph uploadToIpfs(Long userId, Autograph autograph) {
        IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
        Metadata metadata = autograph.getMetadata();
        if (metadata == null) {
            metadata = new Metadata();
            metadata.setAutograph(autograph);
        }
        if (Objects.equals(metadata.getDataIpfsUri(), null)) {
            String dataIpfsHash = uploadDataToIpfs(ipfs, autograph.getAttachment().getPath(userId));
            if (!Objects.equals(dataIpfsHash, null)) {
                metadata.setDataIpfsUri(IPFS_PREFIX + dataIpfsHash);
            } else {
                return null;
            }
        }

        if (Objects.equals(metadata.getMetadataIpfsUri(), null)) {
            String metadataIpfsHash = uploadMetadataToIpfs(ipfs, metadata);
            if (!Objects.equals(metadataIpfsHash, null)) {
                metadata.setMetadataIpfsUri(IPFS_PREFIX + metadataIpfsHash);
            } else {
                return null;
            }
        }

        autograph.setMetadata(metadata);
        return autograph;
    }

    private static String uploadDataToIpfs(IPFS ipfs, String dataFilePath) {
        NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(new File(dataFilePath));
        try {
            MerkleNode addResult = ipfs.add(file).get(0);
            return addResult.hash.toBase58();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String uploadMetadataToIpfs(IPFS ipfs, Metadata metadata) {
        String metadataJson = metadata.toJson();
        NamedStreamable.ByteArrayWrapper file = new NamedStreamable.ByteArrayWrapper(
            "metadata.json", metadataJson.getBytes());
        try {
            MerkleNode addResult = ipfs.add(file).get(0);
            return addResult.hash.toBase58();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
