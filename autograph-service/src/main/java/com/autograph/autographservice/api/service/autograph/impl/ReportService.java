package com.autograph.autographservice.api.service.autograph.impl;

import com.autograph.autographservice.api.dto.autograph.request.ReportAutographRequestDTO;
import com.autograph.autographservice.api.dto.autograph.request.ReportAutographWithoutAttachmentRequestDTO;
import com.autograph.autographservice.api.dto.autograph.response.ReportAutographResponseDTO;
import com.autograph.autographservice.api.entity.autograph.Report;
import com.autograph.autographservice.api.entity.autograph.ReportAttachment;
import com.autograph.autographservice.api.enums.ReportReason;
import com.autograph.autographservice.api.enums.ResponseCodes;
import com.autograph.autographservice.api.repository.autograph.IAutographRepository;
import com.autograph.autographservice.api.repository.autograph.IReportRepository;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.IReportService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReportService implements IReportService {
    private final IReportRepository reportRepository;
    private final ModelMapper modelMapper;
    private final ReportAttachmentService attachmentService;
    private final AutographService autographService;
    private final IAutographRepository autographRepository;
    private final ApiResponse apiResponse;
    private int numberOfItemsOnPage = 10;

    @Autowired
    public ReportService(IReportRepository reportRepository, ModelMapper modelMapper, ReportAttachmentService attachmentService, AutographService autographService, IAutographRepository autographRepository, ApiResponse apiResponse) {
        this.reportRepository = reportRepository;
        this.modelMapper = modelMapper;
        this.attachmentService = attachmentService;
        this.autographService = autographService;
        this.autographRepository = autographRepository;
        this.apiResponse = apiResponse;
    }

    @Override
    public ApiResponse getReportById(Long reportId) {
        Optional<Report> report = reportRepository.findById(reportId);

        if(report.isPresent()){
            apiResponse.setResponse(modelMapper.map(report, ReportAutographResponseDTO.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getReportsByAutographId(Long autographId, int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
        List<Report> report = reportRepository.findByAutographId(autographId, pageable);

        if(!report.isEmpty()){
            List<ReportAutographResponseDTO> reportAutographResponseDTOS = report.stream()
                    .map(reportItem -> modelMapper.map(reportItem, ReportAutographResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setResponse(reportAutographResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getReportsByUserId(Long userId, int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
        List<Report> report = reportRepository.findByUserId(userId, pageable);

        if(!report.isEmpty()){
            List<ReportAutographResponseDTO> reportAutographResponseDTOS = report.stream()
                    .map(reportItem -> modelMapper.map(reportItem, ReportAutographResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setResponse(reportAutographResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse reportAutographById(ReportAutographRequestDTO reportAutographRequestDTO, Long userId) {
        if (Objects.equals(reportAutographRequestDTO.getFile(), null)) {
            apiResponse.setCode(ResponseCodes.FILE_CANNOT_BE_NULL.getValue());
            apiResponse.setResponse(false);

            return apiResponse;
        }

        ReportAttachment attachment = attachmentService.storeFile(reportAutographRequestDTO.getFile(), userId);
        Report report = new Report(autographRepository.getById(reportAutographRequestDTO.getAutographId()), userId, reportAutographRequestDTO.getReason(),
                reportAutographRequestDTO.getComment(), attachment);

        reportRepository.save(report);

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(modelMapper.map(report, ReportAutographResponseDTO.class));

        return apiResponse;
    }

    @Override
    public ApiResponse getAllReports(int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
        Page<Report> report = reportRepository.findAll(pageable);

        if(!report.isEmpty()){
            List<ReportAutographResponseDTO> reportAutographResponseDTOS = report.stream()
                    .map(reportItem -> modelMapper.map(reportItem, ReportAutographResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setResponse(reportAutographResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getReportByReason(ReportReason reason, int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
        List<Report> report = reportRepository.findByReason(reason, pageable);

        if(!report.isEmpty()){
            List<ReportAutographResponseDTO> reportAutographResponseDTOS = report.stream()
                    .map(reportItem -> modelMapper.map(reportItem, ReportAutographResponseDTO.class))
                    .collect(Collectors.toList());

            apiResponse.setResponse(reportAutographResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse reportAutographWithoutAttachmentById(ReportAutographWithoutAttachmentRequestDTO reportAutographRequestDTO, Long userId) {
        Report report = new Report(autographRepository.getById(reportAutographRequestDTO.getAutographId()), userId, reportAutographRequestDTO.getReason(),
                reportAutographRequestDTO.getComment(), null);

        reportRepository.save(report);

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(modelMapper.map(report, ReportAutographResponseDTO.class));

        return apiResponse;
    }
}
