package com.autograph.autographservice.api.entity.autograph;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "attachments")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Attachment {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "attachment")
    private Autograph autograph;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "document_type")
    private String documentType;

    @Column(name = "upload_directory")
    private String uploadDirectory;

    public Attachment(String name, String documentType, String uploadDirectory) {
        this.fileName = name;
        this.documentType = documentType;
        this.uploadDirectory = uploadDirectory;
    }

    public String getPath(Long userId) {
        String separator = System.getProperty("file.separator");
        return uploadDirectory + separator + userId + separator + "attachments" + separator
            + fileName;
    }
}
