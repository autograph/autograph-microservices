package com.autograph.autographservice.api.repository.autograph;

import com.autograph.autographservice.api.entity.autograph.AssetContract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IAssetContractRepository extends JpaRepository<AssetContract, Long> {

    Optional<AssetContract> findById(Long id);
}
