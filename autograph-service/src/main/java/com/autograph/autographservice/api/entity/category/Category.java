package com.autograph.autographservice.api.entity.category;

import com.autograph.autographservice.api.entity.autograph.Autograph;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @Id
    @GeneratedValue
    Long id;

    String name;
    Long totalAutographs = 0L;
    Long monetizedAutographs = 0L;
    @ManyToMany(mappedBy = "categories")
    Set<Autograph> autographs = new HashSet<>();

    public Category(String name) {
        this.name = name;
    }

}
