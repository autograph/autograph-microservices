package com.autograph.autographservice.api.controller.category;

import com.autograph.autographservice.api.dto.category.request.CategoryRequestDTO;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.category.impl.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CategoryController {

    private final CategoryService categoryService;
    private final ResourceServerTokenServices resourceServerTokenServices;

    @Autowired
    public CategoryController(CategoryService categoryService,
        ResourceServerTokenServices resourceServerTokenServices) {
        this.categoryService = categoryService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }

    @PostMapping("/category")
    public ApiResponse createCategory(@RequestBody CategoryRequestDTO categoryRequestDTO) {
        return categoryService.createCategory(categoryRequestDTO);
    }

    @DeleteMapping("/category/{id}")
    public ApiResponse deleteCategoryById(@PathVariable("id") Long categoryId) {
        return categoryService.deleteCategory(categoryId);
    }

    @GetMapping("/category")
    public ApiResponse getListOfCategories(@RequestHeader("Authorization") String auth,
        OAuth2Authentication authentication) {
        System.out.println(authentication);
        HashMap<String, Object> userDetails = getUserDetails(auth);
        return categoryService.getListOfCategories();
    }

    @GetMapping("/category/ids")
    public ApiResponse getListOfCategoriesById(@RequestBody Collection<Long> idList) {
        return categoryService.getListOfCategoriesByListOfIds(idList);
    }

    @PostMapping("/autograph/{autographId}/category/{categoryId}")
    public ApiResponse addCategoryToAutograph(@PathVariable("autographId") Long autographId,
        @PathVariable("categoryId") Long categoryId) {
        return categoryService.addCategoryToAutograph(categoryId, autographId);
    }

    @PostMapping("/autograph/{autographId}/categories")
    public ApiResponse addCategoriesToAutograph(@RequestBody List<Long> categoryIds,
        @PathVariable("autographId") Long autographId) {
        return categoryService.addCategoriesToAutograph(categoryIds, autographId);
    }

    @PostMapping("/autograph/{autographId}/categories/set")
    public ApiResponse setCategoriesToAutograph(@RequestBody List<Long> categoryIds,
                                                @PathVariable("autographId") Long autographId) {
        return categoryService.setCategoriesToAutograph(categoryIds, autographId);
    }

    HashMap<String, Object> getUserDetails(String token) {
        if (!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                .get("userDetails");
        }

        return null;
    }
}
