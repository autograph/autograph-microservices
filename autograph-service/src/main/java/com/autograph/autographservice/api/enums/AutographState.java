package com.autograph.autographservice.api.enums;

public enum AutographState {
    SUBMITTED,
    FROZEN,
    SIGNED_FOR_LAZY_MINT,
    MINTED
}
