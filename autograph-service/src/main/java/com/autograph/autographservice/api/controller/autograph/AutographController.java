package com.autograph.autographservice.api.controller.autograph;

import com.autograph.autographservice.api.dto.autograph.request.AutographDTO;
import com.autograph.autographservice.api.dto.autograph.request.ImportedAutographDTO;
import com.autograph.autographservice.api.dto.autograph.request.SaleDTO;
import com.autograph.autographservice.api.response.ApiResponse;
import com.autograph.autographservice.api.service.autograph.impl.AttachmentService;
import com.autograph.autographservice.api.service.autograph.impl.AutographService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AutographController {

    private final AutographService userAutographService;
    private final AttachmentService attachmentService;
    private final ResourceServerTokenServices resourceServerTokenServices;


    @Autowired
    public AutographController(AutographService userAutographService,
        AttachmentService attachmentService,
        ResourceServerTokenServices resourceServerTokenServices) {
        this.userAutographService = userAutographService;
        this.attachmentService = attachmentService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }

    @PostMapping("/autograph")
    public ApiResponse addAutograph(@ModelAttribute AutographDTO autographDTO,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return userAutographService.addAutograph(userId, autographDTO, authorization);
    }

    @GetMapping("/autograph/{autographId}/owner")
    public ApiResponse getAutographOwnerId(@PathVariable("autographId") Long autographId, @RequestHeader("Authorization") String authorization) {
        return userAutographService.getAutographOwnerId(autographId, authorization);
    }

    @PostMapping("/autograph/import")
    public ApiResponse importAutograph(@ModelAttribute ImportedAutographDTO importedAutographDTO,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.importAutograph(userId, importedAutographDTO, authorization);
    }

    @PostMapping("/autograph/import/multiple")
    public ApiResponse importAutographs(@ModelAttribute List<ImportedAutographDTO> importedAutographsDTOs,
                                       @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.importAutographs(userId, importedAutographsDTOs,authorization);
    }

    @GetMapping("/autograph/{autographId}/asset")
    public ApiResponse getAssetContractByAutographId(
        @PathVariable("autographId") Long autographId) {
        return userAutographService.getAssetContractByAutographId(autographId);
    }

    @GetMapping("/autograph/deleted")
    public ApiResponse getNumberOfDeletedAutographsByUserId(
            @RequestParam("id") Long userId) {
        return userAutographService.getNumberOfDeletedAutographsByUserId(userId);
    }

    @GetMapping("/autograph/owned/{page}")
    public ApiResponse getUserOwnedAutographs(@PathVariable("page") int page,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getUserAutographs(page, authorization, userId);
    }

    @GetMapping("/autograph/owned/user/{userId}/{page}")
    public ApiResponse getUserOwnedAutographsByUserId(@PathVariable("page") int page, @PathVariable("userId") Long userId,
                                              @RequestHeader("Authorization") String authorization) {
        return userAutographService.getUserAutographs(page, authorization, userId);
    }

    @GetMapping("/autograph/created/{page}")
    public ApiResponse getUserCreatedAutographs(@PathVariable("page") int page,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getUserCreatedAutographs(page,authorization, userId);
    }

    @PostMapping("/autograph/list")
    public ApiResponse getAutographsByList(@RequestBody List<Long> ids, @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getAutographsByIdList(ids,authorization, userId);
    }

    @GetMapping("/autograph/all")
    public ApiResponse getAllAutographs(@RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getAllAutographs(userId, authorization);
    }


    @GetMapping("/autograph/reported")
    public ApiResponse getReportedAutographs(@RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getReportedAutographs(userId, authorization);
    }

    @GetMapping("/autograph/blocked/{status}")
    public ApiResponse getBlockedAutographs(@PathVariable("status") Boolean status, @RequestHeader("Authorization") String authorization) {
        return userAutographService.getBlockedAutographs(status, authorization);
    }

    @PostMapping("/autograph/{autographId}/block/{status}")
    public ApiResponse getBlockedAutographs(@PathVariable("autographId") Long autographId,
                                            @PathVariable("status") Boolean status, @RequestHeader("Authorization") String authorization) {
        return userAutographService.blockAutographById(autographId, authorization, status);
    }


    @GetMapping("/autograph/verified/{status}")
    public ApiResponse getVerifiedAutographs(@PathVariable("status") Boolean status, @RequestHeader("Authorization") String authorization) {
        return userAutographService.getVerifiedAutographs(status, authorization);
    }

    @GetMapping("/autograph/{id}")
    public ApiResponse getAutographById(@PathVariable("id") Long id,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.getAutographById(id, authorization, userId);
    }

    @GetMapping("/autograph/{id}/public")
    public ApiResponse getAutographByIdPublic(@PathVariable("id") Long id) {
        return userAutographService.getAutographByIdPublic(id);
    }

    @GetMapping("/autograph/{id}/category")
    public ApiResponse getAutographCategories(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return userAutographService.getCategoriesOfAutographById(id, authorization);
    }


    @GetMapping("/autograph/{id}/previous-owners")
    public ApiResponse getAutographPreviousOwners(@RequestHeader("Authorization") String authorization,
                                                  @PathVariable("id") Long autographId) {

        return userAutographService.getAutographPreviousOwners(autographId, authorization);
    }

    @GetMapping("/autograph/category/{categoryId}/{page}")
    public ApiResponse getAutographsByCategory(@PathVariable("categoryId") Long categoryId,
        @PathVariable("page") int page, @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return userAutographService.getAutographsByCategory(categoryId, authorization, userId, page);
    }

    @GetMapping("/autograph/search-feed/{page}")
    public ApiResponse getSearchFeed(@PathVariable("page") int page, @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return userAutographService.getSearchFeed(page, authorization, userId);
    }

    @PutMapping("/autograph/{id}")
    public ApiResponse editAutographById(@PathVariable("id") Long id,
        @ModelAttribute AutographDTO autographDTO,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.updateAutograph(userId, authorization, id, autographDTO);
    }

    @DeleteMapping("/autograph/{id}")
    public ApiResponse deleteAutographById(@PathVariable("id") Long id,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.deleteAutographById(userId, authorization, id);
    }

    @PostMapping("/autograph/{autographId}/like")
    public ApiResponse likeAutographById(@PathVariable("autographId") Long autographId,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.likeAutograph(autographId, userId, authorization);
    }

    @GetMapping("/autograph/{id}/like")
    public ApiResponse getAutographLikes(@PathVariable("id") Long id,
        @RequestHeader("Authorization") String authorization) {
        return userAutographService.getAutographLikes(id, authorization);
    }


    @GetMapping("/autograph/{id}/is-liked")
    public ApiResponse getAutographIsLiked(@PathVariable("id") Long id,
                                         @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.isAutographLiked(id, userId, authorization);
    }

    @GetMapping("/autograph/files")
    public ApiResponse listAttachments(@RequestHeader("Authorization") String authorization) {
        return attachmentService.listAllAttachments();
    }

    @GetMapping("/autograph/files/{id}")
    public ApiResponse getAttachmentById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization) {
        return attachmentService.getAttachmentById(id);
    }

    @GetMapping("/autograph/search")
    public ApiResponse search(@RequestParam String query, @RequestHeader("Authorization") String authorization) {
        return userAutographService.search(query, authorization);
    }

    @GetMapping("/autograph/autographs-info")
    public ApiResponse getAutographsInfo() {
        return userAutographService.getAutographsInfo();
    }

    @PostMapping("/autograph/{id}/sell")
    public ApiResponse putOnSale(@PathVariable("id") Long id, @RequestBody SaleDTO saleDTO,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.putAutographOnSale(userId, id, saleDTO, authorization);
    }

    HashMap<String, Object> getUserDetails(String token) {
        if (!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token) {
        HashMap<String, Object> userDetails = getUserDetails(token);

        if (userDetails != null) {
            return ((Integer) userDetails.get("id")).longValue();
        } else {
            return null;
        }
    }

    @GetMapping("/autograph/{id}/freeze")
    public ApiResponse freeze(@PathVariable("id") Long id,
        @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);

        return userAutographService.freezeAutograph(userId, authorization, id);
    }
}
