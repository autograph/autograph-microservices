package com.autograph.autographservice.api.service.autograph;

import com.autograph.autographservice.api.entity.autograph.ReportAttachment;
import com.autograph.autographservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IReportAttachmentService {
    ReportAttachment storeFile(MultipartFile file, Long id);

    ApiResponse listAllAttachments();

    ApiResponse getAttachmentById(Long id);
}
