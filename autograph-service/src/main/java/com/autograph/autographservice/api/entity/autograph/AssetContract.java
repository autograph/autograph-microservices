package com.autograph.autographservice.api.entity.autograph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AssetContract {

    @OneToOne(fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "assetContract")
    Autograph autograph;
    @Id
    @GeneratedValue
    private Long id;
    private String tokenId;
    private String animationUrl;
    private String animationOriginalUrl;
    private String address;
    private String assetContractType;
    private String buyerFeeBasisPoints;
    private BigInteger openseaBuyerFeeBasisPoints;
    private BigInteger openseaSellerFeeBasisPoints;
    @DateTimeFormat
    private LocalDateTime createdAt;
    private String description;
    private String externalLink;
    private String name;
    private String nftVersion;
    private String openseaVersion;
    private String onlyProxiedTransfers;
    private BigInteger ownerId;
    private String payoutAddress;
    private String schemaName;
    private String totalSupply;
    private String ownerAddress, creatorAddress;
    private String transferFee;

}
