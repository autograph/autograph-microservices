package com.autograph.autographservice.api.entity.autograph;

import com.autograph.autographservice.api.enums.AutographType;
import lombok.*;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONStyle;

import javax.persistence.*;

@Entity
@Table(name = "metadata")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Metadata {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "metadata")
    private Autograph autograph;

    @Column(name = "data_ipfs_uri")
    private String dataIpfsUri;

    @Column(name = "metadata_ipfs_uri")
    private String metadataIpfsUri;

    public String toJson() {
        // TODO: Add `external_link` field.
        JSONObject jsonObject = new JSONObject()
            .appendField("name", autograph.getTitle())
            .appendField("description", autograph.getDescription());

        if (AutographType.IMAGE.equals(autograph.getAutographType())) {
            jsonObject
                .appendField("image", dataIpfsUri);
        } else {
            // TODO: Implement other types.
            throw new RuntimeException("Not yet implemented!");
        }

        return jsonObject.toJSONString(JSONStyle.NO_COMPRESS);
    }
}
