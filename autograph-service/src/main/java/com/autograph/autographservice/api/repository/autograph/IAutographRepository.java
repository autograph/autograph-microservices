package com.autograph.autographservice.api.repository.autograph;

import com.autograph.autographservice.api.entity.autograph.Autograph;
import com.autograph.autographservice.api.entity.category.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IAutographRepository extends JpaRepository<Autograph, Long> {

    Optional<Autograph> findByIdAndIsDeletedFalse(Long id);

    List<Autograph> findAllByOwnerAndIsDeletedFalse(Long owner, Pageable pageable);

    List<Autograph> findAllByCreatorAndIsDeletedFalse(Long creator, Pageable pageable);

    List<Autograph> findAllByCategoriesContainsAndIsDeletedFalse(Category category, Pageable pageable);

    List<Autograph> findAllByTitleContainingAndIsDeletedFalse(String title);

    List<Autograph> findAllByIsBlockedAndIsDeletedFalse(Boolean blocked);

    List<Autograph> findAllByIsVerifiedAndIsDeletedFalse(Boolean verified);

    Page<Autograph> findAllByIsDeletedFalse(Pageable page);

    Integer countByIdNotNull();

    @Query(value = "SELECT a FROM Autograph a WHERE year(a.createdAt) = :year")
    List<Autograph> countByCreatedDate(@Param("year") Integer year);

    @Query("SELECT a FROM Autograph a WHERE year(a.createdAt)=:year AND month(a.createdAt)=:month")
    List<Autograph> countByCreatedDateMonthAndCreatedDateYear(@Param("month") Integer month,@Param("year") Integer year);

    Long countByIsDeletedAndOwner(Boolean status ,Long ownerId);

}
