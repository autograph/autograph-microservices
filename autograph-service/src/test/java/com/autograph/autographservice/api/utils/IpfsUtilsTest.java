package com.autograph.autographservice.api.utils;

import com.autograph.autographservice.api.entity.autograph.Attachment;
import com.autograph.autographservice.api.entity.autograph.Autograph;
import com.autograph.autographservice.api.enums.AutographType;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class IpfsUtilsTest {

    @Test
    void uploadToIpfs() {
        final int ipfsHashLength = 46;
        String ipfsPrefix = "ipfs://";
        Autograph autograph = new Autograph();
        autograph.setAutographType(AutographType.IMAGE);
        autograph.setTitle("Test Autograph");
        autograph.setDescription("Test autograph description.");
        Attachment attachment = new Attachment();
        String separator = System.getProperty("file.separator");
        attachment.setUploadDirectory(Paths.get("").toAbsolutePath() + separator + "files");
        attachment.setFileName("0.jpg");
        autograph.setAttachment(attachment);
        autograph = IpfsUtils.uploadToIpfs(0L, autograph);

        assertNotNull(autograph);
        assertNotNull(autograph.getMetadata());
        assertEquals(autograph, autograph.getMetadata().getAutograph());
        assertNotNull(autograph.getMetadata().getDataIpfsUri());
        assertNotNull(autograph.getMetadata().getMetadataIpfsUri());
        assertEquals(autograph.getMetadata().getDataIpfsUri().length(),
            ipfsPrefix.length() + ipfsHashLength);
        assertEquals(autograph.getMetadata().getMetadataIpfsUri().length(),
            ipfsPrefix.length() + ipfsHashLength);
        assertTrue(autograph.getMetadata().getDataIpfsUri().startsWith(ipfsPrefix));
        assertTrue(autograph.getMetadata().getMetadataIpfsUri().startsWith(ipfsPrefix));
    }
}
