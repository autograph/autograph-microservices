package com.autograph.promotionservice.api.controller.promotion;

import com.autograph.promotionservice.api.dto.request.PromotionAutographList;
import com.autograph.promotionservice.api.dto.request.PromotionRequestDTO;
import com.autograph.promotionservice.api.response.ApiResponse;
import com.autograph.promotionservice.api.service.promotion.impl.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class PromotionController {
    private final ResourceServerTokenServices resourceServerTokenServices;
    private final PromotionService promotionService;

    @Autowired
    public PromotionController(
            ResourceServerTokenServices resourceServerTokenServices, PromotionService promotionService) {
        this.resourceServerTokenServices = resourceServerTokenServices;
        this.promotionService = promotionService;
    }

    @PostMapping("promotion")
    public ApiResponse createPromotion(@RequestBody PromotionRequestDTO promotionRequestDTO,
                                       @RequestHeader("Authorization") String authorization)
    {
        Long userId = getUserId(authorization);
        return promotionService.createPromotion(promotionRequestDTO, userId, authorization);
    }

    @GetMapping("promotion/{page}")
    public ApiResponse getPromotions(@PathVariable("page") int page, @RequestHeader("Authorization") String authorization)
         {
        return promotionService.getAllPromotions(page, authorization);
    }

    @GetMapping("promotion/user/{userId}/{page}")
    public ApiResponse getPromotions(@PathVariable("userId") Long userId, @RequestHeader("Authorization") String authorization,
                                     @PathVariable("page") int page)
    {
        return promotionService.getPromotionsByUserId(userId, authorization, page);
    }

    @GetMapping("promotion/collection/{id}")
    public ApiResponse getPromotionById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization)
         {
        return promotionService.getPromotionById(id, authorization);
    }

    @DeleteMapping("promotion/collection/{id}")
    public ApiResponse deletePromotionById(@PathVariable("id") Long id,
                                           @RequestHeader("Authorization") String authorization)
         {
            Long userId = getUserId(authorization);
            return promotionService.removePromotion(id, userId, authorization);
    }

    @PatchMapping("promotion/collection/{id}")
    public ApiResponse editPromotionById(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization,
        @RequestBody PromotionRequestDTO promotionRequestDTO)  {
        Long userId = getUserId(authorization);
        return promotionService.editPromotion(id, promotionRequestDTO, userId, authorization);
    }

    @PostMapping("promotion/collection/{id}/autographs/{autographId}")
    public ApiResponse addAutographToPromotion(@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization,
        @PathVariable("autographId") Long autographId)  {
        return promotionService.addAutographToPromotion(autographId, id, authorization);
    }

    @DeleteMapping("promotion/collection/{id}/autographs/{autographId}")
    public ApiResponse deleteAutographFromPromotion(@PathVariable("id") Long id,
        @PathVariable("autographId") Long autographId, @RequestHeader("Authorization") String authorization)  {
        Long userId = getUserId(authorization);
        return promotionService.deleteAutographFromPromotion(id, autographId, userId, authorization);
    }

    @PostMapping("promotion/collection/{id}/autographs")
    public ApiResponse addAutographsListToPromotion(@PathVariable("id") Long id,
        @RequestBody PromotionAutographList promotionAutographList,
                                                    @RequestHeader("Authorization") String authorization)  {
        return promotionService
            .addListOfAutographsToPromotion(promotionAutographList.getAutographsId(), id, authorization);
    }

    HashMap<String, Object> getUserDetails(String token){
        if(!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token){
        HashMap<String, Object> userDetails = getUserDetails(token);

        if(userDetails != null)
            return ((Integer) userDetails.get("id")).longValue();
        else return  null;
    }

}
