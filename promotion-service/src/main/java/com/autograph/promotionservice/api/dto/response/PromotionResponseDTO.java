package com.autograph.promotionservice.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PromotionResponseDTO {

    Long id;
    Long user;
    List<AutographResponseDTO> autographs;
    String title;
    String about;

    UserHeaderRequestDTO userHeaderRequestDTO;
}
