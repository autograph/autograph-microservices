package com.autograph.promotionservice.api.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class PromotionAutographList {

    List<Long> autographsId;
}
