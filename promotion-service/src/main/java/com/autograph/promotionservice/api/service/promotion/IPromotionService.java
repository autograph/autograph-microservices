package com.autograph.promotionservice.api.service.promotion;

import com.autograph.promotionservice.api.dto.request.PromotionRequestDTO;
import com.autograph.promotionservice.api.response.ApiResponse;

import java.util.List;

public interface IPromotionService {

    ApiResponse getAllPromotions(int page, String authorization);

    ApiResponse getPromotionById(Long id, String authorization);

    ApiResponse addAutographToPromotion(Long autographId, Long promotionId, String authorization);

    ApiResponse addListOfAutographsToPromotion(List<Long> autographIds, Long promotionId, String authorization);

    ApiResponse createPromotion(PromotionRequestDTO promotionRequestDTO, Long userId, String authorization);

    ApiResponse removePromotion(Long promotionId, Long userId, String authorization);

    ApiResponse editPromotion(Long promotionId, PromotionRequestDTO promotionRequestDTO,
        Long userId, String authorization);

    ApiResponse deleteAutographFromPromotion(Long promotionId, Long autographId, Long userId, String authorization);

    ApiResponse getPromotionsByUserId(Long userId, String authorization, int page);
}
