package com.autograph.promotionservice.api.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Promotion {

    @Id
    @GeneratedValue
    Long id;

    Long owner;

    String title;
    String about;

    @ElementCollection
    List<Long> autographList = new ArrayList<>();


    public Promotion(Long owner, String title, String about) {
        this.owner = owner;
        this.title = title;
        this.about = about;
    }
}
