package com.autograph.promotionservice.api.service.promotion.impl;

import com.autograph.promotionservice.api.dto.request.PromotionRequestDTO;
import com.autograph.promotionservice.api.dto.response.AutographResponseDTO;
import com.autograph.promotionservice.api.dto.response.PromotionResponseDTO;
import com.autograph.promotionservice.api.dto.response.UserHeaderRequestDTO;
import com.autograph.promotionservice.api.entity.Promotion;
import com.autograph.promotionservice.api.enums.ResponseCodes;
import com.autograph.promotionservice.api.repository.promotion.IPromotionRepository;
import com.autograph.promotionservice.api.response.ApiResponse;
import com.autograph.promotionservice.api.service.promotion.IPromotionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PromotionService implements IPromotionService {

    private final IPromotionRepository promotionRepository;
    private final ApiResponse apiResponse;
    private final int numberOfItemsOnPage = 10;
    private final CircuitBreakerFactory<?, ?> cbFactory;
    private final WebClient.Builder webClientBuilder;
    private final ModelMapper modelMapper;


    @Autowired
    public PromotionService(ModelMapper modelMapper,
        IPromotionRepository promotionRepository, ApiResponse apiResponse,
        CircuitBreakerFactory<?, ?> cbFactory, WebClient.Builder webClientBuilder) {
        this.promotionRepository = promotionRepository;
        this.apiResponse = apiResponse;
        this.cbFactory = cbFactory;
        this.webClientBuilder = webClientBuilder;
        this.modelMapper = modelMapper;
    }

    @Override
    public ApiResponse getAllPromotions(int page, String authorization) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        Page<Promotion> promotions = promotionRepository.findAll(pageable);
        List<PromotionResponseDTO> promotionResponseDTOS = promotions
            .stream()
            .map(promotionResponseDTO -> promotionMapper(promotionResponseDTO, authorization))
            .collect(Collectors.toList());

        apiResponse.setResponse(promotionResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse getPromotionById(Long id, String authorization) {
        ApiResponse apiResponse = new ApiResponse();
        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(id);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();

            ApiResponse userHeaderResponse = webClientBuilder.build()
                    .get()
                    .uri("http://user-service/api/user/header/" + promotion.getOwner())
                    .header("Authorization", String.valueOf(authorization))
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

            UserHeaderRequestDTO userHeaderRequestDTO =
                    modelMapper.map(userHeaderResponse.getResponse(), UserHeaderRequestDTO.class);

            //TODO Get promotion autograph ids
            PromotionResponseDTO promotionResponseDTO = promotionMapper(promotion, authorization);
            promotionResponseDTO.setUserHeaderRequestDTO(userHeaderRequestDTO);

            apiResponse.setResponse(promotionResponseDTO);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }


    @Override
    public ApiResponse addAutographToPromotion(Long autographId, Long promotionId, String authorization) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(promotionId);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();
            promotion.getAutographList().add(autographId);

            promotionRepository.save(promotion);

            apiResponse.setResponse(promotionMapper(promotion, authorization));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public ApiResponse addListOfAutographsToPromotion(List<Long> autographIds, Long promotionId
            , String authorization) {
        ApiResponse apiResponse = new ApiResponse();

        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(promotionId);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();

            for (Long autographId : autographIds) {
                promotion.getAutographList().add(autographId);
            }
            promotionRepository.save(promotion);

            apiResponse.setResponse(promotionMapper(promotion, authorization));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse createPromotion(PromotionRequestDTO promotionRequestDTO, Long userId,
                                       String authorization) {
        Promotion promotion = new Promotion(userId, promotionRequestDTO.getTitle(),
            promotionRequestDTO.getAbout());

        for (Long autographId : promotionRequestDTO.getAutographIds()) {
            promotion.getAutographList().add(autographId);
        }

        promotionRepository.save(promotion);

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(promotionMapper(promotion, authorization));

        return apiResponse;
    }

    @Override
    public ApiResponse removePromotion(Long promotionId, Long userId, String authorization) {
        ApiResponse apiResponse = new ApiResponse();
        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(promotionId);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();
            if (promotion.getOwner().equals(userId)) {

                promotionRepository.delete(promotion);
                apiResponse.setResponse(promotionMapper(promotion, authorization));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setCode(ResponseCodes.NOT_OWNER.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse editPromotion(Long promotionId, PromotionRequestDTO promotionRequestDTO,
                                     Long userId, String authorization) {
        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(promotionId);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();

            if (promotion.getOwner().equals(userId)) {
                promotion.setAbout(promotionRequestDTO.getAbout());
                promotion.setTitle(promotionRequestDTO.getTitle());

                promotionRepository.save(promotion);
                apiResponse.setResponse(promotionMapper(promotion, authorization));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setCode(ResponseCodes.NOT_OWNER.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse deleteAutographFromPromotion(Long promotionId, Long autographId,
                                                    Long userId, String authorization) {
        Optional<Promotion> promotionOptional = promotionRepository.findPromotionById(promotionId);

        if (promotionOptional.isPresent()) {
            Promotion promotion = promotionOptional.get();
            if (promotion.getOwner().equals(userId)) {
                List<Long> autographs = promotion.getAutographList();
                autographs.remove(autographId);
                promotion.setAutographList(autographs);

                promotionRepository.save(promotion);

                apiResponse.setResponse(promotionMapper(promotion, authorization));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setCode(ResponseCodes.NOT_OWNER.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.PROMOTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getPromotionsByUserId(Long userId, String authorization, int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        Page<Promotion> promotions = promotionRepository.findAllByOwner(userId, pageable);
        List<PromotionResponseDTO> promotionResponseDTOS = promotions
                .stream()
                .map(promotionResponseDTO -> promotionMapper(promotionResponseDTO, authorization))
                .collect(Collectors.toList());

        apiResponse.setResponse(promotionResponseDTOS);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    PromotionResponseDTO promotionMapper(Promotion promotion, String authorization) {
        ApiResponse response = cbFactory.create("get-list-of-autograph-dtos")
                .run(() -> getListOfAutographDTOs(promotion.getAutographList(), authorization),
                        throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                                Collections.emptyList()));

        List<AutographResponseDTO> autographResponseDTOs = modelMapper.map(response.getResponse(),
                List.class);

        PromotionResponseDTO promotionResponseDTO = new PromotionResponseDTO(promotion.getId(),
            promotion.getOwner(),
            autographResponseDTOs,
            promotion.getTitle(),
            promotion.getAbout(),
                null);

        return promotionResponseDTO;
    }

    private ApiResponse getListOfAutographDTOs(List<Long> autographLikes, String authorization) {
        System.out.println("getListOfAutographs");
        ApiResponse response =  webClientBuilder.build()
            .post()
            .uri("http://autograph-service/api/autograph/list")
                .header("Authorization", authorization)
            .bodyValue(autographLikes.toArray())
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();

        System.out.println(response);
        return response;
    }
}
