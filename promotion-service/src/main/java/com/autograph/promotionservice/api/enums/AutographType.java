package com.autograph.promotionservice.api.enums;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
