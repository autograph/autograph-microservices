package com.autograph.promotionservice.api.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class PromotionRequestDTO {

    List<Long> autographIds;
    String title;
    String about;
}
