package com.autograph.promotionservice.api.repository.promotion;

import com.autograph.promotionservice.api.entity.Promotion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IPromotionRepository extends JpaRepository<Promotion, Long> {

    Optional<Promotion> findPromotionById(Long id);

    List<Promotion> findAllByOwner(Long owner);

    Page<Promotion> findAllByOwner(Long owner, Pageable pageable);

    Page<Promotion> findAll(Pageable pageable);
}
