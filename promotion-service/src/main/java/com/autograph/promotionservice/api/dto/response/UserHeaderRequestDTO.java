package com.autograph.promotionservice.api.dto.response;

import lombok.Data;

@Data
public class UserHeaderRequestDTO {

    private Long id;
    private String email;
    private String username;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private String address;
}
