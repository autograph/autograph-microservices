# Autograph Microservices

## Code Style

After cloning, and opening the project in IntelliJ, import the code style from the `style.xml` file
in IntelliJ setting from `CodeStyle.xml` file in the project root, and select it as the default:
Settings -> Editor -> Code Style -> Scheme -> ⚙ -> Import Scheme -> IntelliJ IDEA code style XML 