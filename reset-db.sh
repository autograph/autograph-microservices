#!/bin/bash

reset_db() {
    echo "Resetting \"$1\"."
    sudo -iu postgres dropdb "$1"
    sudo -iu postgres createdb "$1"
    sleep 1
}

reset_db autograph-config-server
reset_db api-gateway
reset_db discovery-service
reset_db activity-service
reset_db feed-service
reset_db auction-service
reset_db promotion-service
reset_db autograph-service
reset_db user-service
