package com.autograph.auctionservice.api.service;

import com.autograph.auctionservice.api.dto.request.SaleDTO;
import com.autograph.auctionservice.api.dto.response.AuctionResponseDTO;
import com.autograph.auctionservice.api.entity.Auction;
import com.autograph.auctionservice.api.response.ApiResponse;

import java.math.BigDecimal;
import java.util.List;

public interface IAuctionService {

    ApiResponse getAllAuctions(int page) throws Exception;

    ApiResponse findAuctionsByOwner(Long userId, int page) throws Exception;

    ApiResponse getAllByStatus(Boolean status, int page) throws Exception;

    ApiResponse getAllEndingSoon(int page) throws Exception;

    ApiResponse findAuctionById(Long auctionId) throws Exception;

    ApiResponse findAuctionByAutographId(Long autographId) throws Exception;

    ApiResponse getAuctionDetails(Long auctionId) throws Exception;

    AuctionResponseDTO createAuction(Long autographId, Long autographOwnerId, SaleDTO auction,
        Long userId) throws Exception;

    ApiResponse updateAuction(Long auctionId, SaleDTO auction, Long userId) throws Exception;

    ApiResponse deleteAuction(Long auctionId, Long userId, String authorization) throws Exception;

    boolean isBidHigher(Long auctionId, BigDecimal bidValue);

    BigDecimal getLastPrice(Long auctionId);

    List<Auction> getAll();
}
