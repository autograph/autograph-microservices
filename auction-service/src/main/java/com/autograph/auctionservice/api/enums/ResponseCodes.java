package com.autograph.auctionservice.api.enums;

import com.autograph.auctionservice.api.response.ResponseDTO;

public enum ResponseCodes {
    SUCCESS(new ResponseDTO("success", 2000, "Successfully completed!")),
    AUTOGRAPH_NOT_FOUND(
        new ResponseDTO("autograph not found", 2002, "An exception occurred, no autograph found")),
    USER_NOT_FOUND(new ResponseDTO("error", 2003, "User not found")),
    NOT_FOUND(new ResponseDTO("not found", 2005, "Not found")),
    AUCTION_NOT_FOUND(new ResponseDTO("auction not found", 2006, "Auction not found")),
    YOUR_BID_IS_LESS_THAN_PREVIOUS_BID(
        new ResponseDTO("your bid is less than the previous bid", 2013,
            "Your bid should be more than previous bid or minimal bid"));

    private final ResponseDTO value;

    ResponseCodes(ResponseDTO dto) {
        this.value = dto;
    }

    public ResponseDTO getValue() {
        return value;
    }
}
