package com.autograph.auctionservice.api.controller;

import com.autograph.auctionservice.api.dto.request.SaleDTO;
import com.autograph.auctionservice.api.dto.response.AuctionResponseDTO;
import com.autograph.auctionservice.api.response.ApiResponse;
import com.autograph.auctionservice.api.service.impl.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AuctionController {
    private final ResourceServerTokenServices resourceServerTokenServices;
    private final AuctionService auctionService;

    @Autowired
    public AuctionController(ResourceServerTokenServices resourceServerTokenServices, AuctionService auctionService) {
        this.resourceServerTokenServices = resourceServerTokenServices;
        this.auctionService = auctionService;
    }

    //Accessible only within microservices
    @PostMapping("/auction/autograph/{id}/owner/{ownerId}")
    public AuctionResponseDTO putAutographOnSale(@PathVariable("id") Long autographId, @PathVariable("ownerId") Long ownerId,
                                                 @RequestBody SaleDTO saleDTO,
                                                 @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return auctionService.createAuction(autographId, ownerId, saleDTO, userId);
    }

    @PutMapping("/auction/{id}")
    public ApiResponse editAuction(@PathVariable("id") Long id, @RequestBody SaleDTO saleDTO,
                                   @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return auctionService.updateAuction(id, saleDTO, userId);
    }

    @DeleteMapping("/auction/{auctionId}")
    public ApiResponse deleteAuction(@PathVariable("auctionId") Long id,
                                     @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return auctionService.deleteAuction(id, userId, authorization);
    }

    @GetMapping("/auction/user/{userId}/{page}")
    public ApiResponse findAuctionsByOwner(@PathVariable("userId") Long userId,
        @PathVariable("page") int page){
        return auctionService.findAuctionsByOwner(userId, page);
    }

    @GetMapping("/auction/status/{page}")
    public ApiResponse getAllByStatus(@RequestParam("status") Boolean status,
        @PathVariable("page") int page) throws Exception {
        return auctionService.getAllByStatus(status, page);
    }

    @GetMapping("/auction/{id}")
    public ApiResponse getAuctionById(@PathVariable("id") Long id) throws Exception {
        return auctionService.findAuctionById(id);
    }

    @GetMapping("/auction/autograph/{id}")
    public ApiResponse getAuctionByAutographId(@PathVariable("id") Long id) throws Exception {
        return auctionService.findAuctionByAutographId(id);
    }

    @GetMapping("/auction/ending/{page}")
    public ApiResponse getEndingSoon(@PathVariable("page") int page) throws Exception {
        return auctionService.getAllEndingSoon(page);
    }

    HashMap<String, Object> getUserDetails(String token){
        if(!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token){
        HashMap<String, Object> userDetails = getUserDetails(token);

        if(userDetails != null)
            return ((Integer) userDetails.get("id")).longValue();
        else return  null;
    }
}
