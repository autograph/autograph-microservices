package com.autograph.auctionservice.api.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "auctions")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
public class Auction {

    Long seller;
    @Id
    @GeneratedValue
    private Long id;
    private boolean active;
    private BigDecimal price;
    @CreatedDate
    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @OneToMany(mappedBy = "auction")
    private List<Bid> bids;

    private Long autograph;

    public Auction(boolean active, BigDecimal price, LocalDateTime createdDate,
        LocalDateTime startDate, LocalDateTime endDate, Long seller, Long autograph) {
        this.active = active;
        this.price = price;
        this.createdDate = createdDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.seller = seller;
        this.bids = new ArrayList<Bid>();
        this.autograph = autograph;
    }
}
