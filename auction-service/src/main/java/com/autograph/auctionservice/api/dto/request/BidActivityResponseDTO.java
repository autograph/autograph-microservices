package com.autograph.auctionservice.api.dto.request;

import com.autograph.auctionservice.api.dto.response.AutographResponseDTO;
import com.autograph.auctionservice.api.dto.response.UserHeaderResponseDTO;
import com.autograph.auctionservice.api.entity.Bid;
import com.autograph.auctionservice.api.enums.AutographType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BidActivityResponseDTO {

    private Long destinationUserId;

    private Long autographId;
    private String attachmentUploadDirectory;
    private AutographType autographType;
    private String title;

    private Long invokerId;
    private String username;
    private String profilePicId;

    private Long bidId;
    private BigDecimal price;

    public BidActivityResponseDTO(AutographResponseDTO autographResponseDTO,
        UserHeaderResponseDTO userHeaderResponseDTO,
        Bid bid) {
        this.destinationUserId = autographResponseDTO.getOwner();
        this.autographId = autographResponseDTO.getId();
        this.attachmentUploadDirectory = autographResponseDTO.getAttachmentUploadDirectory();
        this.autographType = autographResponseDTO.getAutographType();
        this.title = autographResponseDTO.getTitle();

        this.invokerId = userHeaderResponseDTO.getId();
        this.username = userHeaderResponseDTO.getUsername();
        this.profilePicId = userHeaderResponseDTO.getProfilePictureFileName();

        this.bidId = bid.getId();
        this.price = bid.getBidPrice();
    }
}
