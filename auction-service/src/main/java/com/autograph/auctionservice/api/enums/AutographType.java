package com.autograph.auctionservice.api.enums;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
