package com.autograph.auctionservice.api.service.impl;

import com.autograph.auctionservice.api.dto.request.SaleDTO;
import com.autograph.auctionservice.api.dto.response.AuctionResponseDTO;
import com.autograph.auctionservice.api.dto.response.AutographResponseDTO;
import com.autograph.auctionservice.api.entity.Auction;
import com.autograph.auctionservice.api.entity.Bid;
import com.autograph.auctionservice.api.enums.ResponseCodes;
import com.autograph.auctionservice.api.repository.IAuctionRepository;
import com.autograph.auctionservice.api.response.ApiResponse;
import com.autograph.auctionservice.api.service.IAuctionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuctionService implements IAuctionService {

    private final ModelMapper modelMapper;
    private final IAuctionRepository auctionRepository;
    private final ApiResponse apiResponse;
    private final WebClient.Builder webClientBuilder;
    private CircuitBreakerFactory<?, ?> cbFactory;


    @Autowired
    public AuctionService(ModelMapper modelMapper, IAuctionRepository auctionRepository,
        ApiResponse apiResponse, WebClient.Builder webClientBuilder) {
        this.modelMapper = modelMapper;
        this.auctionRepository = auctionRepository;
        this.apiResponse = apiResponse;
        this.webClientBuilder = webClientBuilder;
    }


    @Override
    public ApiResponse getAllAuctions(int page) {
        Pageable pageable = PageRequest.of(page, 10);
        List<AuctionResponseDTO> auctionList = auctionRepository.findAll(pageable)
            .stream()
            .map(auction -> modelMapper.map(auction, AuctionResponseDTO.class))
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(auctionList);

        return apiResponse;
    }

    @Override
    public ApiResponse findAuctionsByOwner(Long userId, int page) {
        Pageable pageable = PageRequest.of(page, 10);
        List<Auction> auctionList = auctionRepository.findAllBySeller(userId, pageable);
        List<AuctionResponseDTO> auctionResponseDTOS = auctionList.stream()
            .map(auction -> modelMapper.map(auction, AuctionResponseDTO.class))
            .collect(Collectors.toList());

        if (!auctionList.isEmpty()) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(auctionResponseDTOS);
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAllByStatus(Boolean status, int page) {
        Pageable pageable = PageRequest.of(page, 10);
        List<Auction> auctionList = auctionRepository.findAllByActive(status, pageable);
        List<AuctionResponseDTO> auctionResponseDTOS = auctionList.stream()
            .map(auction -> modelMapper.map(auction, AuctionResponseDTO.class))
            .collect(Collectors.toList());

        if (!auctionList.isEmpty()) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(auctionResponseDTOS);
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAllEndingSoon(int page) {

        //TODO

        apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse findAuctionById(Long id) {
        Optional<Auction> auctionOptional = auctionRepository.findById(id);

        if (auctionOptional.isPresent()) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(
                modelMapper.map(auctionOptional.get(), AuctionResponseDTO.class));
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse findAuctionByAutographId(Long autographId) {
        Optional<Auction> auctionOptional = auctionRepository.findByAutograph(autographId);

        if (auctionOptional.isPresent()) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(
                modelMapper.map(auctionOptional.get(), AuctionResponseDTO.class));
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAuctionDetails(Long id) {
        Optional<Auction> auctionOptional = auctionRepository.findById(id);

        if (auctionOptional.isPresent()) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(
                modelMapper.map(auctionOptional.get(), AuctionResponseDTO.class));
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public AuctionResponseDTO createAuction(Long autographId, Long autographOwnerId,
        SaleDTO saleDTO, Long userId) {
        if (autographOwnerId.equals(userId)) {

            Optional<Auction> auctionOptional = auctionRepository.findAuctionByAutograph(
                autographId);
            if (auctionOptional.isEmpty() ||
                (auctionOptional.get().getEndDate().isBefore(LocalDateTime.now())
                    && auctionOptional.get().getBids().isEmpty())) {
                auctionOptional.ifPresent(auctionRepository::delete);

                LocalDateTime createdAt = LocalDateTime.now();
                Auction auction = new Auction(true, saleDTO.getPrice(), createdAt,
                    saleDTO.getStartDate(), saleDTO.getEndDate(), userId, autographId);

                Auction savedAuction = auctionRepository.save(auction);

                return modelMapper.map(savedAuction, AuctionResponseDTO.class);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public ApiResponse updateAuction(Long autographId, SaleDTO saleDTO, Long userId) {
        if (userId != null) {

            Optional<Auction> auctionOptional = auctionRepository.findAuctionByAutograph(
                autographId);
            if (auctionOptional.isEmpty() ||
                (auctionOptional.get().getStartDate().isAfter(LocalDateTime.now())
                    && auctionOptional.get().getBids().isEmpty())) {
                auctionOptional.ifPresent(auctionRepository::delete);

                LocalDateTime createdAt = LocalDateTime.now();
                Auction auction = new Auction(true, saleDTO.getPrice(), createdAt,
                    saleDTO.getStartDate(), saleDTO.getEndDate(), userId, autographId);

                Auction savedAuction = auctionRepository.save(auction);
                apiResponse.setResponse(modelMapper.map(savedAuction, AuctionResponseDTO.class));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                return apiResponse;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public ApiResponse deleteAuction(Long id, Long userId, String authorization) {
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(id);
        if (auctionOptional.isPresent()) {
            ApiResponse autographResponse = cbFactory.create("get-autograph")
                .run(() -> getAutograph(auctionOptional.get().getAutograph(), authorization),
                    throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                        new AutographResponseDTO()));

            AutographResponseDTO autographResponseDTO =
                modelMapper.map(autographResponse.getResponse(), AutographResponseDTO.class);

            Long autographOwnerId = autographResponseDTO.getOwner();
            if (userId.equals(autographOwnerId)) {
                Auction auction = auctionOptional.get();

                auctionRepository.delete(auction);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(auction);
            }
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }
        return apiResponse;
    }

    @Override
    public boolean isBidHigher(Long auctionId, BigDecimal bidValue) {
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(auctionId);

        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();
            BigDecimal price = BigDecimal.ZERO;

            List<Bid> bids = auction.getBids();

            for (Bid bid : bids) {
                if (bid.getBidPrice().compareTo(price) > 0) {
                    price = bid.getBidPrice();
                }
            }

            return price.compareTo(bidValue) > 0;
        } else {
            return false;
        }
    }

    @Override
    public BigDecimal getLastPrice(Long auctionId) {
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(auctionId);

        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();
            BigDecimal price = BigDecimal.ZERO;

            List<Bid> bids = auction.getBids();

            for (Bid bid : bids) {
                if (bid.getBidPrice().compareTo(price) > 0) {
                    price = bid.getBidPrice();
                }
            }

            return price;
        } else {
            BigDecimal price = BigDecimal.ZERO;
        }
        return null;
    }

    @Override
    public List<Auction> getAll() {
        return auctionRepository.findAll();
    }

    ApiResponse getAutograph(Long autographId, String authorization) {
        return webClientBuilder.build()
            .get()
            .uri("http://autograph-service/api/autograph/" + autographId)
            .header("Authorization", authorization)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }
}
