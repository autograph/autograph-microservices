package com.autograph.auctionservice.api.response;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO {

    private String status;
    private int code;
    private String message;
}
