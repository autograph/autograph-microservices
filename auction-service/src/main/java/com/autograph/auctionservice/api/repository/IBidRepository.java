package com.autograph.auctionservice.api.repository;

import com.autograph.auctionservice.api.entity.Auction;
import com.autograph.auctionservice.api.entity.Bid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IBidRepository extends JpaRepository<Bid, Long> {

    List<Bid> findAllByAuction(Auction auction);

    Bid getBidById(Long id);
}
