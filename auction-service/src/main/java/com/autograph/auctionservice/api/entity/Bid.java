package com.autograph.auctionservice.api.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "bids")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter

public class Bid {

    @Id
    @GeneratedValue
    private Long id;

    @CreatedDate
    @Column(name = "created_date")
    private LocalDateTime createdDate;

    private BigDecimal bidPrice;

    @ManyToOne
    private Auction auction;

    private Long bidder;

    public Bid(LocalDateTime createdDate, BigDecimal bidPrice,
        Auction auction, Long bidder) {
        this.createdDate = createdDate;
        this.bidPrice = bidPrice;
        this.auction = auction;
        this.bidder = bidder;
    }
}
