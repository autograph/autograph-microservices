package com.autograph.auctionservice.api.dto.request;

import com.autograph.auctionservice.api.enums.PurchaseType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SaleDTO {

    private BigDecimal price;
    @NotNull
    private PurchaseType purchaseType;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}