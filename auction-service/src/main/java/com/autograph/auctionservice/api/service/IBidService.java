package com.autograph.auctionservice.api.service;

import com.autograph.auctionservice.api.dto.request.BidDTO;
import com.autograph.auctionservice.api.response.ApiResponse;

public interface IBidService {

    ApiResponse addBid(Long auctionId, BidDTO bidDTO, Long userId, String authorization)
        throws Exception;

    ApiResponse acceptBid(Long bidId, Long userId) throws Exception;

    ApiResponse getAuctionWinner(Long auctionId, Long userId) throws Exception;

    ApiResponse getAuctionBids(Long auctionId, int page) throws Exception;

    void markAuctionAsCompleted(Long auctionId) throws Exception;

    void markAuctionAsStarted(Long auctionId) throws Exception;
}
