package com.autograph.auctionservice.api.response;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

    private ResponseDTO code;
    private Object response;
}
