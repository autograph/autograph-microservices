package com.autograph.auctionservice.api.controller;

import com.autograph.auctionservice.api.dto.request.BidDTO;
import com.autograph.auctionservice.api.entity.Auction;
import com.autograph.auctionservice.api.enums.ResponseCodes;
import com.autograph.auctionservice.api.response.ApiResponse;
import com.autograph.auctionservice.api.service.impl.AuctionHandlerService;
import com.autograph.auctionservice.api.service.impl.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;


@Controller
@EnableScheduling
public class AuctionWebsocketController {
    private final SimpMessagingTemplate template;
    private final AuctionHandlerService auctionHandlerService;
    private final AuctionService auctionService;
    private final ResourceServerTokenServices resourceServerTokenServices;



    @Autowired
    public AuctionWebsocketController(
            SimpMessagingTemplate template,
            AuctionHandlerService auctionHandlerService,
            AuctionService auctionService, ResourceServerTokenServices resourceServerTokenServices) {
        this.template = template;
        this.auctionHandlerService = auctionHandlerService;
        this.auctionService = auctionService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }
    @MessageMapping("auction/{auctionId}/join")
    @SendTo("/auction/auction-updates/{auctionId}")
    @SuppressWarnings("unchecked")
    public ApiResponse joinAuction(@DestinationVariable("auctionId")Long auctionId) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(auctionId);
        return apiResponse;
    }

    @MessageMapping("/auction/{auctionId}/bid")
    @SendTo("/auction/auction-updates/{auctionId}")
    @SuppressWarnings("unchecked")
    public ApiResponse placeBidOnAuction(@DestinationVariable("auctionId")Long auctionId, BidDTO bidDTO,
                                         @RequestHeader("Authorization") String authorization){
        try{
            Long userId = getUserId(authorization);
            return auctionHandlerService.addBid(auctionId, bidDTO, userId, authorization);
        }catch (Exception e){
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            e.printStackTrace();

            return apiResponse;
        }
    }

    @MessageMapping("/auction/{auctionId}/details")
    @SendTo("/auction/auction-updates/{auctionId}")
    @SuppressWarnings("unchecked")
//    @GetMapping("/auctions/{id}/details")
    public ApiResponse getAuctionDetailsById(@DestinationVariable("auctionId")Long auctionId) throws Exception {
        try {
            return auctionService.getAuctionDetails(auctionId);
        } catch (Exception e) {
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setCode(ResponseCodes.AUTOGRAPH_NOT_FOUND.getValue());
            e.printStackTrace();

            return apiResponse;
        }
    }


    @Scheduled(fixedRate = 1000)
    public void sendCurrentTime() {
        try {
            ApiResponse apiResponse = new ApiResponse();
            LocalDateTime now = LocalDateTime.now();
            List<Auction> auctions = auctionService.getAll();

            for(Auction auction: auctions){
                if(auction.getStartDate().isAfter(now) && auction.getEndDate().isBefore(now)) {
                    long[] duration = ETABetweenDates(auction.getStartDate(), now);
                    apiResponse.setResponse(duration);
                    apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                    this.template
                            .convertAndSend("/auction/auction-updates/"+auction.getId(),
                            apiResponse);
                }
                else if(auction.getStartDate().isBefore(now)){
                    long[] duration = ETABetweenDates(now, auction.getStartDate());
                    apiResponse.setResponse(duration);
                    apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                    this.template
                        .convertAndSend("/auction/auction-updates/"+auction.getId(),
                            apiResponse);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 1000)
    public void checkAndSendBidStartNotification() {
        try {
            LocalDateTime now = LocalDateTime.now();
            List<Auction> auctions = auctionService.getAll();

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponse("Auction started!");
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

            for(Auction auction: auctions){
                if(auction.getStartDate() == now){
                    auctionHandlerService.markAuctionAsStarted(auction.getId());
                    this.template
                        .convertAndSend("/auction/auction-updates/"+auction.getId(),
                            apiResponse);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 1000)
    public void checkAndSendBidEndNotification() {
        try {
            LocalDateTime now = LocalDateTime.now();
            List<Auction> auctions = auctionService.getAll();

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponse("Auction ended!");
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

            for(Auction auction: auctions){
                if(auction.getEndDate()== now){
                    auctionHandlerService.markAuctionAsStarted(auction.getId());
                    this.template
                        .convertAndSend("/auction/auction-updates/"+auction.getId(),
                            apiResponse);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    long[] ETABetweenDates(LocalDateTime fromDateTime , LocalDateTime toDateTime ){
        long years, months, days, hours, minutes, seconds;

        LocalDateTime tempDateTime = LocalDateTime.from( fromDateTime );

        years = tempDateTime.until( toDateTime, ChronoUnit.YEARS );
        tempDateTime = tempDateTime.plusYears( years );

        months = tempDateTime.until( toDateTime, ChronoUnit.MONTHS );
        tempDateTime = tempDateTime.plusMonths( months );

        days = tempDateTime.until( toDateTime, ChronoUnit.DAYS );
        tempDateTime = tempDateTime.plusDays( days );


        hours = tempDateTime.until( toDateTime, ChronoUnit.HOURS );
        tempDateTime = tempDateTime.plusHours( hours );

        minutes = tempDateTime.until( toDateTime, ChronoUnit.MINUTES );
        tempDateTime = tempDateTime.plusMinutes( minutes );

        seconds = tempDateTime.until( toDateTime, ChronoUnit.SECONDS );

        return new long[]{years, months, days, hours, minutes, seconds};
    }

    HashMap<String, Object> getUserDetails(String token){
        if(!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token){
        HashMap<String, Object> userDetails = getUserDetails(token);

        if(userDetails != null)
            return ((Integer) userDetails.get("id")).longValue();
        else return  null;
    }
}
