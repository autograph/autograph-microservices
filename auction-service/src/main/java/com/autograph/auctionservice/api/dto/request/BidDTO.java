package com.autograph.auctionservice.api.dto.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BidDTO {

    private BigDecimal bidPrice;
    private Long auctionId;
}
