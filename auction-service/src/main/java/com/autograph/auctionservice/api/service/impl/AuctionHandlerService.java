package com.autograph.auctionservice.api.service.impl;

import com.autograph.auctionservice.api.dto.request.BidActivityResponseDTO;
import com.autograph.auctionservice.api.dto.request.BidDTO;
import com.autograph.auctionservice.api.dto.response.AutographResponseDTO;
import com.autograph.auctionservice.api.dto.response.UserHeaderResponseDTO;
import com.autograph.auctionservice.api.entity.Auction;
import com.autograph.auctionservice.api.entity.Bid;
import com.autograph.auctionservice.api.enums.ResponseCodes;
import com.autograph.auctionservice.api.repository.IAuctionRepository;
import com.autograph.auctionservice.api.repository.IBidRepository;
import com.autograph.auctionservice.api.response.ApiResponse;
import com.autograph.auctionservice.api.service.IBidService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class AuctionHandlerService implements IBidService {

    private final IAuctionRepository auctionRepository;
    private final IBidRepository bidRepository;
    private final WebClient.Builder webClientBuilder;
    private final ModelMapper modelMapper;
    private final CircuitBreakerFactory cbFactory;

    @Autowired
    public AuctionHandlerService(IAuctionRepository auctionRepository,
        IBidRepository bidRepository, CircuitBreakerFactory cbFactory,
        WebClient.Builder webClientBuilder, ModelMapper modelMapper) {
        this.auctionRepository = auctionRepository;
        this.bidRepository = bidRepository;
        this.cbFactory = cbFactory;
        this.webClientBuilder = webClientBuilder;
        this.modelMapper = modelMapper;
    }

    @Override
    public ApiResponse addBid(Long auctionId, BidDTO bidDTO, Long userId, String authorization) {
        ApiResponse apiResponse = new ApiResponse();
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(auctionId);

        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();
            Long autographId = auction.getAutograph();

            if (auction.getBids().isEmpty()) {
                auction.setBids(new ArrayList<>());
            }

            if (auction.getPrice().compareTo(bidDTO.getBidPrice()) > 0) {
                LocalDateTime localDateTime = LocalDateTime.now();
                Bid bid = new Bid(localDateTime, bidDTO.getBidPrice(), auction, userId);
                auction.setPrice(bidDTO.getBidPrice());
                bidRepository.save(bid);
                auctionRepository.save(auction);

                ApiResponse userHeaderResponse = cbFactory.create("get-user-header")
                    .run(() -> getUserById(userId, authorization),
                        throwable -> null);

                if (userHeaderResponse != null) {
                    ApiResponse autographResponse = cbFactory.create("get-autograph")
                        .run(() -> getAutograph(autographId, authorization),
                            throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(),
                                new AutographResponseDTO()));

                    AutographResponseDTO autographResponseDTO =
                        modelMapper.map(autographResponse.getResponse(),
                            AutographResponseDTO.class);

                    BidActivityResponseDTO bidActivityResponseDTO =
                        new BidActivityResponseDTO(autographResponseDTO,
                            modelMapper.map(userHeaderResponse.getResponse(),
                                UserHeaderResponseDTO.class), bid);

                    ApiResponse userActivity = cbFactory.create("post-bid-activity")
                        .run(() -> addToUsersActivity(bidActivityResponseDTO, authorization),
                            throwable -> null);

                }

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setCode(ResponseCodes.YOUR_BID_IS_LESS_THAN_PREVIOUS_BID.getValue());
            }
        } else {
            apiResponse.setCode(ResponseCodes.AUCTION_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse acceptBid(Long bidId, Long userId) {
        return null;
    }

    @Override
    public ApiResponse getAuctionWinner(Long auctionId, Long userId) {
        return null;
    }

    @Override
    public ApiResponse getAuctionBids(Long auctionId, int page) {
        return null;
    }

    @Override
    public void markAuctionAsCompleted(Long auctionId) {
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(auctionId);
        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();

            auction.setActive(false);
            auctionRepository.save(auction);
        }
    }

    @Override
    public void markAuctionAsStarted(Long auctionId) {
        Optional<Auction> auctionOptional = auctionRepository.findAuctionById(auctionId);
        if (auctionOptional.isPresent()) {
            Auction auction = auctionOptional.get();

            auction.setActive(true);
            auctionRepository.save(auction);
        }
    }

    ApiResponse addToUsersActivity(BidActivityResponseDTO bidActivityResponseDTO, String authorization) {
        return webClientBuilder.build()
            .post()
            .uri("http://activity-service/api/activity/broadcast/bid")
            .bodyValue(bidActivityResponseDTO)
                .header("Authorization", authorization)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

    ApiResponse getUserById(Long userId, String authentication) {
        return webClientBuilder.build()
            .get()
            .uri("http://user-service/api/user/header/" + userId)
            .header("Authentication", authentication)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

    ApiResponse getAutograph(Long autographId, String authorization) {
        return webClientBuilder.build()
            .get()
            .uri("http://autograph-service/api/autograph/" + autographId)
            .header("Authorization", authorization)
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }

}
