package com.autograph.auctionservice.api.repository;

import com.autograph.auctionservice.api.entity.Auction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IAuctionRepository extends JpaRepository<Auction, Long> {

    List<Auction> findAllBySeller(Long seller, Pageable pageable);

    List<Auction> findAllByActive(Boolean active, Pageable pageable);

    Optional<Auction> findByAutograph(Long autograph);

    Page<Auction> findAll(Pageable pageable);

    Optional<Auction> findAuctionById(Long id);

    Optional<Auction> findAuctionByAutograph(Long id);

    Optional<Auction> getAuctionById(Long id);
}
