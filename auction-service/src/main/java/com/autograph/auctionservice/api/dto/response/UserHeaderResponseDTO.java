package com.autograph.auctionservice.api.dto.response;

import lombok.Data;

@Data
public class UserHeaderResponseDTO {

    private Long id;
    private String email;
    private String username;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private String address;
}
