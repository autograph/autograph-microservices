#!/bin/bash

while true; do
    ./sequence.sh &
    RUN_PID=$!

    while true; do
        git fetch
        UPSTREAM=${1:-'@{u}'}
        LOCAL=$(git rev-parse @)
        REMOTE=$(git rev-parse "$UPSTREAM")
        BASE=$(git merge-base @ "$UPSTREAM")
        [[ $LOCAL = $REMOTE ]] || break
        sleep 15
    done

    if [ $LOCAL = $BASE ]; then
        echo "Need to pull"
        git pull
        kill $RUN_PID
        wait $RUN_PID
    fi
done
