#!/bin/bash

export PREFIX="ams"
tmux new-session -d -s "$PREFIX"
export LAST_PANE=0

tmux_deploy() {
    echo "Started service \"$1\" in a new tmux pane."
    tmux split-window -v
    tmux select-layout tiled
    tmux send-keys -t "${PREFIX}:0.${LAST_PANE}" "cd ${1}" Enter
    tmux send-keys -t "${PREFIX}:0.${LAST_PANE}" "../run.sh" Enter
    LAST_PANE=$((LAST_PANE + 1))
    sleep 10
}

tmux_deploy_ssl() {
    echo "Started service \"$1\" in a new tmux pane with ssl."
    tmux split-window -v
    tmux select-layout tiled
    tmux send-keys -t "${PREFIX}:0.${LAST_PANE}" "cd ${1}" Enter
    tmux send-keys -t "${PREFIX}:0.${LAST_PANE}" "../run.sh" Enter
    LAST_PANE=$((LAST_PANE + 1))
    sleep 10
}

tmux_deploy autograph-config-server
tmux_deploy discovery-service
tmux_deploy_ssl api-gateway
tmux_deploy activity-service
tmux_deploy feed-service
tmux_deploy auction-service
tmux_deploy promotion-service
tmux_deploy autograph-service
tmux_deploy user-service

tmux list-sessions
