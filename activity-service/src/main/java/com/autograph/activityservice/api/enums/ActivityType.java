package com.autograph.activityservice.api.enums;


import com.autograph.activityservice.api.dto.request.ActivityDTO;

public enum ActivityType {
    LIKE(new ActivityDTO("new like", 3001, "New like received")),
    BID(new ActivityDTO("bid received", 3002, "A new bid was received")),
    BID_ACCEPTED(new ActivityDTO("bid accepted", 3003, "Your bid was accepted")),
    BID_REJECTED(new ActivityDTO("bid rejected", 3004, "Your bid was rejected")),
    FOLLOW(new ActivityDTO("follow received", 3005, "You've got follow")),
    RELEASE_NOTES(new ActivityDTO("new release notes", 3006, "Release note received"));

    private final ActivityDTO value;

    ActivityType(ActivityDTO activityDTO) {
        this.value = activityDTO;
    }

    public ActivityDTO getValue() {
        return value;
    }
}
