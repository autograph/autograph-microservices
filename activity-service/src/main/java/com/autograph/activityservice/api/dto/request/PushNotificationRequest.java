package com.autograph.activityservice.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PushNotificationRequest {
    String token;
    String topic;
    String title;
    String message;
    String icon;
}
