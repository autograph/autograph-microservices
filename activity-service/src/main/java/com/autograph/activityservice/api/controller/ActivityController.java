package com.autograph.activityservice.api.controller;

import com.autograph.activityservice.api.dto.response.AutographResponseDTO;
import com.autograph.activityservice.api.dto.response.BidActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.FollowActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.ReleaseNoteResponseDTO;
import com.autograph.activityservice.api.response.ApiResponse;
import com.autograph.activityservice.api.service.activity.impl.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

@CrossOrigin
@RestController
@RequestMapping("/api/activity")
public class ActivityController {
    private final ActivityService activityService;
    private final ResourceServerTokenServices resourceServerTokenServices;

    @Autowired
    public ActivityController(ActivityService activityService, ResourceServerTokenServices resourceServerTokenServices) {
        this.activityService = activityService;
        this.resourceServerTokenServices = resourceServerTokenServices;
    }

    @PostMapping("/broadcast/bid")
    public ApiResponse addBidPlacedActivity(@RequestBody  BidActivityResponseDTO bidActivityResponseDTO,
                                            @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.addBidPlacedActivity(bidActivityResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/broadcast/like")
    public ApiResponse addLikeActivity(@RequestBody AutographResponseDTO likeActivityResponseDTO,
                                       @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.addLikeActivity(likeActivityResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/broadcast/like/remove")
    public ApiResponse removeLikeActivity(@RequestBody AutographResponseDTO likeActivityResponseDTO,
                                       @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.removeLikeActivity(likeActivityResponseDTO, authorization);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/broadcast/bid-accepted")
    public ApiResponse addBidAcceptedActivity(@RequestBody AutographResponseDTO activityResponseDTO,
                                              @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.addBidAcceptedActivity(activityResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @PostMapping("/broadcast/bid-rejected")
    public ApiResponse addBidRejectedActivity(@RequestBody AutographResponseDTO activityResponseDTO,
                                              @RequestHeader("Authorization") String authorization) {
        return activityService.addBidRejectedActivity(activityResponseDTO, authorization);
    }

    @PostMapping("/broadcast/follow")
    public ApiResponse addFollowActivity(@RequestBody FollowActivityResponseDTO followActivityResponseDTO,
                                         @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.addFollowActivity(followActivityResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/broadcast/follow/remove")
    public ApiResponse removeFollowActivity(@RequestBody FollowActivityResponseDTO followActivityResponseDTO,
                                         @RequestHeader("Authorization") String authorization) throws Exception {
        try {
            return activityService.removeFollowActivity(followActivityResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/broadcast/release-notes")
    public ApiResponse addReleaseNotesActivity(@RequestBody ReleaseNoteResponseDTO releaseNoteResponseDTO,
                                               @RequestHeader("Authorization") String authorization) {
        try {
            return activityService.addReleaseNotesActivity(releaseNoteResponseDTO, authorization);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/fetch/{page}")
    public ApiResponse fetchUserActivities(@RequestHeader("Authorization") String authorization,
                                           @PathVariable("page") int page) {
        Long userId = getUserId(authorization);
        return activityService.getUserActivity(userId, authorization, page);
    }

    @GetMapping("/{id}")
    public ApiResponse markActivityAsViewed(@PathVariable("id") Long id) {
        return activityService.markActivityAsViewed(id);
    }

    HashMap<String, Object> getUserDetails(String token){
        if(!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token){
        HashMap<String, Object> userDetails = getUserDetails(token);

        if(userDetails != null)
            return ((Integer) userDetails.get("id")).longValue();
        else return  null;
    }
}
