package com.autograph.activityservice.api.dto.response;

import com.autograph.activityservice.api.dto.request.ActivityDTO;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActivityResponse {

    private ActivityDTO code;
    private Object response;
}
