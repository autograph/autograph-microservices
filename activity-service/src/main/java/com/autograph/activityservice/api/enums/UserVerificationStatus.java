package com.autograph.activityservice.api.enums;

public enum UserVerificationStatus {
    VERIFIED,
    REJECTED,
    PENDING,
    REVISION_NEEDED,
    STALE
}
