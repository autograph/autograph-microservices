package com.autograph.activityservice.api.entity.activity;

import com.autograph.activityservice.api.dto.response.AutographResponseDTO;
import com.autograph.activityservice.api.dto.response.BidActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.FollowActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.ReleaseNoteResponseDTO;
import com.autograph.activityservice.api.enums.AutographType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ActivityMessage {

    @Id
    @GeneratedValue
    Long id;

    @NotNull
    Long destinationUserId;
    boolean isNew = true;
    private Long invokerId;
    private String username;
    private String profilePicId;
    private Long autographId;
    private String attachmentUploadDirectory;
    private AutographType autographType;
    private String title;
    private Long bidId;
    private BigDecimal price;
    private ActivityMessageType activityType;
    private String message;
    private String link = null;
    @CreatedDate
    @Column(name = "created_date")
    private LocalDateTime createdDate;

    public ActivityMessage(AutographResponseDTO autographResponseDTO,
        ActivityMessageType activityMessageType) {
        this.destinationUserId = autographResponseDTO.getDestinationUserId();
        this.invokerId = autographResponseDTO.getInvokerId();
        this.username = autographResponseDTO.getUsername();
        this.profilePicId = autographResponseDTO.getProfilePicId();
        this.autographId = autographResponseDTO.getAutographId();
        this.attachmentUploadDirectory = autographResponseDTO.getAttachmentUploadDirectory();
        this.autographType = autographResponseDTO.getAutographType();
        this.title = autographResponseDTO.getTitle();
        this.bidId = null;
        this.price = null;
        this.activityType = activityMessageType;
        this.message = null;
        this.isNew = true;
        this.createdDate = LocalDateTime.now();
    }

    public ActivityMessage(BidActivityResponseDTO bidActivityResponseDTO) {
        this.destinationUserId = bidActivityResponseDTO.getDestinationUserId();
        this.invokerId = bidActivityResponseDTO.getInvokerId();
        this.username = bidActivityResponseDTO.getUsername();
        this.profilePicId = bidActivityResponseDTO.getProfilePicId();
        this.autographId = bidActivityResponseDTO.getAutographId();
        this.attachmentUploadDirectory = bidActivityResponseDTO.getAttachmentUploadDirectory();
        this.autographType = bidActivityResponseDTO.getAutographType();
        this.title = bidActivityResponseDTO.getTitle();
        this.bidId = bidActivityResponseDTO.getBidId();
        this.price = bidActivityResponseDTO.getPrice();
        this.activityType = ActivityMessageType.BID;
        this.message = null;
        this.isNew = true;
        this.createdDate = LocalDateTime.now();
    }

    public ActivityMessage(FollowActivityResponseDTO followActivityResponseDTO) {
        this.destinationUserId = followActivityResponseDTO.getDestinationUserId();
        this.invokerId = followActivityResponseDTO.getInvokerId();
        this.username = followActivityResponseDTO.getUsername();
        this.profilePicId = followActivityResponseDTO.getProfilePicId();
        this.autographId = null;
        this.attachmentUploadDirectory = null;
        this.autographType = null;
        this.title = null;
        this.bidId = null;
        this.price = null;
        this.activityType = ActivityMessageType.FOLLOW;
        this.message = null;
        this.isNew = true;
        this.createdDate = LocalDateTime.now();
    }

    public ActivityMessage(ReleaseNoteResponseDTO releaseNoteResponseDTO) {
        this.destinationUserId = null;
        this.invokerId = null;
        this.username = null;
        this.profilePicId = null;
        this.autographId = null;
        this.attachmentUploadDirectory = null;
        this.autographType = null;
        this.title = null;
        this.bidId = null;
        this.price = null;
        this.activityType = null;
        this.message = releaseNoteResponseDTO.getMessage();
        this.link = releaseNoteResponseDTO.getLink();
        this.isNew = true;
        this.createdDate = LocalDateTime.now();
    }

}
