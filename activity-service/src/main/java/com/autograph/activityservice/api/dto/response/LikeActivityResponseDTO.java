package com.autograph.activityservice.api.dto.response;

import com.autograph.activityservice.api.enums.AutographType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LikeActivityResponseDTO {

    private Long destinationUserId;

    private Long autographId;
    private String attachmentUploadDirectory;
    private AutographType autographType;
    private String title;

    private Long invokerId;
    private String username;
    private String profilePicId;
}
