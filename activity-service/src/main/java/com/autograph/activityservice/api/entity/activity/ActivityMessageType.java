package com.autograph.activityservice.api.entity.activity;

public enum ActivityMessageType {
    LIKE,
    BID,
    BID_ACCEPTED,
    BID_REJECTED,
    FOLLOW,
    RELEASE_NOTES
}
