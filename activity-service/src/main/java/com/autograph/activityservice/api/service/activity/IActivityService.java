package com.autograph.activityservice.api.service.activity;

import com.autograph.activityservice.api.dto.response.AutographResponseDTO;
import com.autograph.activityservice.api.dto.response.BidActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.FollowActivityResponseDTO;
import com.autograph.activityservice.api.dto.response.ReleaseNoteResponseDTO;
import com.autograph.activityservice.api.response.ApiResponse;

public interface IActivityService {

    ApiResponse getUserActivity(Long userId, String authorization, int page) throws Exception;

    ApiResponse addLikeActivity(AutographResponseDTO likeActivityResponseDTO, String authorization) throws Exception;

    ApiResponse removeLikeActivity(AutographResponseDTO likeActivityResponseDTO, String authorization) throws Exception;

    void sendLikeActivityNotification(AutographResponseDTO likeActivityResponseDTO, String target) throws Exception;

    ApiResponse addFollowActivity(FollowActivityResponseDTO followActivityResponseDTO, String authorization)
        throws Exception;

    ApiResponse removeFollowActivity(FollowActivityResponseDTO followActivityResponseDTO, String authorization)
        throws Exception;

    void sendFollowActivityNotification(FollowActivityResponseDTO followActivityResponseDTO, String target) throws Exception;

    ApiResponse addReleaseNotesActivity(ReleaseNoteResponseDTO releaseNoteResponseDTO, String authorization)
        throws Exception;

    void sendReleaseNotesActivityNotification(ReleaseNoteResponseDTO releaseNoteResponseDTO) throws Exception;


    ApiResponse addBidPlacedActivity(BidActivityResponseDTO bidActivityResponseDTO, String authorization)
        throws Exception;

    void sendBidPlacedActivityNotification(BidActivityResponseDTO bidActivityResponseDTO, String target) throws Exception;

    ApiResponse addBidAcceptedActivity(AutographResponseDTO autographResponseDTO, String authorization) throws Exception;

    void sendBidAcceptedActivityNotification(AutographResponseDTO autographResponseDTO, String target) throws Exception;

    ApiResponse addBidRejectedActivity(AutographResponseDTO autographResponseDTO, String authorization) throws Exception;

    void sendBidRejectedNotification(AutographResponseDTO autographResponseDTO, String target) throws Exception;

    ApiResponse markActivityAsViewed(Long id) throws Exception;
}
