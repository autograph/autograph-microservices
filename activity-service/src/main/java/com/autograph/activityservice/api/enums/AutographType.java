package com.autograph.activityservice.api.enums;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
