package com.autograph.activityservice.api.dto.request;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDTO {

    private String status;
    private int code;
    private String message;
}
