package com.autograph.activityservice.api.dto.request;

public enum NotificationParameter {
    SOUND("default"),
    COLOR("#5A69C2"),
    ICON("http://www.mcicon.com/wp-content/uploads/2020/12/Education_Feather_1-copy-4.jpg");
    private String value;

    NotificationParameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}