package com.autograph.activityservice.api.service.activity.impl;

import com.autograph.activityservice.api.dto.request.NotificationParameter;
import com.autograph.activityservice.api.dto.request.PushNotificationRequest;
import com.autograph.activityservice.api.dto.request.UserHeaderResponseDTO;
import com.autograph.activityservice.api.dto.response.*;
import com.autograph.activityservice.api.entity.activity.ActivityMessage;
import com.autograph.activityservice.api.entity.activity.ActivityMessageType;
import com.autograph.activityservice.api.enums.ResponseCodes;
import com.autograph.activityservice.api.repository.activity.IActivityRepository;
import com.autograph.activityservice.api.response.ApiResponse;
import com.autograph.activityservice.api.service.activity.IActivityService;
import com.google.firebase.messaging.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;


@Service
public class ActivityService implements IActivityService {

    private final ApiResponse apiResponse;
    private final IActivityRepository activityRepository;
    private final int numberOfItemsOnPage = 10;
    private final CircuitBreakerFactory circuitBreakerFactory;
    private final WebClient.Builder webClientBuilder;
    private final ModelMapper modelMapper;

    @Autowired
    public ActivityService(ApiResponse apiResponse, IActivityRepository activityRepository,
                           CircuitBreakerFactory circuitBreakerFactory, WebClient.Builder webClientBuilder, ModelMapper modelMapper) {
        this.apiResponse = apiResponse;
        this.activityRepository = activityRepository;
        this.circuitBreakerFactory = circuitBreakerFactory;
        this.webClientBuilder = webClientBuilder;

        this.modelMapper = modelMapper;
    }

    @Override
    public ApiResponse getUserActivity(Long userId, String authorization, int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        List<ActivityMessage> activityMessageList = activityRepository
            .findAllByDestinationUserIdOrderByCreatedDateDesc(userId, pageable);

        List<ActivityMessage> activityMessageListUpd = activityMessageList.stream().map(activity ->
                    {
                        if(activity.getActivityType().equals(ActivityMessageType.FOLLOW)){
                            ApiResponse userResponse = webClientBuilder.build()
                                    .get()
                                    .uri("http://user-service/api/user/header/"+activity.getInvokerId())
                                    .header("Authorization",  authorization)
                                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                                    .acceptCharset(StandardCharsets.UTF_8)
                                    .ifNoneMatch("*")
                                    .ifModifiedSince(ZonedDateTime.now())
                                    .retrieve()
                                    .bodyToMono(ApiResponse.class)
                                    .block();

                            UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(userResponse.getResponse(), UserHeaderResponseDTO.class);

                            activity.setUsername(userHeaderResponseDTO.getUsername());
                            activity.setProfilePicId(userHeaderResponseDTO.getProfilePictureFileName());
                            return activity;
                        }else{
                            return activity;
                        }
                    }
                ).collect(Collectors.toList());
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageListUpd);
        return apiResponse;
    }

    @Override
    public ApiResponse addLikeActivity(AutographResponseDTO likeActivityResponseDTO, String authorization) throws ExecutionException, InterruptedException {
        ActivityMessage activityMessage = new ActivityMessage(likeActivityResponseDTO,
            ActivityMessageType.LIKE);

        NotificationSettingResponseDTO firebaseToken = getTargetUserFirebaseNotificationToken(likeActivityResponseDTO.getDestinationUserId(), authorization);

        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);

        if(firebaseToken != null && firebaseToken.getLikeNotification())
            sendLikeActivityNotification(likeActivityResponseDTO, firebaseToken.getToken());
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public ApiResponse removeLikeActivity(AutographResponseDTO likeActivityResponseDTO, String authorization) throws Exception {
        List<ActivityMessage> activitiesThatMatch = activityRepository
                .findAllByDestinationUserIdAndInvokerIdAndActivityTypeAndAutographId(likeActivityResponseDTO.getDestinationUserId(),
                        likeActivityResponseDTO.getInvokerId(), ActivityMessageType.LIKE, likeActivityResponseDTO.getAutographId());

        for (ActivityMessage message: activitiesThatMatch) {
            activityRepository.delete(message);
        }

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(true);
        return apiResponse;
    }

    @Override
    public void sendLikeActivityNotification(AutographResponseDTO likeActivityResponseDTO, String target) throws ExecutionException, InterruptedException {
        String title = "You have received a new like";
        String body = "A like received from " + likeActivityResponseDTO.getUsername();
        String icon = "http://a.sssemil.com:6969/user-service-files/" +
            likeActivityResponseDTO.getInvokerId() +
            "/profile-picture/" +
            likeActivityResponseDTO.getProfilePicId();

        PushNotificationRequest request = new PushNotificationRequest(target, "",title, body, icon);

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);

    }

    @Override
    public ApiResponse addFollowActivity(FollowActivityResponseDTO followActivityResponseDTO, String authorization) throws ExecutionException, InterruptedException {
        ActivityMessage activityMessage = new ActivityMessage(followActivityResponseDTO);
        NotificationSettingResponseDTO firebaseToken = getTargetUserFirebaseNotificationToken(followActivityResponseDTO.getDestinationUserId(), authorization);


        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);
        if(firebaseToken != null && firebaseToken.getFollowNotification())
            sendFollowActivityNotification(followActivityResponseDTO, firebaseToken.getToken());
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public ApiResponse removeFollowActivity(FollowActivityResponseDTO followActivityResponseDTO, String authorization) throws Exception {
        List<ActivityMessage> activitiesThatMatch = activityRepository
                .findAllByDestinationUserIdAndInvokerIdAndActivityTypeAndAutographId(followActivityResponseDTO.getDestinationUserId(),
                        followActivityResponseDTO.getInvokerId(), ActivityMessageType.FOLLOW, null);

        for (ActivityMessage message: activitiesThatMatch) {
            activityRepository.delete(message);
        }

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(true);
        return apiResponse;
    }

    @Override
    public void sendFollowActivityNotification(FollowActivityResponseDTO followActivityResponseDTO, String target) throws ExecutionException, InterruptedException {
        String title = "You have a new follower!";
        String body = followActivityResponseDTO.getUsername() + " has followed you!";
        String icon = "http://a.sssemil.com:6969/user-service-files/" +
                followActivityResponseDTO.getInvokerId() +
                "/profile-picture/" +
                followActivityResponseDTO.getProfilePicId();

        PushNotificationRequest request = new PushNotificationRequest(target, "",title, body, icon);

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);
    }

    @Override
    public ApiResponse addReleaseNotesActivity(ReleaseNoteResponseDTO releaseNoteResponseDTO, String authorization) throws ExecutionException, InterruptedException {
        ActivityMessage activityMessage = new ActivityMessage(releaseNoteResponseDTO);


        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);
        sendReleaseNotesActivityNotification(releaseNoteResponseDTO);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public void sendReleaseNotesActivityNotification(ReleaseNoteResponseDTO releaseNoteResponseDTO) throws ExecutionException, InterruptedException {
        String title = "Release Notes";
        String body = releaseNoteResponseDTO.getMessage();

        PushNotificationRequest request = new PushNotificationRequest("", "",title, body, "");

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);
    }

    @Override
    public ApiResponse addBidPlacedActivity(BidActivityResponseDTO bidActivityResponseDTO, String authorization) throws ExecutionException, InterruptedException {
        ActivityMessage activityMessage = new ActivityMessage(bidActivityResponseDTO);
        NotificationSettingResponseDTO firebaseToken = getTargetUserFirebaseNotificationToken(bidActivityResponseDTO.getDestinationUserId(), authorization);

        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);
        if(firebaseToken != null && firebaseToken.getBidNotification())
            sendBidPlacedActivityNotification(bidActivityResponseDTO, firebaseToken.getToken());
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public void sendBidPlacedActivityNotification(BidActivityResponseDTO bidActivityResponseDTO, String target) throws ExecutionException, InterruptedException {
        String title = "You have received a new bid";
        String body = "A bid received from " + bidActivityResponseDTO.getUsername();
        String icon = "http://a.sssemil.com:6969/user-service-files/" +
            bidActivityResponseDTO.getInvokerId() +
            "/profile-picture/" +
            bidActivityResponseDTO.getProfilePicId();

        PushNotificationRequest request = new PushNotificationRequest(target, "",title, body, icon);

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);
    }

    @Override
    public ApiResponse addBidAcceptedActivity(AutographResponseDTO autographResponseDTO, String authorization) throws ExecutionException, InterruptedException {
        ActivityMessage activityMessage = new ActivityMessage(autographResponseDTO,
            ActivityMessageType.BID_ACCEPTED);

        NotificationSettingResponseDTO firebaseToken = getTargetUserFirebaseNotificationToken(autographResponseDTO.getDestinationUserId(), authorization);


        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);
        if(firebaseToken != null && firebaseToken.getBidAcceptedNotification())
            sendBidAcceptedActivityNotification(autographResponseDTO, firebaseToken.getToken());
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public void sendBidAcceptedActivityNotification(AutographResponseDTO autographResponseDTO, String target) throws ExecutionException, InterruptedException {
        String title = "Your bid was accepted!";
        String body = "A bid on " + autographResponseDTO.getTitle() + " was accepted";
        String icon = "http://a.sssemil.com:6969/autograph-service-files/" +
                autographResponseDTO.getDestinationUserId() +
                "/attachments/" +
                autographResponseDTO.getAttachmentUploadDirectory();

        PushNotificationRequest request = new PushNotificationRequest(target, "",title, body, icon);

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);
    }

    @SneakyThrows
    @Override
    public ApiResponse addBidRejectedActivity(AutographResponseDTO autographResponseDTO, String authorization) {
        ActivityMessage activityMessage = new ActivityMessage(autographResponseDTO,
            ActivityMessageType.BID_REJECTED);

        NotificationSettingResponseDTO firebaseToken = getTargetUserFirebaseNotificationToken(autographResponseDTO.getDestinationUserId(), authorization);


        ActivityMessage activityMessageSaved = activityRepository.save(activityMessage);
        if(firebaseToken != null && firebaseToken.getBidRejectedNotification())
            sendBidRejectedNotification(autographResponseDTO, firebaseToken.getToken());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessageSaved);
        return apiResponse;
    }

    @Override
    public void sendBidRejectedNotification(AutographResponseDTO autographResponseDTO, String target) throws ExecutionException, InterruptedException {
        String title = "Your bid was rejected";
        String body = "A bid on " + autographResponseDTO.getTitle() + " was rejected";
        String icon = "http://a.sssemil.com:6969/autograph-service-files/" +
                autographResponseDTO.getDestinationUserId() +
                "/attachments/" +
                autographResponseDTO.getAttachmentUploadDirectory();

        PushNotificationRequest request = new PushNotificationRequest(target, "",title, body, icon);

        Message message = getPreconfiguredMessageToToken(request);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(message);
        String response = sendAndGetResponse(message);
        System.out.println("Sent message to token. Device token: " + request.getToken() + ", " + response+ " msg "+jsonOutput);
    }

    @Override
    public ApiResponse markActivityAsViewed(Long id) {
        ActivityMessage activityMessage = activityRepository.getById(id);
        activityMessage.setNew(false);

        activityRepository.save(activityMessage);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(activityMessage);
        return apiResponse;
    }
    
    private NotificationSettingResponseDTO getTargetUserFirebaseNotificationToken(Long id, String authorization){
        NotificationSettingResponseDTO token;

        ApiResponse tokenResponse = webClientBuilder.build()
                .get()
                .uri("http://user-service/api/user/firebase-token/"+id)
                .header("Authorization",  authorization)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();
        
        token =  modelMapper.map(tokenResponse.getResponse(), NotificationSettingResponseDTO.class) ;
        
        return token;
    }

    private String sendAndGetResponse(Message message) throws InterruptedException, ExecutionException {
        return FirebaseMessaging.getInstance().sendAsync(message).get();
    }

    private AndroidConfig getAndroidConfig(String topic) {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(2).toMillis()).setCollapseKey(topic)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder().setSound(NotificationParameter.SOUND.getValue())
                        .setColor(NotificationParameter.COLOR.getValue()).setTag(topic).setIcon(NotificationParameter.ICON.getValue()).build()).build();
    }

    private ApnsConfig getApnsConfig(String topic) {
        return ApnsConfig.builder()
                .setAps(Aps.builder().setCategory(topic).setThreadId(topic).build()).build();
    }

    private Message getPreconfiguredMessageToToken(PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).setToken(request.getToken())
                .build();
    }

    private Message getPreconfiguredMessageWithoutData(PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).setTopic(request.getTopic())
                .build();
    }

    private Message getPreconfiguredMessageWithData(Map<String, String> data, PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).putAllData(data).setToken(request.getToken())
                .build();
    }

    private Message.Builder getPreconfiguredMessageBuilder(PushNotificationRequest request) {
        AndroidConfig androidConfig = getAndroidConfig(request.getTopic());
        ApnsConfig apnsConfig = getApnsConfig(request.getTopic());
        return Message.builder()
                .setApnsConfig(apnsConfig).setAndroidConfig(androidConfig).setNotification(
                        Notification.builder().setTitle(request.getTitle()).setBody(request.getMessage()).
                                setImage(request.getIcon()).build());
    }

}
