package com.autograph.activityservice.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FollowActivityResponseDTO {

    private Long destinationUserId;

    private Long invokerId;
    private String username;
    private String profilePicId;
}
