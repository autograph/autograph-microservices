package com.autograph.activityservice.api.dto.request;

import com.autograph.activityservice.api.enums.UserVerificationStatus;
import lombok.Data;

@Data
public class UserHeaderResponseDTO {
    private Long id;
    private String email;
    private String username;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private String address;
    private String name, surname, bio;
    private UserVerificationStatus userVerificationStatus;
    private Boolean isBlocked;
    private Long following, followed;
}
