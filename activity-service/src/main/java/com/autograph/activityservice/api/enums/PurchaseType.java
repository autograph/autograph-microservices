package com.autograph.activityservice.api.enums;

public enum PurchaseType {
    PURCHASE_NOW,
    AUCTION,
    ASYNC_AUCTION,
    NOT_FOR_SALE
}
