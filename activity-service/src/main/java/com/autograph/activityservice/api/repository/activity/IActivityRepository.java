package com.autograph.activityservice.api.repository.activity;

import com.autograph.activityservice.api.entity.activity.ActivityMessage;
import com.autograph.activityservice.api.entity.activity.ActivityMessageType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IActivityRepository extends JpaRepository<ActivityMessage, Long> {

    Optional<ActivityMessage> findById(Long id);

    List<ActivityMessage> findAllByDestinationUserIdOrderByCreatedDateAsc(Long id,
        Pageable pageable);

    List<ActivityMessage> findAllByDestinationUserIdOrderByCreatedDateDesc(Long id,
                                                                          Pageable pageable);

    List<ActivityMessage> findAllByDestinationUserIdAndInvokerIdAndActivityTypeAndAutographId(Long destinationUserId,
                                                                                                   Long invokerId, ActivityMessageType activityType, Long autographId);
}
