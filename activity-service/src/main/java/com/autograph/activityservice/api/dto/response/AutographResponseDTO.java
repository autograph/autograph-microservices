package com.autograph.activityservice.api.dto.response;

import com.autograph.activityservice.api.enums.AutographType;
import lombok.Data;

@Data
public class AutographResponseDTO {

    private Long destinationUserId;

    private Long autographId;
    private String attachmentUploadDirectory;
    private AutographType autographType;
    private String title;

    private Long invokerId;
    private String username;
    private String profilePicId;

}
