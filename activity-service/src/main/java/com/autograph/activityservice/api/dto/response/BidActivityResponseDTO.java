package com.autograph.activityservice.api.dto.response;

import com.autograph.activityservice.api.enums.AutographType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BidActivityResponseDTO {

    private Long destinationUserId;

    private Long autographId;
    private String attachmentUploadDirectory;
    private AutographType autographType;
    private String title;

    private Long invokerId;
    private String username;
    private String profilePicId;

    private Long bidId;
    private BigDecimal price;
}
