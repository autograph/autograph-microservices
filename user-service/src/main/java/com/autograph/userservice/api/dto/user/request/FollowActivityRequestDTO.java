package com.autograph.userservice.api.dto.user.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FollowActivityRequestDTO {
    private Long destinationUserId;
    private Long invokerId;
    private String username;
    private String profilePicId;

    public FollowActivityRequestDTO(Long destinationUserId, Long invokerId, String username, String profilePicId) {
        this.destinationUserId = destinationUserId;
        this.invokerId = invokerId;
        this.username = username;
        this.profilePicId = profilePicId;
    }
}
