package com.autograph.userservice.api.service.follow;

import com.autograph.userservice.api.response.ApiResponse;

public interface IFollowService {

    ApiResponse getUserFollowers(String address, int page) throws Exception;

    ApiResponse getUserFollowingUsers(String address, int page) throws Exception;

    ApiResponse getFollowers(int page) throws Exception;

    ApiResponse getFollowings(int page) throws Exception;

    ApiResponse followUser(String address, String authorization) throws Exception;

    ApiResponse isFollowedByUser(String address) throws Exception;

    ApiResponse isFollowingUser(String address) throws Exception;

    ApiResponse muteUserChange(String address) throws Exception;

    ApiResponse notifyUserAutographsChange(String address) throws Exception;

    ApiResponse getFollowInfo(String address) throws Exception;

    ApiResponse getUserFollowers(Long id);

}
