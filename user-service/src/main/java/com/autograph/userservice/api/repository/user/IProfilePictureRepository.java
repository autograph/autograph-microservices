package com.autograph.userservice.api.repository.user;

import com.autograph.userservice.api.entity.user.ProfilePicture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IProfilePictureRepository extends JpaRepository<ProfilePicture, Long> {

    Optional<ProfilePicture> findById(Long id);
}
