package com.autograph.userservice.api.entity.verification;

import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VerificationDetails {

    @OneToOne
    User user;
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String name, surname, description;
    private String websiteURL;
    private UserVerificationStatus userVerificationStatus;
    private String resultDescription;
}
