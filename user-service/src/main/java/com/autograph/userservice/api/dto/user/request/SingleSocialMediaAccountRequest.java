package com.autograph.userservice.api.dto.user.request;

import com.autograph.userservice.api.entity.user.SocialMediaAccountType;
import lombok.Getter;

@Getter
public class SingleSocialMediaAccountRequest {

    private SocialMediaAccountType socialMediaAccountType;

    private String accountDetails;
}
