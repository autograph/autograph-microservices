package com.autograph.userservice.api.entity.follow;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserFollowKey implements Serializable {

    @Column(name = "user_id")
    Long userId;

    @Column(name = "following_id")
    Long followingId;
}
