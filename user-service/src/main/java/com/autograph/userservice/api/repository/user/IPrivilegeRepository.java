package com.autograph.userservice.api.repository.user;

import com.autograph.userservice.api.entity.user.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IPrivilegeRepository extends JpaRepository<Privilege, Long> {

    Optional<Privilege> findById(Long id);

    Privilege findByName(String name);
}
