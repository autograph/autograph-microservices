package com.autograph.userservice.api.enums;

public enum SearchType {
    USER,
    AUTOGRAPH
}
