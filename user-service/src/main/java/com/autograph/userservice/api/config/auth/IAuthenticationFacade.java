package com.autograph.userservice.api.config.auth;

import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {

    Authentication getAuthenticationFacade();
}
