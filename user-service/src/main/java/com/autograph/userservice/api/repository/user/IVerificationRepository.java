package com.autograph.userservice.api.repository.user;

import com.autograph.userservice.api.entity.verification.VerificationDetails;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IVerificationRepository extends JpaRepository<VerificationDetails, Long> {

    List<VerificationDetails> findAllByUserVerificationStatus(
        UserVerificationStatus verificationStatus,
        Pageable pageable);
}
