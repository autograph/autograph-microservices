package com.autograph.userservice.api.dto.user.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationSettingResponseDTO {
    private Boolean likeNotification, bidNotification, bidAcceptedNotification, bidRejectedNotification, followNotification;
    private String token;
}
