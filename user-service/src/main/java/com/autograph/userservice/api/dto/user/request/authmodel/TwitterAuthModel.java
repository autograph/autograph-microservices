package com.autograph.userservice.api.dto.user.request.authmodel;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class TwitterAuthModel {
    private String authToken;
    private String authTokenSecret;
}
