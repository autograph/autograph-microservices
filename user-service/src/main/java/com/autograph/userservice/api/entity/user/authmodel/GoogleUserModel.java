package com.autograph.userservice.api.entity.user.authmodel;

import com.autograph.userservice.api.dto.user.request.UserRequest;
import com.autograph.userservice.api.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.javafaker.Faker;
import lombok.*;

import java.util.Locale;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GoogleUserModel {
    private String email;
    private Boolean emailVerified;
    private String name;
    private String pictureUrl;
    private String givenName;
    private String familyName;
    private String locale;
    private String userId;

    public UserRequest createUserRequest(){
        Faker faker = new Faker();
        String username = faker.superhero().prefix() + givenName + faker.address().buildingNumber();
        UserRequest userRequest = new UserRequest(email, givenName, familyName, username.toLowerCase(Locale.ROOT), Utils.generatePassayPassword(),
                "", "");

        System.out.println(email + " " + givenName + " " + familyName + " " + username.toLowerCase(Locale.ROOT) + " " + userRequest.getPassword());

        return userRequest;
    }
}
