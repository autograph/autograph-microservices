package com.autograph.userservice.api.dto.user.request;

import lombok.Getter;

import java.util.List;

@Getter
public class AddSocialMediaAccountsRequest {

    List<SingleSocialMediaAccountRequest> socialMediaAccounts;
}
