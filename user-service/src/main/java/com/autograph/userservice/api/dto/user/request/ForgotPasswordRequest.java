package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class ForgotPasswordRequest {
    String email;
}
