package com.autograph.userservice.api.dto.user.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ProfilePictureRequestDTO {

    private MultipartFile file;
}
