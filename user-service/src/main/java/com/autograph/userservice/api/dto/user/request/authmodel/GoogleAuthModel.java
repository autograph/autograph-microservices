package com.autograph.userservice.api.dto.user.request.authmodel;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class GoogleAuthModel {
    private String idToken;
}
