package com.autograph.userservice.api.repository.follow;

import com.autograph.userservice.api.entity.follow.UserFollow;
import com.autograph.userservice.api.entity.follow.UserFollowKey;
import com.autograph.userservice.api.entity.user.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IUserFollowRepository extends JpaRepository<UserFollow, UserFollowKey> {

    Optional<UserFollow> findById(UserFollowKey userFollowKey);

    Optional<UserFollow> findByUserAndAndFollowingUser(User user, User followingUser);

    List<UserFollow> findAllByUser(User user, Pageable pageable);

    List<UserFollow> findAllByUser(User user);

    List<UserFollow> findAllByFollowingUser(User user, Pageable pageable);

    List<UserFollow> findAllByFollowingUser(User user);
}
