package com.autograph.userservice.api.service.follow.impl;

import com.autograph.userservice.api.config.auth.IAuthenticationFacade;
import com.autograph.userservice.api.dto.user.request.FollowActivityRequestDTO;
import com.autograph.userservice.api.dto.user.response.UserFollowInfoResponse;
import com.autograph.userservice.api.dto.user.response.UserResponse;
import com.autograph.userservice.api.entity.follow.UserFollow;
import com.autograph.userservice.api.entity.follow.UserFollowKey;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.repository.follow.IUserFollowRepository;
import com.autograph.userservice.api.repository.user.IUserRepository;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.follow.IFollowService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FollowService implements IFollowService {
    private final ModelMapper modelMapper;
    private final IUserRepository userRepository;
    private final IAuthenticationFacade authenticationFacade;
    private final IUserFollowRepository followRepository;
    private final ApiResponse apiResponse;
    private final int numberOfItemsOnPage = 10;
    private final WebClient.Builder webClientBuilder;
    private final CircuitBreakerFactory<?,?> cbFactory;
    Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    public FollowService(ModelMapper modelMapper,
                         IUserRepository userRepository, IAuthenticationFacade authenticationFacade,
                         IUserFollowRepository followRepository, ApiResponse apiResponse,
                         WebClient.Builder webClientBuilder, CircuitBreakerFactory<?,?> cbFactory) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.authenticationFacade = authenticationFacade;
        this.followRepository = followRepository;
        this.apiResponse = apiResponse;
        this.webClientBuilder = webClientBuilder;
        this.cbFactory = cbFactory;
    }

    @Override
    public ApiResponse getUserFollowers(String addressRequest, int page)  {
        logger.trace("Getting user followers: {} on page {}", addressRequest, page);
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        User user = userRepository.findUserByAddress(addressRequest);

        if (user != null) {
            List<UserFollow> userFollows = followRepository.findAllByFollowingUser(user, pageable);
            List<UserResponse> userFollowResponseDTOS = userFollows
                .stream()
                .map(UserFollow::getFollowingUser)
                .map(followingUser -> modelMapper.map(followingUser, UserResponse.class))
                .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollowResponseDTOS);
        } else {
            logger.error("User with address {} not found", addressRequest);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);

        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUserFollowingUsers(String addressRequest, int page)  {

        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        User user = userRepository.findUserByAddress(addressRequest);

        if (user != null) {
            List<UserFollow> userFollowings = followRepository.findAllByUser(user, pageable);
            List<UserResponse> userFollowResponseDTOS = userFollowings
                .stream()
                .map(UserFollow::getFollowingUser)
                .map(followingUser -> modelMapper.map(followingUser, UserResponse.class))
                .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollowResponseDTOS);
        } else {
            logger.error("User with address {} not found", addressRequest);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getFollowers(int page)  {

        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            List<UserFollow> userFollows = followRepository.findAllByFollowingUser(user, pageable);
            List<UserResponse> userFollowResponseDTOS = userFollows
                .stream()
                .map(UserFollow::getFollowingUser)
                .map(followingUser -> modelMapper.map(followingUser, UserResponse.class))
                .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollowResponseDTOS);
        } else {
            logger.error("User {} not found", auth);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getFollowings(int page)  {

        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);

        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            List<UserFollow> userFollowings = followRepository.findAllByUser(user, pageable);
            List<UserResponse> userFollowResponseDTOS = userFollowings
                .stream()
                .map(UserFollow::getFollowingUser)
                .map(followingUser -> modelMapper.map(followingUser, UserResponse.class))
                .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollowResponseDTOS);
        } else {
            logger.error("User {} not found", auth);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse followUser(String addressRequest, String authorization)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        User followedUser = userRepository.findUserByAddress(addressRequest);
        System.out.println(followedUser.getUsername());

        if (user != null && followedUser != null && user != followedUser) {
            Optional<UserFollow> follow = followRepository.findByUserAndAndFollowingUser(user,
                followedUser);

            System.out.println(follow);

            if (follow.isPresent()) {
                System.out.println("Follow is present");

                System.out.println(follow.get().getId());

                Long followingNumber = user.getFollowed() - 1;
                Long followedNumber = followedUser.getFollowing() - 1;

                user.setFollowing(followingNumber);
                followedUser.setFollowed(followedNumber);

                userRepository.save(user);
                userRepository.save(followedUser);

                FollowActivityRequestDTO followActivityRequestDTO =
                        new FollowActivityRequestDTO(followedUser.getId(), user.getId(),
                                user.getUsername(), "");

                ApiResponse response = cbFactory.create("broadcast-remove-follow-activity").run(() ->
                                broadcastRemoveFollowActivity(followActivityRequestDTO, authorization),
                        throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(), "Broadcast failed"));

                followRepository.delete(follow.get());
                apiResponse.setResponse(false);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {

                LocalDateTime followedAt = LocalDateTime.now();
                UserFollowKey userFollowKey = new UserFollowKey(user.getId(), followedUser.getId());

                UserFollow userFollow = new UserFollow(userFollowKey, user, followedUser,
                    followedAt,
                    false, false);

                Long followingNumber = user.getFollowed() + 1;
                Long followedNumber = followedUser.getFollowing() + 1;

                user.setFollowing(followingNumber);
                followedUser.setFollowed(followedNumber);


                String userProfilePicture = "";

                if(user.getProfilePicture() != null){
                    userProfilePicture = user.getProfilePicture().getFileName();
                }

                FollowActivityRequestDTO followActivityRequestDTO =
                        new FollowActivityRequestDTO(followedUser.getId(), user.getId(),
                                user.getUsername(), userProfilePicture);


                ApiResponse response = cbFactory.create("broadcast-follow-activity").run(() ->
                        broadcastFollowActivity(followActivityRequestDTO, authorization),
                        throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(), "Broadcast failed"));

                userRepository.save(user);
                userRepository.save(followedUser);

                followRepository.save(userFollow);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(true);
            }

        } else {
            logger.error("User with address {} not found", addressRequest);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse isFollowedByUser(String addressRequest)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());
        User targetUser = userRepository.findUserByAddress(addressRequest);

        if (user != null && targetUser != null) {
            Optional<UserFollow> userFollow = followRepository.findByUserAndAndFollowingUser(user,
                targetUser);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollow.isPresent());
        } else {
            logger.error("User with address {} not found", addressRequest);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse isFollowingUser(String addressRequest)  {


        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());
        User targetUser = userRepository.findUserByAddress(addressRequest);

        if (user != null && targetUser != null) {
            Optional<UserFollow> userFollow = followRepository.findByUserAndAndFollowingUser(
                targetUser, user);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollow.isPresent());
        } else {
            logger.error("User with address {} not found", addressRequest);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse muteUserChange(String address)  {

        Authentication auth = authenticationFacade.getAuthenticationFacade();

        User targetUser = userRepository.findUserByAddress(address);
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null && targetUser != null) {
            Optional<UserFollow> userFollowOptional = followRepository.findByUserAndAndFollowingUser(
                user, targetUser);

            if (userFollowOptional.isPresent()) {
                UserFollow userFollow = userFollowOptional.get();
                Boolean muteResult = !userFollow.getMuted();
                userFollow.setMuted(muteResult);

                followRepository.save(userFollow);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(muteResult);
            } else {
                apiResponse.setCode(ResponseCodes.FOLLOW_NOT_FOUND.getValue());
            }
        } else {
            logger.error("User with email {} not found", address);

            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse notifyUserAutographsChange(String address)  {

        Authentication auth = authenticationFacade.getAuthenticationFacade();

        User targetUser = userRepository.findUserByAddress(address);
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null && targetUser != null) {
            Optional<UserFollow> userFollowOptional = followRepository.findByUserAndAndFollowingUser(
                user, targetUser);

            if (userFollowOptional.isPresent()) {
                UserFollow userFollow = userFollowOptional.get();
                Boolean notificationResult = !userFollow.getMuted();
                userFollow.setMuted(notificationResult);

                followRepository.save(userFollow);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(notificationResult);
            } else {
                apiResponse.setCode(ResponseCodes.FOLLOW_NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            }
        } else {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getFollowInfo(String address)  {

        Authentication auth = authenticationFacade.getAuthenticationFacade();

        User targetUser = userRepository.findUserByAddress(address);
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null && targetUser != null) {
            Optional<UserFollow> userFollowOptional = followRepository.findByUserAndAndFollowingUser(
                user, targetUser);

            if (userFollowOptional.isPresent()) {
                UserFollowInfoResponse userFollowInfoResponse =
                        modelMapper.map(userFollowOptional.get(), UserFollowInfoResponse.class);

                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(userFollowInfoResponse);
            } else {
                apiResponse.setCode(ResponseCodes.FOLLOW_NOT_FOUND.getValue());
                apiResponse.setResponse(null);
            }
        } else {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUserFollowers(Long id)  {
        User user = userRepository.findUserById(id);

        if (user != null) {
            List<UserFollow> userFollows = followRepository.findAllByFollowingUser(user);
            List<Long> userFollowResponseDTOS = userFollows
                    .stream()
                    .map(UserFollow::getUser)
                    .map(User::getId)
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userFollowResponseDTOS);
        } else {
            logger.error("User with id {} not found", id);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);

        }

        return apiResponse;
    }

    ApiResponse broadcastFollowActivity(FollowActivityRequestDTO followActivityRequestDTO, String authorization){
        return webClientBuilder.build()
                .post()
                .uri("http://activity-service/api/activity/broadcast/follow")
                .bodyValue(followActivityRequestDTO)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .header("Authorization", authorization)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();
    }

    ApiResponse broadcastRemoveFollowActivity(FollowActivityRequestDTO followActivityRequestDTO, String authorization){
        return webClientBuilder.build()
                .post()
                .uri("http://activity-service/api/activity/broadcast/follow/remove")
                .bodyValue(followActivityRequestDTO)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .header("Authorization", authorization)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();
    }
}
