package com.autograph.userservice.api.dto.user.response;

import lombok.*;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserFollowInfoResponse {

    LocalDateTime startedFollowAt;
    Boolean muted;
    Boolean notification;
}
