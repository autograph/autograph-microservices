package com.autograph.userservice.api.entity.user.authmodel;

import com.autograph.userservice.api.dto.user.request.UserRequest;
import com.autograph.userservice.api.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.javafaker.Faker;
import lombok.*;

import java.util.ArrayList;
import java.util.Locale;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TwitterUserModel {
    @JsonIgnore
    public String status;
    private long id;
    private String id_str;
    private String name;
    private String screen_name;
    private String location;
    private String description;
    private Object url;
    private int followers_count;
    private int friends_count;
    private int listed_count;
    private String created_at;
    private Object utc_offset;
    private Object time_zone;
    private boolean verified;
    private Object lang;
    private String profile_background_color;
    private Object profile_background_image_url;
    private Object profile_background_image_url_https;
    private boolean profile_background_tile;
    private String profile_image_url;
    private String profile_image_url_https;
    private String profile_banner_url;
    private String profile_link_color;
    private String profile_sidebar_border_color;
    private String profile_sidebar_fill_color;
    private String profile_text_color;
    private boolean profile_use_background_image;
    private boolean has_extended_profile;
    private boolean default_profile;
    private boolean default_profile_image;
    private String translator_type;
    private ArrayList<Object> withheld_in_countries;
    private boolean suspended;
    private String email;

    public UserRequest createUserRequest(){
        Faker faker = new Faker();
        String firstName, lastName;
        String[] names = name.split(" ");
        if(names.length > 0){
            if(names.length == 1){
                firstName = names[0];
                lastName = "";
            }else{
                firstName = names[0];
                lastName = names[names.length - 1];
            }
        }else{
            firstName = faker.name().firstName();
            lastName = faker.name().lastName();
        }
        String username = faker.superhero().prefix() + firstName + faker.address().buildingNumber();
        UserRequest userRequest = new UserRequest(email, firstName, lastName, username.toLowerCase(Locale.ROOT), Utils.generatePassayPassword(),
                status, "");

        System.out.println(email + " " + firstName + " " + lastName + " " + username.toLowerCase(Locale.ROOT) + " " + userRequest.getPassword()+" "+
                status);

        return userRequest;
    }
}
