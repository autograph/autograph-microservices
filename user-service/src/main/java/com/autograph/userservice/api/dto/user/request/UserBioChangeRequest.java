package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class UserBioChangeRequest {

    String bio;
    String username;
}
