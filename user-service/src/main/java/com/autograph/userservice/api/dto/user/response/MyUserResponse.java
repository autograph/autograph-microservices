package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.entity.user.SocialMediaAccount;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import lombok.Data;

import java.util.List;

@Data
public class MyUserResponse {

    private Long id;
    private String email;
    private String username;
    private String phoneNumber;
    private String address;
    private String name;
    private String surname;
    private String qrCodeValue;
    private List<SocialMediaAccount> socialMediaAccounts;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private Long profilePictureId;
    private int unreadNotificationCount;
    private Long followed, following;
    private String bio;
    private UserVerificationStatus userVerificationStatus;
    private Boolean isBlocked;
    private Long autographCount;
}