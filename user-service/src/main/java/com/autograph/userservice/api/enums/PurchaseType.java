package com.autograph.userservice.api.enums;

public enum PurchaseType {
    PURCHASE_NOW,
    AUCTION,
    ASYNC_AUCTION,
    NOT_FOR_SALE
}
