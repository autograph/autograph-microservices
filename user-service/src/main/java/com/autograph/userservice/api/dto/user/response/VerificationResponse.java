package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.enums.UserVerificationStatus;
import lombok.Data;

@Data
public class VerificationResponse {

    private Long id;
    private Long userId;
    private String userAddress;
    private String name, surname, description;
    private String websiteURL;
    private UserVerificationStatus userVerificationStatus;

    private String resultDescription;
}
