package com.autograph.userservice.api.dto.user.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    private String email;
    private String name;
    private String surname;
    private String username;
    private String password;
    private String about;
    private String websiteUrl;
}
