package com.autograph.userservice.api.service.user.impl;

import com.autograph.userservice.api.config.auth.AuthenticationFacade;
import com.autograph.userservice.api.config.auth.JwtTokenProvider;
import com.autograph.userservice.api.dto.user.request.LoginRequest;
import com.autograph.userservice.api.dto.user.request.UserRequest;
import com.autograph.userservice.api.dto.user.request.UserWalletRequest;
import com.autograph.userservice.api.dto.user.request.WalletLoginRequest;
import com.autograph.userservice.api.dto.user.request.authmodel.FacebookAuthModel;
import com.autograph.userservice.api.dto.user.request.authmodel.GoogleAuthModel;
import com.autograph.userservice.api.dto.user.request.authmodel.TwitterAuthModel;
import com.autograph.userservice.api.dto.user.response.AuthorizationDetailsDTO;
import com.autograph.userservice.api.dto.user.response.MyUserResponse;
import com.autograph.userservice.api.dto.user.response.UserResponse;
import com.autograph.userservice.api.entity.user.PrincipalUser;
import com.autograph.userservice.api.entity.user.Role;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.entity.user.authmodel.FacebookUserModel;
import com.autograph.userservice.api.entity.user.authmodel.GoogleUserModel;
import com.autograph.userservice.api.entity.user.authmodel.TwitterUserModel;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.repository.user.IRoleRepository;
import com.autograph.userservice.api.repository.user.IUserRepository;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.utils.Utils;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private AuthenticationFacade authenticationFacade;
    @Autowired
    private AuthorizationServerEndpointsConfiguration configuration;
    @Autowired
    private WebClient.Builder webClient;


    public CustomUserDetailsService(){

    }

    public ApiResponse findUserByEmail(String email) {
        ApiResponse apiResponse = new ApiResponse();
        try {

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(
                getUserResponse(userRepository.findUserByEmailAndStatus(email.toLowerCase(Locale.ROOT), true)));

        } catch (Exception e) {
            apiResponse.setCode(ResponseCodes.INTERNAL.getValue());
        }
        return null;
    }

    public User findByEmail(String email) {
        return userRepository.findUserByEmailAndStatus(email.toLowerCase(Locale.ROOT), true);
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        LOG.info(email);
        User user = userRepository.findUserByEmailAndStatusTrue(email.toLowerCase(Locale.ROOT));
        if (user != null) {
            return buildUserForAuthentication(user, getUserAuthority(new HashSet<>(user.getRoles())));
        }
        return null;
    }

    private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
        Set<GrantedAuthority> roles = new HashSet<>();
        userRoles.forEach((role) -> roles.add(new SimpleGrantedAuthority(role.getName())));

        return new ArrayList<>(roles);
    }

    private UserDetails buildUserForAuthentication(User user,
        List<GrantedAuthority> authorities) {
        String username = user.getEmail().toLowerCase(Locale.ROOT);
        String password = user.getPassword();
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);

        return new PrincipalUser(username, password,enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities, myUserResponse);
    }


    public String loginResponseHeader(LoginRequest loginRequest) {
        Collection<Role> roleSet;
        Long id;
        User authObject = findByEmail(loginRequest.getEmail().toLowerCase(Locale.ROOT));
        roleSet = authObject.getRoles();
        id = authObject.getId();

        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getEmail().toLowerCase(Locale.ROOT),
                loginRequest.getPassword()));


        return getLoginHeader(loginRequest.getEmail().toLowerCase(Locale.ROOT), new HashSet<>(roleSet), id);
    }

    public String getLoginHeader(String email, Set<Role> roleSet, Long id) {
        String token = "Bearer " + jwtTokenProvider.createToken(email.toLowerCase(Locale.ROOT), roleSet, id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Expose-Headers", "authorization");
        responseHeaders.add("Authorization", token);
        return token;
    }

    private User convertUserRequestToUser(UserRequest userRequest) {
        return modelMapper.map(userRequest, User.class);
    }

    public UserResponse getUserResponse(User user) {
        return modelMapper.map(user, UserResponse.class);
    }


    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new AuthorizationServiceException("");
        }
        String name = authentication.getName();
        User user = userRepository.findUserByEmailAndStatusTrue(name);
        return user;
    }

    public ApiResponse requestWalletLoginToken(String address){
        ApiResponse apiResponse = new ApiResponse();
        User user = userRepository.findUserByAddress(address);

        if(user != null){
           String generatedString = Utils.generateWalletToken();

            user.setUserWalletLoginToken(generatedString);
            userRepository.save(user);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(generatedString);
        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    public ApiResponse loginWithWallet(String address, String signature){
        ApiResponse apiResponse = new ApiResponse();
        User user = userRepository.findUserByAddress(address);


        if(user != null) {
            Collection<Role> roleSet;
            roleSet = user.getRoles();
            Long id = user.getId();
            boolean verification = Utils.isSignatureValid(address, signature, user.getUserWalletLoginToken());

            if(verification && user.isAddressVerified() && !user.getUserWalletLoginToken().isEmpty()
            && user.getUserWalletLoginToken() != null) {
//                authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(user.getEmail(),
//                        user.getPassword()));

                user.setUserWalletLoginToken(null);
                userRepository.save(user);

                ArrayList<String> scopes = new ArrayList<>();
                scopes.add("ui");
                HashMap<String, String> token = generateOAuth2AccessToken(user, user.getRoles().stream().collect(Collectors.toList()), scopes);
                apiResponse.setResponse(token);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            }else{
                apiResponse.setCode(ResponseCodes.USER_ADDRESS_NOT_VERIFIED.getValue());
            }
        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    public ApiResponse facebook(FacebookAuthModel facebookAuthModel) {
        WebClient.Builder webClientBuilder = WebClient.builder().clientConnector(new
                ReactorClientHttpConnector(HttpClient.create(ConnectionProvider.newConnection())));

        String templateUrl = String.format("https://graph.facebook.com/me?fields=email,first_name,last_name&access_token=%s", facebookAuthModel.getAuthToken());
        FacebookUserModel facebookUserModel = webClientBuilder.build().get().uri(templateUrl).retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    throw new ResponseStatusException(clientResponse.statusCode(), "facebook login error");
                })
                .bodyToMono(FacebookUserModel.class)
                .block();

        final User user = userRepository.findUserByEmail(facebookUserModel.getEmail());
        if (user == null) {        //we have no user with given email so register them
            return createUserOAuth(facebookUserModel.createUserRequest());
        } else { // user exists just login
            HashMap<String, String> accessToken =
                    generateOAuth2AccessToken(user, new ArrayList<>(user.getRoles()), Arrays.asList("ui", "application"));

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponse(accessToken);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            return apiResponse;
        }
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setResponse(true);
//        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
//        return apiResponse;
    }

    public ApiResponse twitter(TwitterAuthModel twitterAuthModel) {
        String TWITTER_CONSUMER_KEY = "jTEQ3bD0cI7Yd9hjdqAeYrCfk";
        String TWITTER_CONSUMER_SECRET = "XOEi7sWqQdRMLWYHyXaHO422embUac5SsOD07HpgvW8RXUtyVQ";
        String TWITTER_USER_OAUTH = twitterAuthModel.getAuthToken();
        String TWITTER_USER_OAUTH_SECRET = twitterAuthModel.getAuthTokenSecret();

        Twitter twitter = new TwitterTemplate(TWITTER_CONSUMER_KEY,
                TWITTER_CONSUMER_SECRET,
                TWITTER_USER_OAUTH,
                TWITTER_USER_OAUTH_SECRET);
        TwitterProfile profile = twitter.userOperations().getUserProfile();
        TwitterUserModel userModel = twitter.restOperations().getForObject("https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true", TwitterUserModel.class);

        final User user = userRepository.findUserByEmail(userModel.getEmail());
        if (user == null) {        //we have no user with given email so register them
            return createUserOAuth(userModel.createUserRequest());
        }else{
            HashMap<String, String> accessToken =
                    generateOAuth2AccessToken(user, new ArrayList<>(user.getRoles()), Arrays.asList("ui", "application"));

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponse(accessToken);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            return apiResponse;
        }
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setResponse(true);
//        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
//        return apiResponse;
    }

    public ApiResponse google(GoogleAuthModel googleAuthModel) throws GeneralSecurityException, IOException {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
                .setAudience(Collections.singletonList("257979074189-7fhtihjagr2si9s92crkq0au6hpls6vo.apps.googleusercontent.com"))
                .build();

        GoogleIdToken idToken = verifier.verify(googleAuthModel.getIdToken());
        if (idToken != null) {
            System.out.println("Id token is not null");
            Payload payload = idToken.getPayload();

            // Print user identifier
            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);

            // Get profile information from payload
            String email = payload.getEmail();
            boolean emailVerified = payload.getEmailVerified();
            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
            String locale = (String) payload.get("locale");
            String familyName = (String) payload.get("family_name");
            String givenName = (String) payload.get("given_name");

            GoogleUserModel googleUserModel = new GoogleUserModel(email, emailVerified, name,
                    pictureUrl, givenName, familyName, locale, userId);

            User user = userRepository.findUserByEmail(googleUserModel.getEmail());
            if (user == null) {        //we have no user with given email so register them
                System.out.println("User is null");
                return createUserOAuth(googleUserModel.createUserRequest());
            } else { // user exists just login
                System.out.println("User is not null");
                HashMap<String, String> accessToken =
                        generateOAuth2AccessToken(user, new ArrayList<>(user.getRoles()), Arrays.asList("ui", "application"));

                ApiResponse apiResponse = new ApiResponse();
                apiResponse.setResponse(accessToken);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                return apiResponse;
            }
        }else {
            System.out.println("Id token is null");
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponse(true);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            return apiResponse;
        }
    }



    public ApiResponse login(LoginRequest loginRequest) {
        ApiResponse apiResponse = new ApiResponse();
            LOG.info("User tries to login user: {}, date:{}", loginRequest.getEmail(),
                LocalDateTime.now());
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            if (userRepository.findUserByEmailAndStatus(loginRequest.getEmail().toLowerCase(Locale.ROOT), true) != null) {
                apiResponse.setResponse(loginResponseHeader(loginRequest));
            } else {
                apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            }

        return apiResponse;
    }

    public ApiResponse createUser(UserRequest userRequest){
        ApiResponse apiResponse = new ApiResponse();

        boolean userExists = userRepository.existsByEmail(userRequest.getEmail().toLowerCase(Locale.ROOT)) ||
            userRepository.existsByUsername(userRequest.getUsername().toLowerCase(Locale.ROOT));

        if(userExists){
            apiResponse.setCode(ResponseCodes.ALREADY_EXISTS.getValue());
            return apiResponse;
        }

        if(userRequest.getPassword().length() < 8){
            apiResponse.setCode(ResponseCodes.PASSWORD_TOO_SHORT.getValue());
            return apiResponse;
        }

        if(userRequest.getUsername() == null || userRequest.getUsername().length() < 5){
            apiResponse.setCode(ResponseCodes.USERNAME_TOO_SHORT.getValue());
            return apiResponse;
        }

        Role role = roleRepository.findByName(UserRole.CUSTOMER.name());

        User user = new User(userRequest.getEmail().toLowerCase(Locale.ROOT), userRequest.getName(), userRequest.getSurname(),
            userRequest.getUsername().toLowerCase(Locale.ROOT), userRequest.getAbout(),
            userRequest.getWebsiteUrl(), bCryptPasswordEncoder.encode(userRequest.getPassword()));

        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);

        LoginRequest loginRequest = new LoginRequest(userRequest.getEmail().toLowerCase(Locale.ROOT), userRequest.getPassword());

        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),
                loginRequest.getPassword()));

        String loginHeader = getLoginHeader(loginRequest.getEmail(), user.getRoles(), user.getId());

        apiResponse.setResponse(loginHeader);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }


    public ApiResponse createUserOAuth(UserRequest userRequest){
        ApiResponse apiResponse = new ApiResponse();

        boolean userExists = userRepository.existsByEmail(userRequest.getEmail().toLowerCase(Locale.ROOT)) ||
                userRepository.existsByUsername(userRequest.getUsername().toLowerCase(Locale.ROOT));

        if(userExists){
            apiResponse.setCode(ResponseCodes.ALREADY_EXISTS.getValue());
            return apiResponse;
        }

        if(userRequest.getPassword().length() < 8){
            apiResponse.setCode(ResponseCodes.PASSWORD_TOO_SHORT.getValue());
            return apiResponse;
        }

        if(userRequest.getUsername() == null || userRequest.getUsername().length() < 5){
            apiResponse.setCode(ResponseCodes.USERNAME_TOO_SHORT.getValue());
            return apiResponse;
        }

        Role role = roleRepository.findByName(UserRole.CUSTOMER.name());

        User user = new User(userRequest.getEmail().toLowerCase(Locale.ROOT), userRequest.getName(), userRequest.getSurname(),
                userRequest.getUsername().toLowerCase(Locale.ROOT), userRequest.getAbout(),
                userRequest.getWebsiteUrl(), bCryptPasswordEncoder.encode(userRequest.getPassword()));

        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);

        LoginRequest loginRequest = new LoginRequest(userRequest.getEmail().toLowerCase(Locale.ROOT), userRequest.getPassword());

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),
                        loginRequest.getPassword()));

        HashMap<String, String> accessToken =
                generateOAuth2AccessToken(user, new ArrayList<>(user.getRoles()), Arrays.asList("ui", "application"));

        apiResponse.setResponse(accessToken);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }




    public ApiResponse createUserByAddress(UserWalletRequest userWalletRequest){
        ApiResponse apiResponse = new ApiResponse();

        boolean userExists = userRepository.existsByAddress(userWalletRequest.getAddress());

        if(userExists){
            apiResponse.setCode(ResponseCodes.ALREADY_EXISTS.getValue());
            return apiResponse;
        }

        if(userWalletRequest.getUsername() == null || userWalletRequest.getUsername().length() < 5){
            apiResponse.setCode(ResponseCodes.USERNAME_TOO_SHORT.getValue());
            return apiResponse;
        }

        Role role = roleRepository.findByName(UserRole.CUSTOMER.name());
        User user = new User(userWalletRequest.getUsername(), userWalletRequest.getAddress());

        Set<Role> roles = new HashSet<>();
        roles.add(role);

        user.setRoles(roles);
        userRepository.save(user);

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(user.getUserWalletLoginToken());

        return apiResponse;
    }

    public ApiResponse verifyRegisteredWalletAccount(WalletLoginRequest walletLoginRequest){
        ApiResponse apiResponse = new ApiResponse();

        User user = userRepository.findUserByAddress(walletLoginRequest.getAddress());
        if (user != null) {
            boolean verified = Utils.isSignatureValid(user.getAddress(),
                walletLoginRequest.getSignature(), user.getUserWalletLoginToken());

            if(verified){
                user.setAddressVerified(true);
                userRepository.save(user);
            }
            apiResponse.setResponse(verified);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }
        else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    public ApiResponse parseHeader() {
        ApiResponse apiResponse = new ApiResponse();

        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(new AuthorizationDetailsDTO(user.getId(), user.getRoles()));
        } else {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    public HashMap<String, String> generateOAuth2AccessToken(User user, List<Role> roles, List<String> scopes) {

        Map<String, String> requestParameters = new HashMap<String, String>();
        Map<String, Serializable> extensionProperties = new HashMap<String, Serializable>();

        boolean approved = true;
        Set<String> responseTypes = new HashSet<String>();
        responseTypes.add("code");

        // Authorities
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for(Role role: roles)
            authorities.add(new SimpleGrantedAuthority(role.getName()));

        OAuth2Request oauth2Request = new OAuth2Request(requestParameters, "application", authorities, approved,
                new HashSet<String>(scopes), new HashSet<String>(Arrays.asList("oauth2-resource")), null, responseTypes, extensionProperties);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loadUserByUsername(user.getEmail()),
                "N/A", authorities);

        OAuth2Authentication auth = new OAuth2Authentication(oauth2Request, authenticationToken);

        auth.setDetails(loadUserByUsername(user.getEmail()));
        AuthorizationServerTokenServices tokenService = configuration.getEndpointsConfigurer().getTokenServices();

        OAuth2AccessToken token = tokenService.createAccessToken(auth);
        OAuth2RefreshToken refreshToken = token.getRefreshToken();

        HashMap<String, String> tokens = new HashMap<>();
        tokens.put("access_token", token.getValue());
        tokens.put("refresh_token", refreshToken.getValue());

        return tokens;
    }
}