package com.autograph.userservice.api.dto.user.request;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FillUserInfoRequest {

    @Id
    @NotNull
    @GeneratedValue
    Long id;
    @NotNull
    String name, surname, username, email, details;
    String website;
}
