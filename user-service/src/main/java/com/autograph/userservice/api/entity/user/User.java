package com.autograph.userservice.api.entity.user;

import com.autograph.userservice.api.entity.follow.UserFollow;
import com.autograph.userservice.api.entity.verification.VerificationDetails;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToOne(mappedBy = "user")
    VerificationDetails verificationDetails;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    List<Long> autographs;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    List<Long> createdAutographs;
    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
        name = "users_roles",
        joinColumns = @JoinColumn(
            name = "user_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "role_id", referencedColumnName = "id"))
    Set<Role> roles = new HashSet<>();
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    Set<Long> previousAutographs = new HashSet<>();
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    Set<Long> likedAutographs;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "user")
    Set<UserFollow> followingUsers;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "followingUser")
    Set<UserFollow> followedUsers;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    List<Long> promotions;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    List<String> searchQueries = new ArrayList<>();
    @Id
    @GeneratedValue
    private Long id;
    @Email
    private String email;
    private boolean addressVerified;
    private String address, verificationString, userWalletLoginToken, firebaseNotificationToken, forgotPasswordToken;
    private String name, surname, username, about, websiteURL, phoneNumber, bio;
    private UserVerificationStatus userVerificationStatus;
    private Long following = 0L, followed = 0L;
    private Boolean isBlocked, status, likeNotification, bidNotification, bidAcceptedNotification, bidRejectedNotification, followNotification;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @CreatedDate
    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @CreatedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_profile_picture",
        joinColumns =
            {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns =
            {@JoinColumn(name = "profile_picture_id", referencedColumnName = "id")})
    private com.autograph.userservice.api.entity.user.ProfilePicture profilePicture;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    private List<SocialMediaAccount> socialMediaAccounts;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    private List<Long> activityMessages;


    public User(String email, String name, String surname, String username, String about,
        String websiteURL, String password) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.about = about;
        this.websiteURL = websiteURL;
        this.password = password;
        this.status = true;
        this.firebaseNotificationToken = "";
        this.socialMediaAccounts = new ArrayList<>();
        this.followedUsers = new HashSet<>();
        this.followingUsers = new HashSet<>();
        this.likedAutographs = new HashSet<>();
        this.roles = new HashSet<>();
        this.createdAutographs = new ArrayList<>();
        this.autographs = new ArrayList<>();
        this.userVerificationStatus = UserVerificationStatus.STALE;
        this.verificationDetails = null;
        this.createdDate = LocalDateTime.now();
        this.lastModifiedDate = LocalDateTime.now();
        this.isBlocked = false;
        this.following = 0L;
        this.followed = 0L;
        this.address = "placeholder_" + RandomStringUtils.random(30, true, true);
        this.addressVerified = false;
        this.userWalletLoginToken = "";
        this.phoneNumber = "";
        this.bio = about;
        this.activityMessages = new ArrayList<>();
        this.promotions = new ArrayList<>();

        this.likeNotification = true;
        this.followNotification = true;
        this.bidAcceptedNotification = true;
        this.bidRejectedNotification = true;
        this.bidNotification = true;
    }


    public User(String username, String address) {
        this.email = "";
        this.name = "";
        this.surname = "";
        this.username = username;
        this.address = address;
        this.about = "";
        this.websiteURL = "";
        this.password = "";
        this.status = true;
        this.firebaseNotificationToken = "";
        this.socialMediaAccounts = new ArrayList<>();
        this.followedUsers = new HashSet<>();
        this.followingUsers = new HashSet<>();
        this.likedAutographs = new HashSet<>();
        this.roles = new HashSet<>();
        this.createdAutographs = new ArrayList<>();
        this.autographs = new ArrayList<>();
        this.userVerificationStatus = UserVerificationStatus.STALE;
        this.verificationDetails = null;
        this.createdDate = LocalDateTime.now();
        this.lastModifiedDate = LocalDateTime.now();
        this.isBlocked = false;
        this.following = 0L;
        this.followed = 0L;
        this.address = address;
        this.addressVerified = false;
        this.userWalletLoginToken = Utils.generateWalletToken();
        this.phoneNumber = "";
        this.bio = about;
        this.activityMessages = new ArrayList<>();
        this.promotions = new ArrayList<>();

        this.likeNotification = true;
        this.followNotification = true;
        this.bidAcceptedNotification = true;
        this.bidRejectedNotification = true;
        this.bidNotification = true;
    }
}