package com.autograph.userservice.api.service.user.impl;

import com.autograph.userservice.api.entity.user.Privilege;
import com.autograph.userservice.api.entity.user.Role;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.enums.Privileges;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.repository.user.IPrivilegeRepository;
import com.autograph.userservice.api.repository.user.IRoleRepository;
import com.autograph.userservice.api.repository.user.IUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataInitializer implements CommandLineRunner {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserRepository userRepository;


    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private IPrivilegeRepository privilegeRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;


    @Override
    public void run(String... args) {
        if (roleRepository.findAll().size() == 0) {
            Privilege readPrivilege
                = createPrivilegeIfNotFound(Privileges.VIEW.toString());
            Privilege writePrivilege
                = createPrivilegeIfNotFound(Privileges.CREATE.toString());
            Privilege editPrivilege
                = createPrivilegeIfNotFound(Privileges.EDIT.toString());
            Privilege deletePrivilege
                = createPrivilegeIfNotFound(Privileges.DELETE.toString());

            Set<Privilege> adminPrivileges = new HashSet<>();
            Set<Privilege> moderatorPrivileges = new HashSet<>();
            Set<Privilege> celebrityPrivileges = new HashSet<>();
            Set<Privilege> customerPrivileges = new HashSet<>();

            adminPrivileges.add(deletePrivilege);
            moderatorPrivileges.add(editPrivilege);
            celebrityPrivileges.add(writePrivilege);
            customerPrivileges.add(readPrivilege);

            Role admin = createRoleIfNotFound(UserRole.ADMIN.name(), adminPrivileges);
            Role moderator = createRoleIfNotFound(UserRole.MODERATOR.name(), moderatorPrivileges);
            Role celebrity = createRoleIfNotFound(UserRole.CELEBRITY.name(), celebrityPrivileges);
            Role customer= createRoleIfNotFound(UserRole.CUSTOMER.name(), customerPrivileges);

            Set<Role> rolesAdmin = new HashSet<>();
            Set<Role> rolesModerator = new HashSet<>();
            Set<Role> rolesCelebrity = new HashSet<>();
            rolesAdmin.add(admin);
            rolesModerator.add(moderator);
            rolesCelebrity.add(celebrity);
            userRepository.save(
                User.builder()
                    .createdDate(LocalDateTime.now())
                    .email("murad@gmail.com")
                    .password(bCryptPasswordEncoder.encode("123456"))
                    .name("Murad")
                    .surname("Mammad")
                    .username("muradm373")
                    .bio("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .about("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .phoneNumber("+994556181203")
                    .userVerificationStatus(UserVerificationStatus.VERIFIED)
                    .status(true)
                    .address("000")
                    .roles(null)
                    .followed(0L)
                    .following(0L)
                    .roles(rolesAdmin)
                    .websiteURL("https://www.autographspace.com")
                    .build()
            );

            userRepository.save(
                User.builder()
                    .createdDate(LocalDateTime.now())
                    .email("arif@gmail.com")
                    .password(bCryptPasswordEncoder.encode("123456"))
                    .name("Arif")
                    .surname("Movsumzade")
                    .username("arif.movsumzade")
                    .phoneNumber("+994513954085")
                    .bio("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .about("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .userVerificationStatus(UserVerificationStatus.PENDING)
                    .status(true)
                    .address("aaa")
                    .roles(null)
                    .followed(0L)
                    .following(0L)
                    .roles(rolesModerator)
                    .websiteURL("https://www.autographspace.com")
                    .build()
            );

            userRepository.save(
                User.builder()
                    .createdDate(LocalDateTime.now())
                     .username("sssemil")
                    .email("emil@gmail.com")
                    .password(bCryptPasswordEncoder.encode("123456"))
                    .name("Emil")
                    .surname("Suleymanov")
                    .phoneNumber("+994513954085")
                    .bio("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .about("Esse culpa occaecat tempor qui non \uD83D\uDCF7 velit consectetur non proident cupidatat dolor occaecat eu.")
                    .userVerificationStatus(UserVerificationStatus.STALE)
                    .status(true)
                    .address("anonymous")
                    .roles(null)
                    .followed(0L)
                    .following(0L)
                    .roles(rolesCelebrity)
                    .websiteURL("https://www.autographspace.com")
                    .build()
            );

        }
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound(
        String name, Set<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
