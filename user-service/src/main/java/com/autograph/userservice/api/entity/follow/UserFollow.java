package com.autograph.userservice.api.entity.follow;

import com.autograph.userservice.api.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserFollow {

    @EmbeddedId
    UserFollowKey id;

    @ManyToOne
    @MapsId("userId")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @MapsId("followingId")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "following_id")
    User followingUser;

    LocalDateTime startedFollowAt;
    Boolean muted;
    Boolean notification;

}
