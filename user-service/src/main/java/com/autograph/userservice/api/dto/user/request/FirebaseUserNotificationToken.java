package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class FirebaseUserNotificationToken {
    String token;
}
