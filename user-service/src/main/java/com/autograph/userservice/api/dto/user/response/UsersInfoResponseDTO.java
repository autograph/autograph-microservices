package com.autograph.userservice.api.dto.user.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;

@AllArgsConstructor
@Data
public class UsersInfoResponseDTO {
    Integer numberOfUsers;
    HashMap<Integer, Integer> registeredByMonth;
    HashMap<Integer, Integer> registeredByYear;
}
