package com.autograph.userservice.api.config.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class ResourceServerSecurityConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests()
                .antMatchers("/api/user/auth/**").permitAll()
                .antMatchers("/api/actuator").permitAll()
                .antMatchers("/ws-product/**").permitAll()
                .antMatchers("/ws-product").permitAll()
                .antMatchers("/ws/**").permitAll()
                .antMatchers("/oauth/token").permitAll()
                .antMatchers("/api/user/username-email/availability").permitAll()
                .antMatchers("/api/user/auth/signin/wallet").permitAll()
                .antMatchers("/api/user/auth/signin/wallet/walletLoginTokenRequest").permitAll()
                .antMatchers("/api/user/update-password").permitAll()
                .antMatchers("/api/user/forgot-password").permitAll()
                .antMatchers("/api/user/header/*").permitAll()
                .antMatchers(
                        "/v2/api-docs", "/swagger-ui.html",
                        "/swagger-resources", "/swagger-resources/**",
                        "/swagger-resources/configuration/ui",
                        "/swagger-resources/configuration/security")
                .permitAll()
                .anyRequest().authenticated()
                .and().csrf().disable();
    }
}
