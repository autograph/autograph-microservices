package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.entity.user.Role;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AuthorizationDetailsDTO {

    Long id;
    Set<Role> role;
}
