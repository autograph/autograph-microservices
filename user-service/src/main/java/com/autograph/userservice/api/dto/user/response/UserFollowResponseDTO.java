package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.entity.follow.UserFollowKey;
import com.autograph.userservice.api.entity.user.User;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserFollowResponseDTO {

    UserFollowKey id;
    User user;
    User followingUser;
    LocalDateTime startedFollowingAt;
}
