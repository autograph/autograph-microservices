package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class EditUserRequest {

    private String email;
    private String phoneNumber;
    private String name;
    private String surname;

}
