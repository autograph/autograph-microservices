package com.autograph.userservice.api.controller.user;


import com.autograph.userservice.api.dto.user.request.*;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.user.IUserService;
import com.autograph.userservice.api.service.user.impl.ProfilePictureService;
import com.autograph.userservice.api.service.verification.impl.VerificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserController {
    private final VerificationService verificationService;
    private final IUserService userService;
    private final ProfilePictureService profilePictureService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public UserController(VerificationService verificationService, ProfilePictureService profilePictureService, IUserService userService) {
        this.verificationService = verificationService;
        this.profilePictureService = profilePictureService;
        this.userService = userService;
    }

    @PostMapping("/user/verification/{address}")
    public ApiResponse setUserAddress(@PathVariable("address") String address)  {
        return userService.requestVerificationToken(address);
    }

    @PostMapping("/user/firebase/notification")
    public ApiResponse setNotificationSettings(@RequestHeader("Authorization") String authorization,
                                             @RequestBody NotificationSetting notificationSetting) {
        return userService.setNotificationSettings(notificationSetting, authorization);
    }

    @GetMapping("/user/firebase/notification")
    public ApiResponse getNotificationSettings(@RequestHeader("Authorization") String authorization) {
        return userService.getNotificationSettings(authorization);
    }

    @GetMapping("/user/address/verification")
    public ApiResponse checkAddressVerification(@RequestParam String signature)  {
        return userService.checkVerificationToken(signature);
    }

    @GetMapping("/user/username-email/availability")
    public ApiResponse checkUsernameAvailability(@RequestParam String username, @RequestParam String email)  {
        return userService.checkUsernameAvailability(username, email);
    }

    @GetMapping("/user/imported-autograph/invite")
    public ApiResponse informUserAboutImportedAutograph(@RequestParam String email, @RequestParam Long autographId,
                                                        @RequestHeader("Authorization") String authorization) throws MessagingException, IOException {
        return userService.informUserAboutImportedAutograph(email, autographId, authorization);
    }

    @GetMapping("/user/header/{id}")
    public ApiResponse getUserHeaderById(@PathVariable Long id)  {
        return userService.getUserHeaderById(id);
    }

    @PostMapping("/user/headers")
    public ApiResponse getUserHeadersByIdList(@RequestBody List<Long> ids)  {
        return userService.getUserHeadersByIdList(ids);
    }

    @GetMapping("/user/{address}/owned/{page}")
    public ApiResponse getUserOwnedAutographs(@PathVariable("address") String address,
        @PathVariable("page") int page, @RequestHeader("Authorization") String authorization)  {
        return userService.getAutographsOwnedByUserAddress(address, page, authorization);
    }

    @GetMapping("/user/{address}/created/{page}")
    public ApiResponse getUserCreatedAutographs(@PathVariable("address") String address,
        @PathVariable("page") int page, @RequestHeader("Authorization") String authorization)  {
        return userService.getAutographsCreatedByUserAddress(address, page, authorization);
    }

    @GetMapping("/user/{address}")
    public ApiResponse getUserByAddress(@PathVariable("address") String address)  {
        return userService.findUserByAddress(address);
    }

    @PostMapping("/user/forgot-password")
    public ApiResponse forgotPasswordRequest(@RequestBody ForgotPasswordRequest request)  {
        return userService.forgotPasswordRequest(request);
    }

    @PostMapping("/user/update-password")
    public ApiResponse updatePasswordRequest(@RequestBody UpdatePasswordTokenRequest request)  {
        return userService.updatePasswordRequest(request);
    }

    @GetMapping("/user/users-info")
    public ApiResponse getUsersInfo(){
        return userService.getUsersInfo();
    }

    @GetMapping("/user/search")
    public ApiResponse search(@RequestParam String query) {
        return userService.search(query);
    }

    @GetMapping("/user/{address}/liked")
    public ApiResponse getLikedAutographs(@PathVariable("address") String address,
                                          @RequestHeader("Authorization") String authorization)
         {
        return userService.getLikedAutographs(address, authorization);
    }

    @GetMapping("/user/liked")
    public ApiResponse getLikedAutographs(@RequestHeader("Authorization") String authorization)
    {
        return userService.getLikedAutographs(authorization);
    }

    @PostMapping("/user/liked/{autographId}")
    public ApiResponse addLikedAutograph(@PathVariable Long autographId){
        return userService.likeAutograph(autographId);
    }

    @PostMapping("/users")
    public ApiResponse getUsersByList(@RequestBody List<Long> ids){
        return userService.getUserHeaders(ids);
    }

    @PostMapping("/user/autograph/{autographId}")
    public ApiResponse addAutograph(@PathVariable("autographId") Long autographId) {
        return userService.addAutograph(autographId);
    }

    @GetMapping("/user/{address}/previous")
    public ApiResponse getPreviousAutographs(@PathVariable("address") String address, @RequestHeader("Authorization") String authorization)
         {
        return userService.getPreviousAutographs(address, authorization);
    }

    @GetMapping("/user/email/{email}")
    public ApiResponse getUserByEmail(@PathVariable("email") String email)  {
        return userService.findUserByEmail(email);
    }

    @GetMapping("/user/all")
    public ApiResponse getAllUsers()  {
        return userService.getAllUsers();
    }

    @GetMapping("/user/role/{role}")
    public ApiResponse getUsersByRole(@PathVariable("role") UserRole role)  {
        return userService.getUsersByRole(role);
    }

    @GetMapping("/user/verification/{status}")
    public ApiResponse getUsersByStatus(@PathVariable("status") UserVerificationStatus status)  {
        return userService.getUsersByStatus(status);
    }

    @GetMapping("/user/{address}/verification")
    public ApiResponse getVerificationStatus(@PathVariable("address") String address)
         {
        return verificationService.getVerificationStatus(address);
    }

    @PostMapping("/user/data")
    public ApiResponse fillAccountData(@RequestBody FillUserInfoRequest fillUserInfoRequest)
         {
        return userService.fillAccountData(fillUserInfoRequest);
    }

    @PostMapping("/user/data/bio")
    public ApiResponse fillBio(@RequestBody UserBioChangeRequest userBioChangeRequest)
         {
        return userService.bioChangeRequest(userBioChangeRequest);
    }

    @GetMapping("/user/data")
    public ApiResponse getAccountData()
         {
        return userService.findUser();
    }

    @GetMapping("/user/profile_picture")
    public Resource getProfilePicture() throws IOException {
        return userService.getProfilePicture();
    }

    @GetMapping("/user/roles")
    public ApiResponse getUserRoles()  {
        return userService.getRoles();
    }

    @DeleteMapping("/user")
    public ApiResponse deleteAccount()  {
        return userService.removeUserAccount();
    }

    @PostMapping("/user/verification")
    public ApiResponse requestVerification(@RequestBody FillUserInfoRequest fillUserInfoRequest)
         {
        return verificationService.requestVerification(fillUserInfoRequest);
    }

    @GetMapping("/user/verification")
    public ApiResponse requestVerificationInfo()
    {
        return verificationService.getVerificationStatus();
    }

    @PostMapping("/user/profile-picture")
    public ApiResponse setProfilePicture(@ModelAttribute ProfilePictureRequestDTO profilePictureRequestDTO)
         {
        return userService.setProfilePicture(profilePictureRequestDTO);
    }

    @GetMapping("/user/profile-picture/{id}")
    public ApiResponse getProfilePictureById(@PathVariable("id") Long id)
         {
        return profilePictureService.getProfilePictureById(id);
    }

    @DeleteMapping("/user/verification")
    public ApiResponse cancelVerification()  {
        return verificationService.cancelVerification();
    }


    @PostMapping("/user/search-queries")
    public ApiResponse pushSearch(@RequestParam String query)
    {
        return userService.searchPush(query);
    }

    @GetMapping("/user/search-queries")
    public ApiResponse getSearch()
    {
        return userService.getSearchQueries();
    }

    @PostMapping("/user/firebase-token")
    public ApiResponse setUserFirebaseNotificationToken(@RequestBody FirebaseUserNotificationToken token,
                                                        @RequestHeader("Authorization") String authorization){
        return userService.setFirebaseNotificationToken(authorization, token.getToken());
    }

    @DeleteMapping("/user/firebase-token")
    public ApiResponse setUserFirebaseNotificationToken(@RequestHeader("Authorization") String authorization){
        return userService.removeFirebaseNotificationToken(authorization);
    }

    @GetMapping("/user/firebase-token/{id}")
    public ApiResponse setUserFirebaseNotificationToken(@PathVariable("id") Long userId,
                                                        @RequestHeader("Authorization") String authorization){
        return userService.getFirebaseNotificationToken(authorization, userId);
    }

}
