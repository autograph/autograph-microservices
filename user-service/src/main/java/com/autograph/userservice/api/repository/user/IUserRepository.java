package com.autograph.userservice.api.repository.user;

import com.autograph.userservice.api.entity.user.Role;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUserRepository extends JpaRepository<User, Long> {

    User findUserById(Long id);

    User findUserByPhoneNumberAndStatus(String phoneNumber, boolean status);

    User findUserByEmailAndStatusTrue(String email);

    User findUserByEmailAndStatus(String email, boolean status);

    User findUserByIdAndStatus(Long id, boolean status);

    List<User> findAllByUserVerificationStatus(UserVerificationStatus verificationStatus);

    List<User> findAllByRolesContains(Role role);

    List<User> findAllByIsBlocked(Boolean status);

    List<User> findAllByAddressContainingOrUsernameContaining(String query1, String query2);

    User findUserByAddress(String address);

    User findUserByEmail(String address);

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username);

    Boolean existsByAddress(String address);

    User findByAddressOrEmail(String address, String email);

    Integer countByUsernameIsNotNull();


    @Query(value = "SELECT u FROM User u WHERE year(u.createdDate) = :year")
    List<User> countByCreatedDate(@Param("year") Integer year);

    @Query("SELECT u FROM User u WHERE year(u.createdDate)=:year AND month(u.createdDate)=:month")
    List<User> countByCreatedDateMonthAndCreatedDateYear(@Param("month") Integer month,@Param("year") Integer year);

//    @Query("SELECT * FROM users u WHERE u.status=TRUE and id in (SELECT user_id from users_roles WHERE roles_id = 2) AND LOWER(name) LIKE %?1%")
//    List<User> searchUserByName(String query, Pageable pageable);

    User findUserByEmailAndStatusTrueAndPassword(String username, String password);
}