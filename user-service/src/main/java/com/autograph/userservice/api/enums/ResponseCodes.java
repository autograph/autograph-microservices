package com.autograph.userservice.api.enums;


import com.autograph.userservice.api.response.ResponseDTO;

public enum ResponseCodes {
    SUCCESS(new ResponseDTO("success", 2000, "Successfully completed!")),
    INTERNAL(new ResponseDTO("error", 2001, "An exception occurred")),
    AUTOGRAPH_NOT_FOUND(
        new ResponseDTO("autograph not found", 2002, "An exception occurred, no autograph found")),
    USER_NOT_FOUND(new ResponseDTO("error", 2003, "User not found")),
    USER_ADDRESS_NOT_VERIFIED(
        new ResponseDTO("address not verified", 2004, "User address is not verified")),
    NOT_FOUND(new ResponseDTO("not found", 2005, "Not found")),
    AUCTION_NOT_FOUND(new ResponseDTO("auction not found", 2006, "Auction not found")),
    FOLLOW_NOT_FOUND(new ResponseDTO("follow not found", 2007, "Follow info not found")),
    ALREADY_REJECTED(new ResponseDTO("already rejected", 2008, "User was already rejected")),
    ALREADY_EXISTS(
        new ResponseDTO("already exists", 2009, "User with given username/email already exists.")),
    ALREADY_MINTED(new ResponseDTO("already minted", 2010, "This autograph is immutable")),
    CATEGORY_NOT_FOUND(new ResponseDTO("category not found", 2011, "Category not found")),
    AUCTION_TIME(new ResponseDTO("auction time", 2012, "Auction time")),
    YOUR_BID_IS_LESS_THAN_PREVIOUS_BID(
        new ResponseDTO("your bid is less than the previous bid", 2013,
            "Your bid should be more than previous bid or minimal bid")),
    CATEGORY_ALREADY_EXISTS(
        new ResponseDTO("category already exists", 2014, "Category already exists")),
    USER_IS_NOT_OWNER_OF_THE_AUTOGRAPH(new ResponseDTO("user is not owner of the autograph", 2015,
        "This autograph is not owned by the user")),
    PASSWORD_TOO_SHORT(
        new ResponseDTO("too short", 2016, "Password should be at least 8 symbols long")),
    USERNAME_TOO_SHORT(
        new ResponseDTO("too short", 2017, "Username should be at least 5 symbols long")),
    ACTIVITY_NOT_FOUND(new ResponseDTO("activity not found", 2018, "Activity not found")),
    PROMOTION_NOT_FOUND(new ResponseDTO("promotion not found", 2019, "Promotion not found")),
    ADDRESS_ALREADY_EXISTS(new ResponseDTO("This address belongs to another user", 2021, "Address belongs to another user")),
    NOT_OWNER(new ResponseDTO("promotion was not created by you", 2020,
        "Promotion was not created by you"));

    private final ResponseDTO value;

    ResponseCodes(ResponseDTO dto) {
        this.value = dto;
    }

    public ResponseDTO getValue() {
        return value;
    }
}
