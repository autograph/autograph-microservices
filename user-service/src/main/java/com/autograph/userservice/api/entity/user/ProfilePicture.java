package com.autograph.userservice.api.entity.user;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "profile_pictures")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfilePicture {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "profilePicture")
    private User user;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "upload_directory")
    private String uploadDirectory;

    public ProfilePicture(String name, String uploadDirectory) {
        this.fileName = name;
        this.uploadDirectory = uploadDirectory;
    }
}
