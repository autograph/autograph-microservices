package com.autograph.userservice.api.entity.user.authmodel;

import com.autograph.userservice.api.dto.user.request.UserRequest;
import com.autograph.userservice.api.utils.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.javafaker.Faker;
import lombok.Data;

import java.util.Locale;

@Data
public class FacebookUserModel {
    private String id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String email;

    public UserRequest createUserRequest(){
        Faker faker = new Faker();
        String username = faker.superhero().prefix() + firstName + faker.address().buildingNumber();
        UserRequest userRequest = new UserRequest(email, firstName, lastName, username.toLowerCase(Locale.ROOT), Utils.generatePassayPassword(),
                "", "");
        System.out.println(email + " " + firstName + " " + lastName + " " + username.toLowerCase(Locale.ROOT) + " " + userRequest.getPassword());
        return userRequest;
    }
}

