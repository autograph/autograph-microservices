package com.autograph.userservice.api.service.verification.impl;

import com.autograph.userservice.api.config.auth.IAuthenticationFacade;
import com.autograph.userservice.api.dto.user.request.FillUserInfoRequest;
import com.autograph.userservice.api.dto.user.response.VerificationResponse;
import com.autograph.userservice.api.entity.user.Role;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.entity.verification.VerificationDetails;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.repository.user.IRoleRepository;
import com.autograph.userservice.api.repository.user.IUserRepository;
import com.autograph.userservice.api.repository.user.IVerificationRepository;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.verification.IVerificationService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VerificationService implements IVerificationService {

    private final ModelMapper modelMapper;
    private final IUserRepository userRepository;
    private final IVerificationRepository verificationRepository;
    private final IAuthenticationFacade authenticationFacade;
    private final IRoleRepository roleRepository;
    private final ApiResponse apiResponse;
    private final int numberOfItemsOnPage = 20;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    VerificationService(ModelMapper modelMapper, IUserRepository userRepository,
        IVerificationRepository verificationRepository,
        IAuthenticationFacade authenticationFacade,
        IRoleRepository roleRepository, ApiResponse apiResponse) {
        this.userRepository = userRepository;
        this.verificationRepository = verificationRepository;
        this.modelMapper = modelMapper;
        this.authenticationFacade = authenticationFacade;
        this.roleRepository = roleRepository;
        this.apiResponse = apiResponse;
    }

    @Override
    public ApiResponse requestVerification(FillUserInfoRequest fillUserInfoRequest) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            VerificationDetails verificationDetails = user.getVerificationDetails();

            if (verificationDetails == null) {
                VerificationDetails verificationDetailsRequest = new VerificationDetails();
                verificationDetailsRequest.setDescription(fillUserInfoRequest.getDetails());
                verificationDetailsRequest.setUserVerificationStatus(
                    UserVerificationStatus.PENDING);
                verificationDetailsRequest.setName(fillUserInfoRequest.getName());
                verificationDetailsRequest.setSurname(fillUserInfoRequest.getSurname());
                verificationDetailsRequest.setWebsiteURL(fillUserInfoRequest.getWebsite());
                verificationDetailsRequest.setUser(user);

                verificationRepository.save(verificationDetailsRequest);

                apiResponse.setResponse(true);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setResponse(false);
                apiResponse.setCode(ResponseCodes.ALREADY_REJECTED.getValue());
            }

        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse acceptVerification(String address) {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            Role celebrity = roleRepository.findByName(UserRole.CELEBRITY.name());
            VerificationDetails verificationDetails = user.getVerificationDetails();
            verificationDetails.setUserVerificationStatus(UserVerificationStatus.VERIFIED);
            user.setVerificationDetails(verificationDetails);
            user.setUserVerificationStatus(UserVerificationStatus.VERIFIED);

            Set<Role> roles = new HashSet<>();
            roles.add(celebrity);
            user.setRoles(roles);
            userRepository.save(user);

            verificationRepository.save(verificationDetails);

            apiResponse.setResponse(
                modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getVerificationDetails(String address) {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            if (user.getVerificationDetails() != null) {
                apiResponse.setResponse(
                    modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setResponse(false);
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            }
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse rejectVerification(String address, String resultDescription) {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            VerificationDetails verificationDetails = user.getVerificationDetails();
            verificationDetails.setUserVerificationStatus(UserVerificationStatus.REJECTED);
            verificationDetails.setResultDescription(resultDescription);
            user.setUserVerificationStatus(UserVerificationStatus.REJECTED);

            user.setVerificationDetails(verificationDetails);
            userRepository.save(user);

            verificationRepository.save(verificationDetails);

            apiResponse.setResponse(
                modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse reviseVerification(String address, String resultDescription) {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            VerificationDetails verificationDetails = user.getVerificationDetails();
            verificationDetails.setUserVerificationStatus(UserVerificationStatus.REVISION_NEEDED);
            verificationDetails.setResultDescription(resultDescription);
            user.setUserVerificationStatus(UserVerificationStatus.REVISION_NEEDED);

            user.setVerificationDetails(verificationDetails);
            userRepository.save(user);

            verificationRepository.save(verificationDetails);

            apiResponse.setResponse(
                modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getVerificationStatus(String address) {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            apiResponse.setResponse(
                modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse cancelVerification() {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        VerificationDetails verificationDetails = user.getVerificationDetails();
        if (user != null) {
            if (verificationDetails != null
                && verificationDetails.getUserVerificationStatus()
                != UserVerificationStatus.REJECTED) {
                verificationDetails.setUserVerificationStatus(UserVerificationStatus.STALE);
                user.setVerificationDetails(verificationDetails);
                user.setUserVerificationStatus(UserVerificationStatus.STALE);
                userRepository.save(user);

                apiResponse.setResponse(true);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

            } else {
                apiResponse.setResponse(false);
                apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            }
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getVerificationRequests(UserVerificationStatus userVerificationStatus,
        int page) {
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage);
        List<VerificationDetails> verificationRequests =
            verificationRepository.findAllByUserVerificationStatus(userVerificationStatus,
                pageable);

        List<VerificationResponse> verificationResponses = verificationRequests
            .stream()
            .map(verificationDetails -> modelMapper.map(verificationDetails,
                VerificationResponse.class))
            .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(verificationResponses);

        return apiResponse;
    }

    @Override
    public ApiResponse getVerificationDetailsByEmail(String email) {
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            if (user.getVerificationDetails() != null) {
                apiResponse.setResponse(
                    modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            } else {
                apiResponse.setResponse(false);
                apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
            }
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getVerificationStatus() {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            // TODO: Why is this here? Wtf is this .......... >:c
            // apiResponse.setResponse(
            //     modelMapper.map(user.getVerificationDetails(), VerificationResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }
}
