package com.autograph.userservice.api.controller.user;

import com.autograph.userservice.api.dto.user.request.*;
import com.autograph.userservice.api.dto.user.request.authmodel.FacebookAuthModel;
import com.autograph.userservice.api.dto.user.request.authmodel.GoogleAuthModel;
import com.autograph.userservice.api.dto.user.request.authmodel.TwitterAuthModel;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.user.IUserService;
import com.autograph.userservice.api.service.user.impl.CustomUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Principal;

@CrossOrigin
@RestController
@RequestMapping("/api/user/auth")
public class UserAuthController {
    Logger logger = LoggerFactory.getLogger(getClass());



    @Autowired
    private CustomUserDetailsService userService;
    @Autowired
    private IUserService userDetailsService;


    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public Principal getUser(Principal principal) {
        userDetailsService.getUserInfoForPrinciple();
        return principal;
    }

    @GetMapping("/parseHeader")
    public ApiResponse parseHeader(){
        return userService.parseHeader();
    }

    @PostMapping("/signin")
    public ApiResponse signin(@RequestBody LoginRequest loginRequest) {
        return userService.login(loginRequest);
    }

    @PostMapping("/signin/wallet")
    public ApiResponse signinWithWallet(@RequestBody WalletLoginRequest loginRequest) {
        return userService.loginWithWallet(loginRequest.getAddress(), loginRequest.getSignature());
    }

    @PostMapping("/signin/facebook")
    public ApiResponse facebook(@RequestBody @Valid FacebookAuthModel facebookAuthModel) {
        return userService.facebook(facebookAuthModel);
    }

    @PostMapping("/signin/twitter")
    public ApiResponse twitter(@RequestBody @Valid TwitterAuthModel twitterAuthModel) {
        return userService.twitter(twitterAuthModel);
    }

    @PostMapping("/signin/google")
    public ApiResponse google(@RequestBody @Valid GoogleAuthModel googleAuthModel) {
        try {
            return userService.google(googleAuthModel);
        } catch (IOException | GeneralSecurityException e) {
            ApiResponse apiResponse = new ApiResponse(ResponseCodes.USER_NOT_FOUND.getValue(), false);
            e.printStackTrace();
            return apiResponse;
        }
    }

    @PostMapping("/signin/wallet/walletLoginTokenRequest")
    public ApiResponse requestWalletLoginToken(@RequestBody WalletLoginTokenRequest tokenRequest) {
        return userService.requestWalletLoginToken(tokenRequest.getAddress());
    }

    @PostMapping("/signup")
    public ApiResponse signup(@RequestBody UserRequest userRequest) {
        return userService.createUser(userRequest);
    }

    @PostMapping("/signup/wallet")
    public ApiResponse signupWithWallet(@RequestBody UserWalletRequest userWalletRequest) {
        return userService.createUserByAddress(userWalletRequest);
    }

    @PostMapping("/signup/wallet/verify")
    public ApiResponse verifySignupWalletRequest(@RequestBody WalletLoginRequest walletLoginRequest) {
        return userService.verifyRegisteredWalletAccount(walletLoginRequest);
    }
}