package com.autograph.userservice.api.dto.user.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationSetting {
    private Boolean likeNotification, bidNotification, bidAcceptedNotification, bidRejectedNotification, followNotification;
}
