package com.autograph.userservice.api.enums;

public enum UserRole {
    ADMIN,
    MODERATOR,
    CELEBRITY,
    CUSTOMER
}
