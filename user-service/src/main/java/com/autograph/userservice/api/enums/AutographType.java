package com.autograph.userservice.api.enums;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
