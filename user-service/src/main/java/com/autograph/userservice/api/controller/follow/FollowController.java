package com.autograph.userservice.api.controller.follow;

import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.follow.impl.FollowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class FollowController {
    Logger logger = LoggerFactory.getLogger(getClass());

    FollowService followService;

    @Autowired
    FollowController(FollowService followService) {
        this.followService = followService;
    }

    @PostMapping("/follow/{address}/")
    public ApiResponse followUser(@PathVariable("address") String address,
                                  @RequestHeader("Authorization") String authorization) {
        return followService.followUser(address, authorization);
    }

    @PostMapping("/mute/{address}/")
    public ApiResponse muteChange(@PathVariable("address") String address) {
        return followService.muteUserChange(address);
    }

    @PostMapping("/notify/{address}/")
    public ApiResponse notifyChange(@PathVariable("address") String address) {
        return followService.notifyUserAutographsChange(address);
    }

    @GetMapping("/follow/{address}/")
    public ApiResponse getFollowDetails(@PathVariable("address") String address) {
        return followService.getFollowInfo(address);
    }

    @GetMapping("/follow/{address}/followed")
    public ApiResponse isFollowedBy(@PathVariable("address") String address) {
        return followService.isFollowedByUser(address);
    }

    @GetMapping("/follow/{address}/following")
    public ApiResponse isFollowing(@PathVariable("address") String address) {
        return followService.isFollowingUser(address);
    }

    @GetMapping("/follow/{address}/followers/{page}")
    public ApiResponse getUserFollowers(@PathVariable("address") String address,
        @PathVariable("page") int page) {
        return followService.getUserFollowers(address, page);
    }

    @GetMapping("/follow/{address}/followings/{page}")
    public ApiResponse getUserFollowings(@PathVariable("address") String address,
        @PathVariable("page") int page) {
        return followService.getUserFollowingUsers(address, page);
    }

    @GetMapping("/follow/followers/{page}")
    public ApiResponse getFollowers(@PathVariable("page") int page) {
        return followService.getFollowers(page);
    }

    @GetMapping("/follow/followers")
    public ApiResponse getFollowers(@RequestHeader("id") Long userId) {
        return followService.getUserFollowers(userId);
    }

    @GetMapping("/follow/following/{page}")
    public ApiResponse getFollowings(@PathVariable("page") int page) {
        return followService.getFollowings(page);
    }

}
