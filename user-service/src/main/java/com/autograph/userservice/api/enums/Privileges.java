package com.autograph.userservice.api.enums;

public enum Privileges {
    DELETE,
    EDIT,
    CREATE,
    VIEW
}
