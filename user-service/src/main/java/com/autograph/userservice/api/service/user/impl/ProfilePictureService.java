package com.autograph.userservice.api.service.user.impl;

import com.autograph.userservice.api.config.FileStorageProperties;
import com.autograph.userservice.api.dto.user.response.ProfilePictureResponseDTO;
import com.autograph.userservice.api.entity.user.ProfilePicture;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.exception.AttachmentStorageException;
import com.autograph.userservice.api.repository.user.IProfilePictureRepository;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.user.IProfilePictureService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProfilePictureService implements IProfilePictureService {
    final ModelMapper modelMapper;
    final IProfilePictureRepository profilePictureRepository;
    private final Path fileStorageLocation;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public ProfilePictureService(FileStorageProperties fileStorageProperties,
                                 ModelMapper modelMapper, IProfilePictureRepository profilePictureRepository) {

        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
            .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new AttachmentStorageException(
                "Could not create the directory where the uploaded files will be stored.", ex);
        }

        this.modelMapper = modelMapper;
        this.profilePictureRepository = profilePictureRepository;
    }

    @Override
    public ProfilePicture uploadProfilePicture(MultipartFile file, Long userId) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new AttachmentStorageException(
                    "Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExtension = getExtensionByStringHandling(fileName).get();

            Path targetLocationWithUserId = this.fileStorageLocation.resolve(Long
                .toString(userId))
                .resolve("profile-picture");


            if(!Files.exists(targetLocationWithUserId)){
                Files.createDirectories(targetLocationWithUserId);
            }

            int fileCount = Objects
                .requireNonNull(new File(targetLocationWithUserId.toString()).list()).length + 1;
            String newFileName = fileCount +"." +fileExtension;


            Path targetLocation = targetLocationWithUserId.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

            return new ProfilePicture(newFileName,
                this.fileStorageLocation.toString());

        } catch (IOException ex) {
            throw new AttachmentStorageException(
                "Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public ApiResponse listAllProfilePictures() {
        ApiResponse apiResponse = new ApiResponse();
        List<ProfilePicture> profilePictures = profilePictureRepository.findAll();
        if(!profilePictures.isEmpty()){
            List<ProfilePictureResponseDTO> profilePictureResponseDTOS = profilePictures
                .stream()
                .map(profilePicture -> modelMapper.map(profilePicture, ProfilePictureResponseDTO.class))
                .collect(Collectors.toList());

            apiResponse.setResponse(profilePictureResponseDTOS);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getProfilePictureById(Long id) {
        ApiResponse apiResponse = new ApiResponse();
        Optional<ProfilePicture> profilePictureOptional = profilePictureRepository.findById(id);

        if(profilePictureOptional.isPresent()){
            ProfilePicture profilePicture = profilePictureOptional.get();

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(modelMapper.map(profilePicture, ProfilePictureResponseDTO.class));
        }else {
            apiResponse.setCode(ResponseCodes.NOT_FOUND.getValue());
        }
        return null;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
            .filter(f -> f.contains("."))
            .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
