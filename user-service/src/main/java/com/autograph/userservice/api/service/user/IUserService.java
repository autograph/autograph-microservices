package com.autograph.userservice.api.service.user;

import com.autograph.userservice.api.dto.user.request.*;
import com.autograph.userservice.api.dto.user.response.MyUserResponse;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.response.ApiResponse;
import org.springframework.core.io.Resource;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface IUserService {

    ApiResponse findUserByEmail(String email) ;

    ApiResponse getUserHeaderById(Long id) ;

    ApiResponse getUsersByRole(UserRole role);

    ApiResponse getUsersByBlockedStatus(Boolean status);

    ApiResponse getAllUsers();

    ApiResponse getUsersByStatus(UserVerificationStatus status);

    ApiResponse findUserByAddress(String address) ;

    ApiResponse fillAccountData(FillUserInfoRequest userInfoRequest) ;

    ApiResponse removeUserAccount() ;

    ApiResponse removeUserAccount(String address) ;

    ApiResponse getLikedAutographs(String address, String authorization);

    ApiResponse getLikedAutographs(String authorization) ;

    ApiResponse likeAutograph(Long id) ;

    ApiResponse getPreviousAutographs(String address, String authorization);

    ApiResponse setProfilePicture(ProfilePictureRequestDTO profilePictureRequestDTO) ;

    ApiResponse getRoles() ;

    ApiResponse requestVerificationToken(String address) ;

    ApiResponse checkVerificationToken(String signature) ;

    ApiResponse getUserHeaders(List<Long> ids) ;

    ApiResponse addAutograph(Long autographId);

    ApiResponse search(String query);

    ApiResponse getAutographsCreatedByUserAddress(String address, int page, String authorization);

    ApiResponse getAutographsOwnedByUserAddress(String address, int page, String authorization);

    Resource getProfilePicture() throws IOException;

    ApiResponse bioChangeRequest(UserBioChangeRequest userBioChangeRequest) ;

    ApiResponse findUser() ;

    public MyUserResponse getUserInfoForPrinciple();

    ApiResponse blockUserByEmail(String email);

    ApiResponse searchPush(String query);

    ApiResponse getSearchQueries();

    ApiResponse getUserHeadersByIdList(List<Long> ids);

    ApiResponse setFirebaseNotificationToken(String authorization, String firebaseToken);

    ApiResponse getFirebaseNotificationToken(String authorization, Long userId);

    ApiResponse setNotificationSettings(NotificationSetting notificationSetting, String authorization);

    ApiResponse getNotificationSettings(String authorization);

    ApiResponse checkUsernameAvailability(String username, String email);

    ApiResponse informUserAboutImportedAutograph(String email, Long autographId, String authorization) throws MessagingException, IOException;

    ApiResponse forgotPasswordRequest(ForgotPasswordRequest request);

    ApiResponse updatePasswordRequest(UpdatePasswordTokenRequest request);

    ApiResponse getUsersInfo();

    ApiResponse removeFirebaseNotificationToken(String authorization);
}
