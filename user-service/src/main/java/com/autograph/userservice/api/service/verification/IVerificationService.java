package com.autograph.userservice.api.service.verification;


import com.autograph.userservice.api.dto.user.request.FillUserInfoRequest;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.response.ApiResponse;

public interface IVerificationService {

    ApiResponse requestVerification(FillUserInfoRequest fillUserInfoRequest);

    ApiResponse acceptVerification(String address);

    ApiResponse getVerificationDetails(String address);

    ApiResponse rejectVerification(String address, String resultDescription);

    ApiResponse reviseVerification(String address, String resultDescription);

    ApiResponse getVerificationStatus(String address);

    ApiResponse cancelVerification();

    ApiResponse getVerificationRequests(UserVerificationStatus userVerificationStatus, int page);

    ApiResponse getVerificationDetailsByEmail(String email);

    ApiResponse getVerificationStatus();


}
