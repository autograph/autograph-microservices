package com.autograph.userservice.api.enums;

public enum UserVerificationStatus {
    VERIFIED,
    REJECTED,
    PENDING,
    REVISION_NEEDED,
    STALE
}
