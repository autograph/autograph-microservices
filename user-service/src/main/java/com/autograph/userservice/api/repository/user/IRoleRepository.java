package com.autograph.userservice.api.repository.user;

import com.autograph.userservice.api.entity.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}