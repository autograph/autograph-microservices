package com.autograph.userservice.api.controller.user;

import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.user.impl.UserService;
import com.autograph.userservice.api.service.verification.impl.VerificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/user/admin")
public class AdminController {
    private final UserService userService;
    private final VerificationService verificationService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public AdminController(UserService userService, VerificationService verificationService) {
        this.userService = userService;
        this.verificationService = verificationService;
    }

    @GetMapping("/{address}")
    public ApiResponse getUserByAddress(@PathVariable("address") String address) throws Exception {
        return userService.findUserByAddress(address);
    }

    @GetMapping("/blocked/{status}")
    public ApiResponse getUsersByBlockedStatus(@PathVariable("status") Boolean status) throws Exception {
        return userService.getUsersByBlockedStatus(status);
    }


    @GetMapping("/{address}/liked")
    public ApiResponse getLikedAutographs(@PathVariable("address") String address)
            throws Exception {
        return userService.getLikedAutographs(address);
    }

    @DeleteMapping("/{address}")
    public ApiResponse deleteAccount(@PathVariable("address") String address) throws Exception {
        return userService.removeUserAccount(address);
    }

    @GetMapping("/{address}/verification")
    public ApiResponse getVerificationInfo(@PathVariable("address") String address)
            throws Exception {
        return verificationService.getVerificationDetails(address);
    }

    @GetMapping("/email/{email}/verification")
    public ApiResponse getVerificationInfoByEmail(@PathVariable("email") String email)
            throws Exception {
        return verificationService.getVerificationDetailsByEmail(email);
    }

    @GetMapping("/verifications/{page}")
    public ApiResponse getVerificationRequests(@RequestParam("status") UserVerificationStatus userVerificationStatus,
                                               @PathVariable("page") int page)
            throws Exception {
        return verificationService.getVerificationRequests(userVerificationStatus, page);
    }

    @PostMapping("/{address}/verification")
    public ApiResponse acceptVerification(@PathVariable("address") String address)
            throws Exception {
        return verificationService.acceptVerification(address);
    }

    @PostMapping("/email/{email}/block")
    public ApiResponse blockUserByEmail(@PathVariable("email") String email)
            throws Exception {
        return userService.blockUserByEmail(email);
    }

    @DeleteMapping("/{address}/verification")
    public ApiResponse rejectVerification(@PathVariable("address") String address,
                                          @RequestBody String reason) throws Exception {
        return verificationService.rejectVerification(address, reason);
    }

    @PutMapping("/{address}/verification")
    public ApiResponse editVerification(@PathVariable("address") String address,
                                        @RequestBody String reason) throws Exception {
        return verificationService.reviseVerification(address, reason);
    }

}
