package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class UpdatePasswordTokenRequest {
    String token;
    String email;
    String password;
}
