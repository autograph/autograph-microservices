package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.enums.UserVerificationStatus;
import lombok.Data;

@Data
public class UserHeaderResponseDTO {

    private Long id;
    private String email;
    private String username;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private String address;
    private String name, surname, bio;
    private UserVerificationStatus userVerificationStatus;
    private Boolean isBlocked;
    private Long following, followed;
    private Long autographCount;
}
