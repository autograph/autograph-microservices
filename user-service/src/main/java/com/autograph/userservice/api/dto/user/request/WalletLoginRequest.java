package com.autograph.userservice.api.dto.user.request;

import lombok.Data;

@Data
public class WalletLoginRequest {

    String address;
    String signature;
}
