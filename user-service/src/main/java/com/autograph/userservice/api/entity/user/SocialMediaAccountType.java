package com.autograph.userservice.api.entity.user;

public enum SocialMediaAccountType {
    FACEBOOK,
    INSTAGRAM,
    GOOGLE,
    SKYPE,
    WHATSAPP,
    ZOOM

}
