package com.autograph.userservice.api.dto.user.response;

import lombok.Data;

@Data
public class ProfilePictureResponseDTO {

    private Long id;
    private String fileName;
    private String uploadDirectory;
}
