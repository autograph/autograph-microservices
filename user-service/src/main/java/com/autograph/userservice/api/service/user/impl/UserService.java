package com.autograph.userservice.api.service.user.impl;

import com.autograph.userservice.api.config.auth.IAuthenticationFacade;
import com.autograph.userservice.api.dto.user.request.*;
import com.autograph.userservice.api.dto.user.response.*;
import com.autograph.userservice.api.entity.user.ProfilePicture;
import com.autograph.userservice.api.entity.user.Role;
import com.autograph.userservice.api.entity.user.User;
import com.autograph.userservice.api.enums.ResponseCodes;
import com.autograph.userservice.api.enums.UserRole;
import com.autograph.userservice.api.enums.UserVerificationStatus;
import com.autograph.userservice.api.repository.user.IRoleRepository;
import com.autograph.userservice.api.repository.user.IUserRepository;
import com.autograph.userservice.api.response.ApiResponse;
import com.autograph.userservice.api.service.user.IUserService;
import com.autograph.userservice.api.utils.Utils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    private final IUserRepository userRepository;
    private final ModelMapper modelMapper;
    private final IAuthenticationFacade authenticationFacade;
    private final ApiResponse apiResponse;
    private final ProfilePictureService profilePictureService;
    private final WebClient.Builder webClientBuilder;
    private final CircuitBreakerFactory cbFactory;
    private final IRoleRepository roleRepository;
    private final EmailSenderService emailSenderService;
    private final PasswordEncoder bCryptPasswordEncoder;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public UserService(IUserRepository userRepository, ModelMapper modelMapper, WebClient.Builder webClientBuilder,
                       IAuthenticationFacade authenticationFacade, ApiResponse apiResponse, ProfilePictureService profilePictureService, CircuitBreakerFactory cbFactory, IRoleRepository roleRepository, EmailSenderService emailSenderService, PasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.authenticationFacade = authenticationFacade;
        this.apiResponse = apiResponse;
        this.profilePictureService = profilePictureService;
        this.webClientBuilder = webClientBuilder;
        this.cbFactory = cbFactory;
        this.roleRepository = roleRepository;
        this.emailSenderService = emailSenderService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public ApiResponse findUserByEmail(String email)  {
        User user = userRepository.findUserByEmail(email.toLowerCase(Locale.ROOT));

        if (user != null) {
            MyUserResponse userResponse = modelMapper.map(user, MyUserResponse.class);
            userResponse.setAutographCount(getUserAutographsCount(user));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userResponse);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUserHeaderById(Long id)  {
        User user = userRepository.findUserById(id);

        if (user != null) {
            UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
            userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userHeaderResponseDTO);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }


    @Override
    public ApiResponse getUsersByRole(UserRole role)  {
        Role roleOptional = roleRepository.findByName(role.name());

        List<User> users = userRepository.findAllByRolesContains(roleOptional);

        if (users != null) {
            List<UserHeaderResponseDTO> userHeaderResponseDTOs = users.stream()
                    .map(user ->{
                    UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                    userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                    return userHeaderResponseDTO;
                    })
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userHeaderResponseDTOs);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUsersByBlockedStatus(Boolean status)  {
        List<User> users = userRepository.findAllByIsBlocked(status);

        if (users != null) {
            List<UserHeaderResponseDTO> userHeaderResponseDTOs = users.stream()
                    .map(user ->{
                        UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                        userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                        return userHeaderResponseDTO;
                    })
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userHeaderResponseDTOs);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getAllUsers()  {
        List<User> users = userRepository.findAll();

        if (users != null) {
            List<UserHeaderResponseDTO> userHeaderResponseDTOs = users.stream()
                    .map(user ->{
                        UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                        userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                        return userHeaderResponseDTO;
                    })
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userHeaderResponseDTOs);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUsersByStatus(UserVerificationStatus status){

        List<User> users = userRepository.findAllByUserVerificationStatus(status);

        if (users != null) {
            List<UserHeaderResponseDTO> userHeaderResponseDTOs = users.stream()
                    .map(user ->{
                        UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                        userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                        return userHeaderResponseDTO;
                    })
                    .collect(Collectors.toList());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(userHeaderResponseDTOs);
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse findUserByAddress(String address)  {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);

            myUserResponse.setAutographCount(getUserAutographsCount(user));
            apiResponse.setResponse(myUserResponse);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse fillAccountData(FillUserInfoRequest userInfoRequest)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            user.setEmail(userInfoRequest.getEmail());
            user.setName(userInfoRequest.getName());
            user.setSurname(userInfoRequest.getSurname());
            user.setAbout(userInfoRequest.getDetails());
            user.setUsername(userInfoRequest.getUsername());
            user.setWebsiteURL(userInfoRequest.getWebsite());
            user.setLastModifiedDate(LocalDateTime.now());
            user.setBio(userInfoRequest.getDetails());

            userRepository.save(user);

            MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);
            apiResponse.setResponse(myUserResponse);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse removeUserAccount()  {

        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            userRepository.delete(user);

            apiResponse.setResponse(modelMapper.map(user, UserResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse removeUserAccount(String address)  {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            userRepository.delete(user);

            apiResponse.setResponse(modelMapper.map(user, UserResponse.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getLikedAutographs(String address, String authorization)  {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            Set<Long> autographLikes = user.getLikedAutographs();

            ApiResponse response = cbFactory.create("get-list-of-autograph-dtos")
                    .run(()->getListOfAutographDTOs(autographLikes, authorization),
                            throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(), autographLikes));

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            assert response != null;
            apiResponse.setResponse(response.getResponse());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getLikedAutographs( String authorization)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            Set<Long> autographLikes = user.getLikedAutographs();

            ApiResponse response = webClientBuilder.build()
                    .post()
                    .uri("http://autograph-service/api/autograph/list")
                    .bodyValue(autographLikes.toArray())
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .header("Authorization",  authorization)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(response.getResponse());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse likeAutograph(Long id)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            Set<Long> likedAutographs = user.getLikedAutographs();

            if(likedAutographs.contains(id)){
                likedAutographs.remove(id);
                apiResponse.setResponse(false);

            }else {
                likedAutographs.add(id);
                apiResponse.setResponse(true);
            }
            user.setLikedAutographs(likedAutographs);
            userRepository.save(user);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getPreviousAutographs(String address,  String authorization)  {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            Set<Long> previousAutographs = user.getPreviousAutographs();

            ApiResponse response = webClientBuilder.build()
                    .post()
                    .uri("http://autograph-service/api/autograph/list")
                    .bodyValue(previousAutographs.toArray())
                    .header("Authorization",  authorization)
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

            return response;

        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(new ArrayList<>());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse setProfilePicture(ProfilePictureRequestDTO profilePictureRequest)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            ProfilePicture profilePicture =
                profilePictureService.uploadProfilePicture(profilePictureRequest.getFile(), user.getId());

            user.setProfilePicture(profilePicture);
            userRepository.save(user);

            apiResponse.setResponse(modelMapper.map(profilePicture, ProfilePictureResponseDTO.class));
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getRoles()  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            apiResponse.setResponse(user.getRoles());
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse requestVerificationToken(String address)  {

            Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            String generatedString = Utils.generateWalletToken();

            user.setAddress(address);
            user.setVerificationString(generatedString);
            user.setAddressVerified(false);

            userRepository.save(user);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(generatedString);
        }
        else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse checkVerificationToken(String signature)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            boolean verified = Utils.isSignatureValid(user.getAddress(),
                signature, user.getVerificationString());

            if(verified){
                user.setAddressVerified(true);
                userRepository.save(user);
            }
            apiResponse.setResponse(verified);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }
        else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getUserHeaders(List<Long> ids) {
        List<UserHeaderResponseDTO> usersList = ids
                .stream()
                .map(id ->{
                    User userData  = userRepository.findUserById(id);
                    UserHeaderResponseDTO userHeaderResponseDTO =  modelMapper.map(userData, UserHeaderResponseDTO.class);
                    userHeaderResponseDTO.setAutographCount((long) userData.getAutographs().size());
                    return userHeaderResponseDTO;
                })
                .collect(Collectors.toList());

        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        apiResponse.setResponse(usersList);
        return apiResponse;
    }

    @Override
    public ApiResponse addAutograph( Long autographId)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            List<Long> ownedAutographs = user.getAutographs();
            Set<Long> previouslyOwnedAutographs = user.getPreviousAutographs();
            List<Long> createdAutographs = user.getCreatedAutographs();

            ownedAutographs.add(autographId);
            previouslyOwnedAutographs.add(autographId);
            createdAutographs.add(autographId);

            user.setAutographs(ownedAutographs);
            user.setPreviousAutographs(previouslyOwnedAutographs);
            user.setCreatedAutographs(createdAutographs);

            userRepository.save(user);

            apiResponse.setResponse(true);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse search(String query) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();

        List<User> users = userRepository.findAllByAddressContainingOrUsernameContaining(query.toLowerCase(Locale.ROOT), query.toLowerCase(Locale.ROOT));

        List<UserHeaderResponseDTO> usersDTO = users.stream()
                .filter(user -> !user.getEmail().equals(auth.getName()))
                .map(user ->{
                    UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                    userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                    return userHeaderResponseDTO;
                })
                .collect(Collectors.toList());

        apiResponse.setResponse(usersDTO);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    @Override
    public ApiResponse getAutographsCreatedByUserAddress(String address, int page, String authorization)  {
        User user = userRepository.findUserByAddress(address);
        if(user != null) {
            Long userId = user.getId();
            ApiResponse response = webClientBuilder.build()
                    .get()
                    .uri("http://autograph-service/api/autograph/created/"+page)
                    .header("id", String.valueOf(userId))
                    .header("Authorization",  authorization)
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

            apiResponse.setCode(response.getCode());
            apiResponse.setResponse(response.getResponse());
        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(new ArrayList<>());
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getAutographsOwnedByUserAddress(String address, int page,  String authorization)  {
        User user = userRepository.findUserByAddress(address);

        if (user != null) {
            Collection<Long> autographs = user.getAutographs();

            Long userId = user.getId();
            ApiResponse response = webClientBuilder.build()
                    .get()
                    .uri("http://autograph-service/api/autograph/owned/user/"+ userId +"/"+page)
                    .header("id", String.valueOf(userId))
                    .header("Authorization",  authorization)
                    .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                    .acceptCharset(StandardCharsets.UTF_8)
                    .ifNoneMatch("*")
                    .ifModifiedSince(ZonedDateTime.now())
                    .retrieve()
                    .bodyToMono(ApiResponse.class)
                    .block();

            apiResponse.setCode(response.getCode());
            apiResponse.setResponse(response.getResponse());

        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(new ArrayList<>());
        }
        return apiResponse;
    }

    @Override
    public Resource getProfilePicture() throws IOException {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        ProfilePicture profilePicture = user.getProfilePicture();

        Path path = Paths.get(profilePicture.getUploadDirectory());
        Path filePath = path
            .resolve(user.getId().toString())
            .resolve("profile-picture")
            .resolve(profilePicture.getFileName());
        byte[] imageBytes = Files.readAllBytes(filePath);

        return new ByteArrayResource(imageBytes);
    }

    @Override
    public ApiResponse bioChangeRequest(UserBioChangeRequest userBioChangeRequest)  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName().toLowerCase(Locale.ROOT));

        if (user != null) {
            user.setUsername(userBioChangeRequest.getUsername().toLowerCase(Locale.ROOT));
            user.setBio(userBioChangeRequest.getBio());

            userRepository.save(user);

            MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);
            apiResponse.setResponse(myUserResponse);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse findUser()  {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if (user != null) {
            MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);
            myUserResponse.setAutographCount(getUserAutographsCount(user));
            apiResponse.setResponse(myUserResponse);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    public MyUserResponse getUserInfoForPrinciple(){
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        MyUserResponse myUserResponse = modelMapper.map(user, MyUserResponse.class);
        myUserResponse.setAutographCount(getUserAutographsCount(user));

        return myUserResponse;
    }

    @Override
    public ApiResponse blockUserByEmail(String email) {
        User user = userRepository.findUserByEmail(email.toLowerCase(Locale.ROOT));

        if (user != null) {
            user.setIsBlocked(!user.getIsBlocked());
            userRepository.save(user);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(user.getIsBlocked());
        } else {
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse searchPush(String query) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if(user != null) {
            List<String> previousSearchQueries = user.getSearchQueries();
            previousSearchQueries.add(query);

            user.setSearchQueries(previousSearchQueries);

            userRepository.save(user);
            apiResponse.setResponse(true);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getSearchQueries() {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if(user != null) {
            List<String> previousSearchQueries = user.getSearchQueries();

            apiResponse.setResponse(previousSearchQueries);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(new ArrayList<>());
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    //    @Override
//    public ApiResponse getUserActivity(int page)  {
//        
//        Pageable pageable = PageRequest
//            .of(page, numberOfItemsOnPage, Sort.by("createdAt", "desc"));
//
//        Authentication auth = authenticationFacade.getAuthenticationFacade();
//        User user = userRepository.findUserByEmail(auth.getName());
//
//        if (user != null) {
//            List<ActivityMessage> activity = user.getActivityMessages();
//            List<ActivityResponse> activityResponses = activity
//                .stream()
//                .map(activityMessage -> activityMapper(activityMessage))
//                .collect(Collectors.toList());
//
//            Page<ActivityResponse> pageResult =
//                new PageImpl<>(activityResponses, pageable, activityResponses.size());
//
//            apiResponse.setResponse(pageResult.get());
//            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
//        } else {
//            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
//        }
//        return apiResponse;
//    }
//
//    @Override
//    public ApiResponse viewActivity(Long id)  {
//        
//
//        Authentication auth = authenticationFacade.getAuthenticationFacade();
//        User user = userRepository.findUserByEmail(auth.getName());
//
//        if (user != null) {
//            Optional<ActivityMessage> activityMessageOptional = activityRepository.findById(id);
//
//            if(activityMessageOptional.isPresent()){
//                ActivityMessage activityMessage = activityMessageOptional.get();
//                activityMessage.setNew(false);
//
//            activityRepository.save(activityMessage);
//            }else{
//                apiResponse.setCode(ResponseCodes.ACTIVITY_NOT_FOUND.getValue());
//            }
//        }
//        else{
//            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
//        }
//
//        return apiResponse;
//    }

//    public ActivityResponse activityMapper(ActivityMessage activityMessage){
//        ActivityResponse activityResponse = new ActivityResponse();
//        User invoker = userRepository.findUserById(activityMessage.getUserId());
//        UserResponse invokerDTO = modelMapper.map(invoker, UserResponse.class);
//        Autograph autograph = null;
//        AutographResponseDTO autographResponseDTO = null;
//        BidActivityResponseDTO bidActivityResponseDTO = null;
//
//        if(activityMessage.getAutographId() != null){
//            autograph = autographRepository.findById(activityMessage.getAutographId()).get();
//            autographResponseDTO = modelMapper.map(autograph, AutographResponseDTO.class);
//        }
//
//        if(activityMessage.getBidId() != null){
//            bidActivityResponseDTO =
//                new BidActivityResponseDTO(activityMessage.getId(), activityMessage.getUserId(),
//                    autographResponseDTO,invokerDTO, activityMessage.getBidId(), activityMessage.getPrice());
//        }
//
//        switch(activityMessage.getActivityType()){
//            case FOLLOW:
//                FollowActivityResponseDTO
//                ActivityResponseDTO =
//                    new FollowActivityResponseDTO(activityMessage.getId(), activityMessage.getUserId(),
//                        invokerDTO);
//                activityResponse.setCode(ActivityType.FOLLOW.getValue());
//                activityResponse.setResponse(followActivityResponseDTO);
//                break;
//            case LIKE:
//                LikeActivityResponseDTO likeActivityResponseDTO =
//                    new LikeActivityResponseDTO(activityMessage.getId(), activityMessage.getUserId(),
//                        autographResponseDTO, invokerDTO);
//
//                activityResponse.setCode(ActivityType.LIKE.getValue());
//                activityResponse.setResponse(likeActivityResponseDTO);
//                break;
//            case RELEASE_NOTES:
//                activityResponse.setCode(ActivityType.RELEASE_NOTES.getValue());
//                activityResponse.setResponse(activityMessage.getMessage());
//                break;
//            case BID:
//                activityResponse.setCode(ActivityType.BID.getValue());
//                activityResponse.setResponse(bidActivityResponseDTO);
//                break;
//            case BID_ACCEPTED:
//                activityResponse.setCode(ActivityType.BID_ACCEPTED.getValue());
//                activityResponse.setResponse(bidActivityResponseDTO);
//                break;
//            case BID_REJECTED:
//                activityResponse.setCode(ActivityType.BID_REJECTED.getValue());
//                activityResponse.setResponse(bidActivityResponseDTO);
//                break;
//        }
//
//        return activityResponse;
//    }

    @Override
    public ApiResponse getUserHeadersByIdList(List<Long> ids) {
        List<UserHeaderResponseDTO> headers = new ArrayList<>();
        for (Long id: ids){
            User user = userRepository.findUserById(id);
            if (user != null) {
                UserHeaderResponseDTO userHeaderResponseDTO = modelMapper.map(user, UserHeaderResponseDTO.class);
                userHeaderResponseDTO.setAutographCount(getUserAutographsCount(user));
                headers.add(userHeaderResponseDTO);
            }
        }
        if(headers.isEmpty()) {
            apiResponse.setResponse(headers);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }else{
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(headers);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse setFirebaseNotificationToken(String authorization, String firebaseToken) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if(user != null) {
            user.setFirebaseNotificationToken(firebaseToken);

            userRepository.save(user);

            apiResponse.setResponse(true);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getFirebaseNotificationToken(String authorization, Long userId) {
        User user = userRepository.findUserById(userId);

        if(user != null) {
            NotificationSettingResponseDTO notificationSetting = new NotificationSettingResponseDTO();

            notificationSetting.setLikeNotification(user.getLikeNotification());
            notificationSetting.setBidNotification(user.getBidNotification());
            notificationSetting.setFollowNotification(user.getFollowNotification());
            notificationSetting.setBidAcceptedNotification(user.getBidAcceptedNotification());
            notificationSetting.setBidRejectedNotification(user.getBidRejectedNotification());
            notificationSetting.setToken(user.getFirebaseNotificationToken());

            apiResponse.setResponse(notificationSetting);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse setNotificationSettings(NotificationSetting notificationSetting, String authorization) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if(user != null){
            user.setLikeNotification(notificationSetting.getLikeNotification());
            user.setBidNotification(notificationSetting.getBidNotification());
            user.setFollowNotification(notificationSetting.getFollowNotification());
            user.setBidAcceptedNotification(notificationSetting.getBidAcceptedNotification());
            user.setBidRejectedNotification(notificationSetting.getBidRejectedNotification());

            userRepository.save(user);

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(true);
        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(false);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getNotificationSettings(String authorization) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());


        if(user != null){
            NotificationSettingResponseDTO notificationSetting = new NotificationSettingResponseDTO();

            notificationSetting.setLikeNotification(user.getLikeNotification());
            notificationSetting.setBidNotification(user.getBidNotification());
            notificationSetting.setFollowNotification(user.getFollowNotification());
            notificationSetting.setBidAcceptedNotification(user.getBidAcceptedNotification());
            notificationSetting.setBidRejectedNotification(user.getBidRejectedNotification());
            notificationSetting.setToken(user.getFirebaseNotificationToken());

            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(notificationSetting);
        }else{
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            apiResponse.setResponse(null);
        }

        return apiResponse;
    }

    @Override
    public ApiResponse checkUsernameAvailability(String username, String email) {
        Boolean usernameExists = userRepository.existsByUsername(username);
        Boolean emailExists = userRepository.existsByEmail(email);

        HashMap<String, Boolean> response = new HashMap<>();
        response.put("username", usernameExists);
        response.put("email", emailExists);

        apiResponse.setResponse(response);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse informUserAboutImportedAutograph(String email, Long autographId, String authorization) throws MessagingException, IOException {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        String subject = "You were discovered in Autograph!";
        String link = "https://www.autographspace.com/share-link/autograph/" + autographId;
        String body = "Good day!\n\n\n User @" + user.getUsername() + " find out that " + link + " was created by you!\n Join us on Autograph!";
        emailSenderService.sendSimpleMessage(email, subject, body);

        apiResponse.setResponse(true);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse forgotPasswordRequest(ForgotPasswordRequest request) {
        User user = userRepository.findUserByEmail(request.getEmail());

        if (user != null) {
            String generatedString = Utils.generateForgotPasswordToken();

            user.setForgotPasswordToken(generatedString);

            String subject = "[Autograph] Please reset your password";
            String link = "https://www.autographspace.com/password-recovery?token=" + generatedString+"&mail="+user.getEmail();
            String body = "Autograph password reset\n\n " +"We heard that you lost your Autograph password. Sorry about that!\n" +
                    "\n" +
                    "But don’t worry! You can use the following button to reset your password: " + link + "\n " +
                    "If you don’t use this link within 3 hours, it will expire. \n" +
                    "Thanks,\n" +
                    "The AutographSpace Team";
            emailSenderService.sendSimpleMessage(request.getEmail(), subject, body);

            userRepository.save(user);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
            apiResponse.setResponse(true);
        }
        else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse updatePasswordRequest(UpdatePasswordTokenRequest request) {
        User user = userRepository.findUserByEmail(request.getEmail());

        if (user != null) {
            if(request.getToken() != null && request.getToken().equals(user.getForgotPasswordToken())) {
                user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
                user.setForgotPasswordToken(null);

                userRepository.save(user);
                apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
                apiResponse.setResponse(true);
            }else{
                apiResponse.setResponse(null);
                apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
            }
        }
        else{
            apiResponse.setResponse(null);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getUsersInfo() {
        Integer numberOfUsers = userRepository.countByUsernameIsNotNull();
        LocalDateTime current = LocalDateTime.now();
        Integer currentMonth = current.getMonthValue();
        Integer currentYear = current.getYear();

        HashMap<Integer, Integer> months = new HashMap<>();
        HashMap<Integer, Integer> years = new HashMap<>();

        for(int i = currentYear; i > currentYear - 5; i--){
            years.put(i, userRepository.countByCreatedDate(i).size());
        }

        for(int i = currentMonth; i > currentMonth - 5; i--){
            if(i < 1)
                months.put(12+i, userRepository.countByCreatedDateMonthAndCreatedDateYear(12+i, currentYear - 1).size());
            else
                months.put(i, userRepository.countByCreatedDateMonthAndCreatedDateYear(i, currentYear).size());
        }

        UsersInfoResponseDTO usersInfoResponseDTO = new UsersInfoResponseDTO(numberOfUsers, months,years);
        apiResponse.setResponse(usersInfoResponseDTO);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        return apiResponse;
    }

    @Override
    public ApiResponse removeFirebaseNotificationToken(String authorization) {
        Authentication auth = authenticationFacade.getAuthenticationFacade();
        User user = userRepository.findUserByEmail(auth.getName());

        if(user != null) {
            user.setFirebaseNotificationToken(null);

            userRepository.save(user);

            apiResponse.setResponse(true);
            apiResponse.setCode(ResponseCodes.SUCCESS.getValue());
        }else{
            apiResponse.setResponse(false);
            apiResponse.setCode(ResponseCodes.USER_NOT_FOUND.getValue());
        }

        return apiResponse;
    }

    private ApiResponse getListOfAutographDTOs(Set<Long> autographLikes,  String authorization) {
        return webClientBuilder.build()
                        .post()
                        .uri("http://autograph-service/api/autograph/list")
                        .header("Authorization",  authorization)
                        .bodyValue(autographLikes.toArray())
                        .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                        .acceptCharset(StandardCharsets.UTF_8)
                        .ifNoneMatch("*")
                        .ifModifiedSince(ZonedDateTime.now())
                        .retrieve()
                        .bodyToMono(ApiResponse.class)
                        .block();
    }

    Long getUserAutographsCount(User user){
        Long countAll = (long) user.getAutographs().size();

        ApiResponse countDeletedResponse = cbFactory.create("get-number-of-deleted-autographs-of-user")
                .run(()->getNumberOfDeletedAutographsOfUser(user.getId()),
                         throwable -> {
                            apiResponse.setResponse(0L);
                            return apiResponse;
                         });


        Long countDeleted = ((Integer) countDeletedResponse.getResponse()).longValue();
        return countAll - countDeleted;
    }

    private ApiResponse getNumberOfDeletedAutographsOfUser(Long userId) {
        return (ApiResponse) webClientBuilder.build()
                .get()
                .uri("http://autograph-service/api/autograph/deleted?id="+userId)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(ApiResponse.class)
                .block();
    }

}
