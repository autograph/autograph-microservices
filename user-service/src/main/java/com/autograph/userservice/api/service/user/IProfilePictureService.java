package com.autograph.userservice.api.service.user;

import com.autograph.userservice.api.entity.user.ProfilePicture;
import com.autograph.userservice.api.response.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IProfilePictureService {

    ProfilePicture uploadProfilePicture(MultipartFile file, Long userId);

    ApiResponse listAllProfilePictures();

    ApiResponse getProfilePictureById(Long id);
}
