package com.autograph.userservice.api.dto.user.response;

import com.autograph.userservice.api.enums.UserVerificationStatus;
import lombok.Data;

@Data
public class UserResponse {

    private Long id;
    private String email;
    private String name, surname;
    private String username;
    private String address;
    private String profilePictureFileName;
    private String profilePictureUploadDirectory;
    private Long profilePictureId;
    private UserVerificationStatus userVerificationStatus;
    private Boolean isBlocked;
    private Long autographCount;
}
