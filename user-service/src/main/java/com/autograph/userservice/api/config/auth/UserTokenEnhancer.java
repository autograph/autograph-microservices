package com.autograph.userservice.api.config.auth;

import com.autograph.userservice.api.dto.user.response.MyUserResponse;
import com.autograph.userservice.api.entity.user.PrincipalUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class UserTokenEnhancer implements TokenEnhancer {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        final Map<String, Object> additionalInfo = new HashMap<String, Object>();
        logger.info("User principal: " + ((PrincipalUser)oAuth2Authentication.getPrincipal()).getUsername());

        MyUserResponse user = ((PrincipalUser)oAuth2Authentication.getPrincipal()).getUserDetails();

        additionalInfo.put("details", user);

         ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionalInfo);

        return oAuth2AccessToken;
    }
}
