package com.autograph.userservice.api.entity.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class SocialMediaAccount {

    @Id
    @GeneratedValue
    private Long id;

    private SocialMediaAccountType socialMediaAccountType;

    private String accountDetails;

}
