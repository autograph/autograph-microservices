package com.autograph.feedservice.api.service.feed.impl;

import com.autograph.feedservice.api.ResponseCodes;
import com.autograph.feedservice.api.entity.feed.FeedItem;
import com.autograph.feedservice.api.repository.IFeedRepository;
import com.autograph.feedservice.api.response.ApiResponse;
import com.autograph.feedservice.api.service.feed.IFeedService;
import org.modelmapper.ModelMapper;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeedService implements IFeedService {

    private final ModelMapper modelMapper;
    private final IFeedRepository feedRepository;
    private final ApiResponse apiResponse;
    private final WebClient.Builder webClientBuilder;
    private final CircuitBreakerFactory<?, ?> cbFactory;

    public FeedService(ModelMapper modelMapper, IFeedRepository feedRepository,
        ApiResponse apiResponse, WebClient.Builder webClientBuilder,
        CircuitBreakerFactory cbFactory) {
        this.modelMapper = modelMapper;
        this.feedRepository = feedRepository;
        this.apiResponse = apiResponse;
        this.webClientBuilder = webClientBuilder;
        this.cbFactory = cbFactory;
    }

    @Override
    public ApiResponse getUserFeed(int page, Long userId, String authorization) {
        int numberOfItemsOnPage = 20;
        Pageable pageable = PageRequest.of(page, numberOfItemsOnPage,
            Sort.by(Sort.Direction.DESC, "createdAt"));

        List<FeedItem> feed = feedRepository.findAllByUserId(userId, pageable);
        List<Long> autographs = feed.stream().map(FeedItem::getAutographId)
            .collect(Collectors.toList());

        return cbFactory.create("get-list-of-autograph-dtos")
            .run(() -> getListOfAutographResponseDTOs(autographs, authorization),
                throwable -> new ApiResponse(ResponseCodes.SUCCESS.getValue(), autographs));
    }

    @Override
    public ApiResponse addFeedItem(List<Long> userIds, Long initiatorUserId, Long autographId) {
        for (Long userId : userIds) {
            FeedItem feedItem = new FeedItem(userId, autographId, initiatorUserId);
            feedRepository.save(feedItem);
        }

        apiResponse.setResponse(true);
        apiResponse.setCode(ResponseCodes.SUCCESS.getValue());

        return apiResponse;
    }

    private ApiResponse getListOfAutographResponseDTOs(List<Long> autographs, String authorization) {
        return webClientBuilder.build()
            .post()
            .uri("http://autograph-service/api/autograph/list")
                .header("Authorization", authorization)
            .bodyValue(autographs.toArray())
            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
            .acceptCharset(StandardCharsets.UTF_8)
            .ifNoneMatch("*")
            .ifModifiedSince(ZonedDateTime.now())
            .retrieve()
            .bodyToMono(ApiResponse.class)
            .block();
    }
}
