package com.autograph.feedservice.api.repository;

import com.autograph.feedservice.api.entity.feed.FeedItem;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFeedRepository extends JpaRepository<FeedItem, Long> {

    List<FeedItem> findAllByUserId(Long userId, Pageable pageable);
}
