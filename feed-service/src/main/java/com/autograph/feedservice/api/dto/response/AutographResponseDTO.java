package com.autograph.feedservice.api.dto.response;

import com.autograph.feedservice.api.AutographType;
import com.autograph.feedservice.api.PurchaseType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AutographResponseDTO {

    private Long id;
    private LocalDateTime createdAt;
    private String title;
    private String description;
    private String externalLink;
    private Long creator;
    private Long owner;
    private Long attachmentId;
    private String attachmentUploadDirectory;
    private String attachmentFileName;
    private AutographType autographType;
    private Long numberOfLikes;
    private PurchaseType purchaseType;
    private BigDecimal currentPrice;
    private Boolean isLiked, isDeleted;
    private Long auctionId;
}
