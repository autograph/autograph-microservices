package com.autograph.feedservice.api;

public enum AutographType {
    VIDEO,
    AUDIO,
    IMAGE,
    TWEET
}
