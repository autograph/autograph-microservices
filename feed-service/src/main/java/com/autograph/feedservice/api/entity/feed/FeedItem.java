package com.autograph.feedservice.api.entity.feed;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FeedItem {

    @Id
    @GeneratedValue
    private Long id;

    private Long userId;
    private Long autographId;
    private Long initiatorUserId;

    @CreationTimestamp
    private Timestamp createdAt;

    public FeedItem(Long userId, Long autographId, Long initiatorUserId) {
        this.userId = userId;
        this.autographId = autographId;
        this.initiatorUserId = initiatorUserId;
    }
}
