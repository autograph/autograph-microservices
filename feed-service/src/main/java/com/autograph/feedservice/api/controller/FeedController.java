package com.autograph.feedservice.api.controller;

import com.autograph.feedservice.api.response.ApiResponse;
import com.autograph.feedservice.api.service.feed.impl.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/feed")
public class FeedController {
    private final ResourceServerTokenServices resourceServerTokenServices;
    private final FeedService feedService;

    @Autowired
    public FeedController(ResourceServerTokenServices resourceServerTokenServices, FeedService feedService) {
        this.resourceServerTokenServices = resourceServerTokenServices;
        this.feedService = feedService;
    }

    @PostMapping("/{page}")
    public ApiResponse getUserFeed(@PathVariable("page") int page, @RequestHeader("Authorization") String authorization) {
        Long userId = getUserId(authorization);
        return feedService.getUserFeed(page, userId, authorization);
    }

    @PostMapping("/users/{autographId}")
    public ApiResponse feed(@PathVariable("autographId") Long autographId, @RequestBody List<Long> userIds,
                            @RequestHeader("Authorization") String authorization) {
        Long initiatorUserId = getUserId(authorization);
        return feedService.addFeedItem(userIds, initiatorUserId, autographId);
    }

    HashMap<String, Object> getUserDetails(String token){
        if(!token.isEmpty()) {
            String authToken = token.split(" ")[1];

            OAuth2Authentication auth2 = resourceServerTokenServices.loadAuthentication(authToken);

            return (HashMap<String, Object>) ((HashMap<String, Object>) auth2.getDetails())
                    .get("userDetails");
        }

        return null;
    }

    Long getUserId(String token){
        HashMap<String, Object> userDetails = getUserDetails(token);

        if(userDetails != null)
            return ((Integer) userDetails.get("id")).longValue();
        else return  null;
    }
}
