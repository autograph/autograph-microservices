package com.autograph.feedservice.api;

public enum PurchaseType {
    PURCHASE_NOW,
    AUCTION,
    ASYNC_AUCTION,
    NOT_FOR_SALE
}
