package com.autograph.feedservice.api.service.feed;

import com.autograph.feedservice.api.response.ApiResponse;

import java.util.List;

public interface IFeedService {

    ApiResponse getUserFeed(int page, Long userId, String authorization);

    ApiResponse addFeedItem(List<Long> userIds, Long initiatorUserId, Long autographId);
}
